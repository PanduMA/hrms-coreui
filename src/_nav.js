export default {
  items: [
    {
      name: 'Dashboard',
      url: '/home',
      icon: 'icon-speedometer',
    },
    {
      name : 'SDM',
      icon : 'fa fa-group',
      children:[
        {
          name:'List SDM',
          url:'/sdm/list',
          icon:'fa fa-list-alt',
        },
        {
          name:'Historis SDM',
          url:'/sdm/historis',
          icon:'fa fa-history',
        },
        {
          name:'List Psycology',
          url:'/sdm/psycology',
          icon:'fa fa-stethoscope',
        }
      ],
    },
    {
      name : 'SDM Allocation',
      icon : 'fa fa-random',
      children:[
        {
          name:'List Skill',
          url:'/sdmallocation/listskill',
          icon:'fa fa-list',
        },
        {
          name:'List Category',
          url:'/sdmallocation/category',
          icon:'fa fa-list-ul',
        },
        {
          name:'List SDM Skill Allocation',
          url:'/sdmallocation/list',
          icon:'fa fa-cubes',
        },
        {
          name:'Multi Filtering SDM',
          url:'/sdmallocation/multifilter',
          icon:'fa fa-search',
        }
      ],
    },
    {
      name : 'Project Assignment',
      icon : 'fa fa-tasks',
      children:[
        {
          name:'Input Project SDM',
          url:'/projectassignment/add',
          icon:'icon-note',
        },
        {
          name:'List Detail Project SDM',
          url:'/projectassignment/list',
          icon:'icon-calendar',
        },
        {
          name:'List Detail Client',
          url:'/projectassignment/client',
          icon:'icon-speech',
        },
        {
          name:'Hiring SDM',
          url:'/projectassignment/hiring',
          icon:'fa fa-send',
        },
        {
          name:'SDM Assignment',
          url:'/projectassignment/assignment',
          icon:'fa fa-address-book',
        }
      ],
    },
  ],
};
