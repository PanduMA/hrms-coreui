import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { getDatasFinal } from '../../services/APIFunction';
import {
    Row, Col,
    Card, CardBody, Table,
    FormGroup, Input, InputGroup, InputGroupAddon, InputGroupText, Label, Button,
    Pagination, PaginationItem, PaginationLink,
    Alert, Badge
} from 'reactstrap';

let skip = 0, startSlice = 0, endSlice = 10, total, start = 0, found, messageHiring
class HiringSDM extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            search: '',
            client: '',
            searchBy: '1',
            dataHirings: [],
            dataHiringsAll: [],
            dataHiringsFilter: [],
            totalItems: '',
            dataClients: [],
            picClient: '...',
            clientContact: '...',
            message: '',
            visible: false
        })
        this.handleKeyDown = this.handleKeyDown.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.onDismiss = this.onDismiss.bind(this);
    }
    onDismiss() {
        localStorage.removeItem("messageHiring")
        this.setState({ visible: false });
    }
    componentDidMount() {
        skip = 0
        this.getDataHirings()
        this.getDataClients()
        messageHiring = localStorage.getItem("messageHiring")
        if (messageHiring !== null) {
            this.setState({ visible: true, message: messageHiring })
            this.setTimeoutAlert()
            localStorage.removeItem("messageHiring")
        }
    }
    getDataHirings() {
        getDatasFinal('http://209.97.160.40:8080/SA061118/project/mengelolaHiring/readAll')
            .then((result) => {
                this.setState({
                    dataHiringsAll: result.data.items,
                    dataHirings: result.data.items.slice(startSlice, endSlice),
                    totalItems: result.data.items.length
                })
            });
    }
    getDataClients() {
        var paramOrderBy = 'client_name ASC'
        getDatasFinal('http://209.97.160.40:8080/SA061118/lov/clients', '', '', '', paramOrderBy)
            .then((result) => {
                this.setState({
                    dataClients: result.data
                })
            })
    }
    getDataHiringsFilter(filter) {
        var paramFilter = 'client_id=' + filter
        getDatasFinal('http://209.97.160.40:8080/SA061118/project/mengelolaHiring/readAll', '', '', paramFilter)
            .then((result) => {
                if (result.data != null) {
                    this.setState({
                        dataHiringsFilter: result.data.items,
                        dataHirings: result.data.items.slice(startSlice, endSlice),
                        totalItems: result.data.items.length
                    })
                } else {
                    this.setState({
                        totalItems: '0',
                        dataHirings: [],
                        picClient: '...',
                        clientContact: '...'
                    })
                }
            });
    }
    getDataClientFilter(id) {
        var paramFilter = 'client_id=' + id
        getDatasFinal('http://209.97.160.40:8080/SA061118/project/MengelolaClient/readAll', '', '', paramFilter)
            .then((result) => {
                if (result.data != null) {
                    this.setState({
                        picClient: result.data.items[0].client_picclient,
                        clientContact: result.data.items[0].client_mobileclient
                    })
                } else {
                    this.setState({
                        totalItems: '0',
                        dataHirings: [],
                        picClient: '...',
                        clientContact: '...'
                    })
                }
            })
    }
    handleChange(e) {
        skip = 0
        startSlice = skip + start
        endSlice = parseInt(startSlice) + 10
        if (e.target.name === 'client') {
            this.setState({ client: e.target.value })
            this.getDataHiringsFilter(e.target.value)
            this.getDataClientFilter(e.target.value)
        } else if (e.target.name === 'searchby' && e.target.value !== 'prompt') {
            this.setState({
                searchBy: e.target.value
            })
        } else {
            this.setState({
                search: e.target.value
            })
        }
    }
    handleClick(e) {
        if (e.target.name === 'pagination') { // do when click pagination 
            skip = e.target.value
            startSlice = skip + start
            endSlice = parseInt(startSlice) + 10
            if (this.state.search !== '') {
                if (this.state.searchBy === '1') {
                    this.searchName(this.state.search)
                } else {
                    this.searchStatus(this.state.search)
                }
            } else if (this.state.client !== '') {
                this.setState({
                    dataHirings: this.state.dataHiringsFilter.slice(startSlice, endSlice)
                })
            } else {
                this.setState({
                    dataHirings: this.state.dataHiringsAll.slice(startSlice, endSlice)
                })
            }
        }
        if (e.target.name === 'previous') {
            skip = 0
            startSlice = 0
            endSlice = 10
            if (this.state.search !== '') {
                if (this.state.searchBy === '1') {
                    this.searchName(this.state.search)
                } else {
                    this.searchStatus(this.state.search)
                }
            } else if (this.state.client !== '') {
                this.setState({
                    dataHirings: this.state.dataHiringsFilter.slice(startSlice, endSlice)
                })
            } else {
                this.setState({
                    dataHirings: this.state.dataHiringsAll.slice(startSlice, endSlice)
                })
            }
        }
        if (e.target.name === 'next') {
            skip = parseInt(total) - 1
            startSlice = skip.toString() + start
            endSlice = parseInt(startSlice) + 10
            if (this.state.search !== '') {
                if (this.state.searchBy === '1') {
                    this.searchName(this.state.search)
                } else {
                    this.searchStatus(this.state.search)
                }
            } else if (this.state.client !== '') {
                this.setState({
                    dataHirings: this.state.dataHiringsFilter.slice(startSlice, endSlice)
                })
            } else {
                this.setState({
                    dataHirings: this.state.dataHiringsAll.slice(startSlice, endSlice)
                })
            }
        }
        if (e.target.name === 'reset') {
            this.getDataHirings()
            this.setState({
                client: '',
                picClient: '...',
                clientContact: '...',
                searchBy: '1',
                search: ''
            })
            skip = 0
            startSlice = 0
            endSlice = 10
        }
    }
    handleKeyDown(e) { //handle when user press enter
        if (e.keyCode === 13) {
            this.setState({ search: e.target.value })
            if (this.state.searchBy === '1') {
                this.searchName(e.target.value)
            } else {
                this.searchStatus(e.target.value)
            }
        }
    }
    searchName(value) {
        let word = new RegExp(value, 'gi')
        if (this.state.client !== '') {
            found = this.state.dataHiringsFilter.filter(hiring => hiring.sdm_name.match(word))
        } else {
            found = this.state.dataHiringsAll.filter(hiring => hiring.sdm_name.match(word))
        }
        this.setState({
            dataHirings: found.slice(startSlice, endSlice),
            totalItems: found.length
        })
    }
    searchStatus(value) {
        let word = new RegExp(value, 'gi')
        if (this.state.client !== '') {
            found = this.state.dataHiringsFilter.filter(hiring => hiring.hirestat_name.match(word))
        } else {
            found = this.state.dataHiringsAll.filter(hiring => hiring.hirestat_name.match(word))
        }
        this.setState({
            dataHirings: found.slice(startSlice, endSlice),
            totalItems: found.length
        })
    }
    setTimeoutAlert() {
        setTimeout(() => {
            this.setState({ visible: false })
        }, 3000);
    }
    createPagination = () => {
        let element = []
        total = Math.ceil(this.state.totalItems / 10)
        for (let index = 0; index < total; index++) {
            element.push(
                <PaginationItem key={index} className={parseInt(skip) === index ? 'active' : ''}>
                    <PaginationLink value={index} onClick={this.handleClick} name="pagination">
                        {index + 1}
                    </PaginationLink>
                </PaginationItem>
            )
        }
        if (total > 5) {
            if (skip < 3) {
                return element.slice(0, 5)
            } else {
                var startMove = parseInt(skip) - 2
                var endMove = parseInt(skip) + 3
                if (startMove > (total - 5) && endMove > total) {
                    startMove = total - 5
                    endMove = total
                }
                return element.slice(startMove, endMove)
            }
        } else {
            return element.slice(0, 5)
        }
    }
    render() {
        return (
            <div>
                <Alert color="success" isOpen={this.state.visible} toggle={this.onDismiss} className="shadow-sm">
                    {this.state.message}
                </Alert>
                <Card className="rounded-0 shadow-sm animated fadeIn">
                    <CardBody className="px-5 py-5">
                        <Row>
                            <Label sm={{ size: 1 }}>Client:</Label>
                            <Col sm={{ size: 3 }}>
                                <FormGroup>
                                    <Input type="select" className="rounded-0" name="client" value={this.state.client} onChange={this.handleChange}>
                                        <option value="prompt">Please Choose...</option>
                                        {this.state.dataClients.map(client =>
                                            <option key={client.key} value={client.key}>{client.values.clients_client_name}</option>
                                        )}
                                    </Input>
                                </FormGroup>
                            </Col>
                            <Col sm={{ size: 4 }}>
                                <Label>PIC Client: {this.state.picClient}</Label>
                            </Col>
                            <Col sm={{ size: 4 }}>
                                <Label>Client Contact: {this.state.clientContact}</Label>
                            </Col>
                        </Row>
                        <Row>
                            <Col sm={{ size: 2, offset: 1 }}>
                                {this.state.client !== '' && this.state.client !== '1' ?
                                    <Link to={`/projectassignment/hire/${this.state.client}`} style={{ textDecoration: 'none' }}>
                                        <Button className="rounded-0" name="hire" outline color="primary" block>
                                            <i className="fa fa-fw fa-plus" /> Hire SDM
                                        </Button>
                                    </Link>
                                    :
                                    <Button className="rounded-0" name="hire" outline color="primary" block disabled>
                                        <i className="fa fa-fw fa-plus" /> Hire SDM
                                    </Button>
                                }
                            </Col>
                        </Row>
                    </CardBody>
                </Card>
                <br />
                <Card className="rounded-0 shadow-sm animated fadeIn">
                    <CardBody>
                        <Row>
                            <Label md={{ size: 2 }} className="text-right">Search by</Label>
                            <Col sm={{ size: 3 }}>
                                <FormGroup>
                                    <Input type="select" className="rounded-0" name="searchby" value={this.state.searchBy} onChange={this.handleChange}>
                                        <option value="1">Name</option>
                                        <option value="2">Status</option>
                                    </Input>
                                </FormGroup>
                            </Col>
                            <Col sm={{ size: 4 }}>
                                <FormGroup>
                                    <InputGroup>
                                        <Input type="text" className="rounded-0" onKeyDown={this.handleKeyDown} placeholder="Enter Keyword" value={this.state.search} onChange={this.handleChange} />
                                        <InputGroupAddon addonType="append">
                                            <InputGroupText style={{ backgroundColor: '#FFFFFF' }}>
                                                <i className="fa fa-search"></i>
                                            </InputGroupText>
                                        </InputGroupAddon>
                                    </InputGroup>
                                </FormGroup>
                            </Col>
                            <Col sm={{ size: 2 }}>
                                <Button className="rounded-0" name="reset" outline color="danger" block onClick={this.handleClick}>
                                    <i className="fa fa-fw fa-close" />
                                    Reset
                                </Button>
                            </Col>
                        </Row>
                        <div className="clearfix px-3">
                            <div className="float-right caption-color">
                                <p>Total : {this.state.totalItems}</p>
                            </div>
                        </div>
                        <Table responsive hover bordered className="text-center">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Contact</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.dataHirings.length < 1 ? <tr><td colSpan="5">0 data display</td></tr> :
                                    this.state.dataHirings.map(hiring =>
                                        <tr key={hiring.sdmhiring_id}>
                                            <th>{hiring.norut}</th>
                                            <td>{hiring.sdm_name}</td>
                                            <td>{hiring.sdm_phone}</td>
                                            <td>
                                                {(hiring.hirestat_id === 9 || hiring.hirestat_id === 5) ?
                                                    <h5><Badge color="danger">{hiring.hirestat_name}</Badge></h5>
                                                    : (hiring.hirestat_id === 4 || hiring.hirestat_id === 10) ?
                                                        <h5><Badge color="success">{hiring.hirestat_name}</Badge></h5>
                                                        : (hiring.hirestat_id >= 6 && hiring.hirestat_id <= 8) ?
                                                            <h5><Badge color="warning" className="text-white">{hiring.hirestat_name}</Badge></h5>
                                                            : (hiring.hirestat_id <= 2) ?
                                                                <h5><Badge color="info" className="text-white">{hiring.hirestat_name}</Badge></h5>
                                                                : (hiring.hirestat_id === 3) ?
                                                                    <h5><Badge color="primary">{hiring.hirestat_name}</Badge></h5>
                                                                    : '-'
                                                }
                                            </td>
                                            <td>
                                                <Link to={`/projectassignment/hiring/update/${hiring.sdmhiring_id}`}>
                                                    <Button className="rounded-0" outline color="primary" id="edit-sdm">
                                                        <i className="fa fa-edit"></i>
                                                    </Button>
                                                </Link>
                                            </td>
                                        </tr>
                                    )
                                }
                            </tbody>
                        </Table>
                        <div className="clearfix">
                            {Math.ceil(this.state.totalItems / 10) > 1 ?
                                <div className="float-right">
                                    <Pagination aria-label="Page navigation example">
                                        <PaginationItem className={parseInt(skip) === 0 ? "disabled ds-pagination" : ''} >
                                            <PaginationLink previous onClick={this.handleClick} name="previous" />
                                        </PaginationItem>
                                        {this.createPagination()}
                                        <PaginationItem className={parseInt(skip) === (total - 1) ? "disabled ds-pagination" : ''} >
                                            <PaginationLink next onClick={this.handleClick} name="next" />
                                        </PaginationItem>
                                    </Pagination>
                                </div>
                                : null}
                        </div>
                    </CardBody>
                </Card>
            </div>
        );
    }
}

export default HiringSDM;