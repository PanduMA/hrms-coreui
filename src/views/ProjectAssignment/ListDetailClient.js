import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { deleteDatas, getDatasFinal } from '../../services/APIFunction';
import {
    Row, Col,
    Card, CardBody,
    Table, Alert,
    Input, InputGroup, InputGroupAddon, InputGroupText, Label, Button, FormGroup,
    Pagination, PaginationItem, PaginationLink,
    Modal, ModalHeader, ModalBody, ModalFooter,
} from 'reactstrap';
let skip, total, startSlice = 0, endSlice = 10, start = 0, messageClient
class ListDetailClient extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            search: '',
            dataClientsAll: [],
            dataClients: [],
            totalItems: '',
            clientId: '',
            modal: false,
            message: '',
            visible: false
        })
        this.handleKeyDown = this.handleKeyDown.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.toggle = this.toggle.bind(this);
        this.onDismiss = this.onDismiss.bind(this);
    }
    componentDidMount() {
        skip = 0
        this.getDataClients(skip)
        messageClient = localStorage.getItem("messageClient")
        if (messageClient !== null) {
            this.setState({ visible: true, message: messageClient })
            this.setTimeoutAlert()
            localStorage.removeItem("messageClient")
        }
    }
    toggle(e) {
        if (e.target.id === 'yes') {
            deleteDatas('http://209.97.160.40:8080/SA061118/project/MengelolaClient/delete?client_id=' + this.state.clientId)
                .then((result) => {
                    this.setState({
                        message: result.message,
                        clientId: '',
                        visible: true
                    })
                    this.getDataClients(skip = 0)
                    this.setState({ search: '' })
                    this.setTimeoutAlert()
                })
        }
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
    }
    onDismiss() {
        localStorage.removeItem("messageClient")
        this.setState({ visible: false });
    }
    getDataClients(skip) {
        getDatasFinal('http://209.97.160.40:8080/SA061118/project/MengelolaClient/readAll', skip, 10)
            .then((result) => {
                this.setState({
                    dataClients: result.data.items,
                    totalItems: result.data.totalItems
                })
            });
        getDatasFinal('http://209.97.160.40:8080/SA061118/project/MengelolaClient/readAll')
            .then((result) => {
                this.setState({
                    dataClientsAll: result.data.items,
                })
            });
    }
    handleChange(e) {
        this.setState({ search: e.target.value })
    }
    handleKeyDown(e) { //handle when user press enter
        if (e.keyCode === 13) {
            this.setState({ search: e.target.value })
            skip = 0
            startSlice = 0
            endSlice = 10
            this.searchName(e.target.value)
        }
    }
    searchName(value) {
        let word = new RegExp(value, 'gi')
        let found = this.state.dataClientsAll.filter(client => client.client_name.match(word))
        this.setState({
            dataClients: found.slice(startSlice, endSlice),
            totalItems: found.length
        })
    }
    setTimeoutAlert() {
        setTimeout(() => {
            this.setState({ visible: false })
        }, 3000);
    }
    handleClick(e) {
        if (e.target.name === 'pagination') { // do when click pagination 
            skip = e.target.value
            startSlice = skip + start
            endSlice = parseInt(startSlice) + 10
            if (this.state.search !== '') {
                this.searchName(this.state.search)
            } else {
                this.getDataClients(skip)
            }
        }
        if (e.target.name === 'previous') {
            skip = 0
            if (this.state.search !== '') {
                startSlice = 0
                endSlice = 10
                this.searchName(this.state.search)
            } else {
                this.getDataClients(skip)
            }
        }
        if (e.target.name === 'next') {
            skip = parseInt(total) - 1
            if (this.state.search !== '') {
                startSlice = skip.toString() + start
                endSlice = parseInt(startSlice) + 10
                this.searchName(this.state.search)
            } else {
                this.getDataClients(skip)
            }
        }
        if (e.target.name === 'reset') {
            skip = 0
            this.getDataClients(skip)
            this.setState({ search: '' })
        }

    }
    handleClickAction = (e, param) => {
        e.preventDefault();
        this.setState({ clientId: param })
        this.setState(prevState => ({ modal: !prevState.modal }));
    }
    createPagination = () => {
        let element = []
        total = Math.ceil(this.state.totalItems / 10)
        for (let index = 0; index < total; index++) {
            element.push(
                <PaginationItem key={index} className={parseInt(skip) === index ? 'active' : ''}>
                    <PaginationLink value={index} onClick={this.handleClick} name="pagination">
                        {index + 1}
                    </PaginationLink>
                </PaginationItem>
            )
        }
        if (total > 5) {
            if (skip < 3) {
                return element.slice(0, 5)
            } else {
                var startMove = parseInt(skip) - 2
                var endMove = parseInt(skip) + 3
                if (startMove > (total - 5) && endMove > total) {
                    startMove = total - 5
                    endMove = total
                }
                return element.slice(startMove, endMove)
            }
        } else {
            return element.slice(0, 5)
        }
    }
    render() {
        return (
            <div>
                <Alert color="success" isOpen={this.state.visible} toggle={this.onDismiss} className="shadow-sm">
                    {this.state.message}
                </Alert>
                <Row>
                    <Col sm={{ size: 12 }}>
                        <Card className="rounded-0 shadow-sm animated fadeIn">
                            <CardBody>
                                <Row>
                                    <Label md={{ size: 2 }} className="text-right">Search by</Label>
                                    <Col sm={{ size: 2 }}>
                                        <FormGroup>
                                            <Input type="select" className="rounded-0">
                                                <option>Client Name</option>
                                            </Input>
                                        </FormGroup>
                                    </Col>
                                    <Col sm={{ size: 4 }}>
                                        <FormGroup>
                                            <InputGroup>
                                                <Input
                                                    type="text"
                                                    className="rounded-0"
                                                    placeholder="Enter Keyword"
                                                    onKeyDown={this.handleKeyDown}
                                                    value={this.state.search}
                                                    onChange={this.handleChange} />
                                                <InputGroupAddon addonType="append">
                                                    <InputGroupText style={{ backgroundColor: '#FFFFFF' }}>
                                                        <i className="fa fa-search"></i>
                                                    </InputGroupText>
                                                </InputGroupAddon>
                                            </InputGroup>
                                        </FormGroup>
                                    </Col>
                                    <Col sm={{ size: 2 }}>
                                        <Button className="rounded-0" name="reset" outline color="danger" block onClick={this.handleClick}>
                                            <i className="fa fa-fw fa-close" />
                                            Reset
                                        </Button>
                                    </Col>
                                    <Col sm={{ size: 2 }}>
                                        <Link to={`/projectassignment/client/add`} style={{ textDecoration: 'none' }}>
                                            <Button className="rounded-0" outline color="primary" block>
                                                <i className="fa fa-fw fa-plus" />
                                                Add Client
                                            </Button>
                                        </Link>
                                    </Col>
                                </Row>
                                <div className="clearfix px-3">
                                    <div className="float-right caption-color">
                                        <p>Total : {this.state.totalItems}</p>
                                    </div>
                                </div>
                                <Table responsive hover bordered className="text-center">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Client Name</th>
                                            <th>PIC Handler</th>
                                            <th>Contact Person</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.dataClients.length < 1 ? <tr><td colSpan="5">0 data display</td></tr> :
                                            this.state.dataClients.map(client =>
                                                <tr key={client.client_id}>
                                                    <th>{client.norut}</th>
                                                    <td>{client.client_name}</td>
                                                    <td>{client.client_picclient}</td>
                                                    <td>{client.client_mobileclient}</td>
                                                    <td>
                                                        <Link to={`/projectassignment/client/update/${client.client_id}`}>
                                                            <Button className="rounded-0 mr-2" outline color="primary" id="edit-sdm">
                                                                <i className="fa fa-edit"></i>
                                                            </Button>
                                                        </Link>
                                                        <Button className="rounded-0" outline color="danger" id="delete-sdm" onClick={(e) => this.handleClickAction(e, client.client_id)}>
                                                            <i className="fa fa-trash"></i>
                                                        </Button>
                                                    </td>
                                                </tr>
                                            )}
                                    </tbody>
                                </Table>
                                <div className="clearfix">
                                    {Math.ceil(this.state.totalItems / 10) > 1 ?
                                        <div className="float-right">
                                            <Pagination aria-label="Page navigation example">
                                                <PaginationItem className={parseInt(skip) === 0 ? "disabled ds-pagination" : ''} >
                                                    <PaginationLink previous onClick={this.handleClick} name="previous" />
                                                </PaginationItem>
                                                {this.createPagination()}
                                                <PaginationItem className={parseInt(skip) === (total - 1) ? "disabled ds-pagination" : ''} >
                                                    <PaginationLink next onClick={this.handleClick} name="next" />
                                                </PaginationItem>
                                            </Pagination>
                                        </div>
                                        : null}
                                </div>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className} centered >
                    <ModalHeader toggle={this.toggle}>
                        <i className="fa fa-warning text-warning"></i> Warning
                    </ModalHeader>
                    <ModalBody>
                        Are you sure want to delete?
                    </ModalBody>
                    <ModalFooter>
                        <Button className="rounded-0" id="yes" color="primary" onClick={this.toggle}>Yes</Button>
                        <Button className="rounded-0" color="danger" onClick={this.toggle}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default ListDetailClient;