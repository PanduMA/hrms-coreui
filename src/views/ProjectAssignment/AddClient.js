import React, { Component } from 'react';
import { postDatas } from '../../services/APIFunction';
import { Link } from 'react-router-dom';
import {
    Card, CardBody,
    Form, FormGroup, Label, Input, Button,
    Col, Row
} from 'reactstrap';
class AddClient extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            clientName: '',
            picHandler: '',
            contactHandler: '',
            clientAddress: '',
        })
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.handleKey = this.handleKey.bind(this)
    }
    handleSubmit(event) {
        event.preventDefault()
        var body = JSON.stringify({
            client_name: this.state.clientName,
            client_picclient: this.state.picHandler,
            client_mobileclient: this.state.contactHandler,
            client_address: this.state.clientAddress
        })
        this.createNewClient(body)
    }
    handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    handleKey(e) {
        if (e.charCode < 48 || e.charCode > 57) {
            e.preventDefault()
        }
    }
    createNewClient(body) {
        postDatas('http://209.97.160.40:8080/SA061118/project/MengelolaClient/create', body)
            .then(result => {
                this.clearFields()
                localStorage.setItem("messageClient", result.message)
                window.location = "#/projectassignment/client"
            })
    }
    clearFields() {
        this.setState({
            clientName: '',
            picHandler: '',
            contactHandler: '',
            clientAddress: ''
        })
    }
    render() {
        return (
            <div>
                <Card className="rounded-0 shadow-sm animated fadeIn">
                    <CardBody className="px-5 py-5">
                        <Form onSubmit={this.handleSubmit}>
                            <FormGroup row>
                                <Label sm={{ size: 3, offset: 1 }} className="text-right">Client Name<i className="text-danger">*</i></Label>
                                <Col sm={{ size: 5 }}>
                                    <Input
                                        type="text"
                                        className="rounded-0"
                                        name="clientName"
                                        placeholder="Enter Client Name"
                                        value={this.state.clientName}
                                        onChange={this.handleChange}
                                        required />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 3, offset: 1 }} className="text-right">PIC Handler</Label>
                                <Col sm={{ size: 5 }}>
                                    <Input
                                        type="text"
                                        className="rounded-0"
                                        name="picHandler"
                                        placeholder="Enter PIC Handler"
                                        value={this.state.picHandler}
                                        onChange={this.handleChange} />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 3, offset: 1 }} className="text-right">Contact Handler</Label>
                                <Col sm={{ size: 5 }}>
                                    <Input
                                        type="text"
                                        className="rounded-0"
                                        name="contactHandler"
                                        placeholder="Enter Contact Handler"
                                        maxLength="13"
                                        value={this.state.contactHandler}
                                        onChange={this.handleChange}
                                        onKeyPress={this.handleKey} />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 3, offset: 1 }} className="text-right">Client Address</Label>
                                <Col sm={{ size: 5 }}>
                                    <Input
                                        type="textarea"
                                        className="rounded-0"
                                        name="clientAddress"
                                        placeholder="Enter Client Address"
                                        value={this.state.clientAddress}
                                        onChange={this.handleChange} />
                                </Col>
                            </FormGroup>
                            <Row>
                                <Col sm={{ size: 3, offset: 3 }}>
                                    {this.state.clientName === '' ?
                                        <Button className="rounded-0" color="primary" outline block disabled>Save</Button>
                                        :
                                        <Button type="submit" className="rounded-0" color="primary" outline block>Save</Button>
                                    }
                                </Col>
                                <Col sm={{ size: 3 }}>
                                    <Link to="/projectassignment/client" className="btn btn-outline-danger btn-block rounded-0" role="button">
                                        Back
                                    </Link>
                                </Col>
                            </Row>
                        </Form>
                    </CardBody>
                </Card>
            </div>
        );
    }
}

export default AddClient;