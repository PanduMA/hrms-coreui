import React, { Component } from 'react';
import { getDatasFinal } from '../../services/APIFunction';
import { FormGroup, Label, Col, Input } from 'reactstrap';
class ModalBodyContent extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            dataProject: []
        })
    }
    componentDidMount() {
        this.getDataSDMProject()
    }
    getDataSDMProject() {
        var paramFilter = 'project_id=' + this.props.param
        getDatasFinal('http://209.97.160.40:8080/SA061118/project/MengelolaProject/readAll', '', '', paramFilter)
            .then((result) => {
                this.setState({
                    dataProject: result.data.items
                })
            })
    }
    render() {
        if (this.props.id === 'view-sdm') {
            return (
                <div key={this.props.id}>
                    <FormGroup row>
                        <Label sm={{ size: 2 }} className="text-right">
                            SDM Name :
                                </Label>
                        <Col sm={{ size: 4 }}>
                            {this.state.dataProject.map(project =>
                                <Input className="rounded-0" type="text" name="sdmName" value={project.sdm_name} readOnly />
                            )}
                        </Col>
                        <Label sm={{ size: 2 }} className="text-right">
                            Role Project :
                                </Label>
                        <Col sm={{ size: 4 }}>
                            {this.state.dataProject.map(project =>
                                <Input className="rounded-0" type="text" name="roleProject" value={project.project_role} readOnly />
                            )}
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm={{ size: 2 }} className="text-right">NIK :</Label>
                        <Col sm={{ size: 4 }}>
                            {this.state.dataProject.map(project =>
                                <Input className="rounded-0" type="text" name="nik" value={project.sdm_nik} readOnly />
                            )}
                        </Col>
                        <Label sm={{ size: 2 }} className="text-right">App Type :</Label>
                        <Col sm={{ size: 4 }}>
                            {this.state.dataProject.map(project =>
                                <Input className="rounded-0" type="text" name="appType" value={project.project_apptype} readOnly />
                            )}
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm={{ size: 2 }} className="text-right">
                            Project Name :
                                </Label>
                        <Col sm={{ size: 4 }}>
                            {this.state.dataProject.map(project =>
                                <Input className="rounded-0" type="text" name="projectName" value={project.project_name} readOnly />
                            )}
                        </Col>
                        <Label sm={{ size: 2 }} className="text-right">Dev Language :</Label>
                        <Col sm={{ size: 4 }}>
                            {this.state.dataProject.map(project =>
                                <Input className="rounded-0" type="text" name="devLang" value={project.project_devlanguage} readOnly />
                            )}
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm={{ size: 2 }} className="text-right">Customer :</Label>
                        <Col sm={{ size: 4 }}>
                            {this.state.dataProject.map(project =>
                                <Input className="rounded-0" type="text" name="customer" value={project.project_customer} readOnly />
                            )}
                        </Col>
                        <Label sm={{ size: 2 }} className="text-right">Server OS :</Label>
                        <Col sm={{ size: 4 }}>
                            {this.state.dataProject.map(project =>
                                <Input className="rounded-0" type="text" name="serverOS" value={project.project_serveros} readOnly />
                            )}
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm={{ size: 2 }} className="text-right">
                            Project Site :
                                </Label>
                        <Col sm={{ size: 4 }}>
                            {this.state.dataProject.map(project =>
                                <Input className="rounded-0" type="text" name="projectSite" value={project.project_projectsite} readOnly />
                            )}
                        </Col>
                        <Label sm={{ size: 2 }} className="text-right">Framework :</Label>
                        <Col sm={{ size: 4 }}>
                            {this.state.dataProject.map(project =>
                                <Input className="rounded-0" type="text" name="framework" value={project.project_framework} readOnly />
                            )}
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm={{ size: 2 }} className="text-right">Period Start :</Label>
                        <Col sm={{ size: 4 }}>
                            {this.state.dataProject.map(project =>
                                <Input className="rounded-0" type="date" name="periodStart" value={project.project_startdate} readOnly />
                            )}
                        </Col>
                        <Label sm={{ size: 2 }} className="text-right">Database :</Label>
                        <Col sm={{ size: 4 }}>
                            {this.state.dataProject.map(project =>
                                <Input className="rounded-0" type="text" name="database" value={project.project_database} readOnly />
                            )}
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm={{ size: 2 }} className="text-right">Period End :</Label>
                        <Col sm={{ size: 4 }}>
                            {this.state.dataProject.map(project =>
                                <Input className="rounded-0" type="date" name="periodEnd" value={project.project_enddate} readOnly />
                            )}
                        </Col>
                        <Label sm={{ size: 2 }} className="text-right">Dev Tools :</Label>
                        <Col sm={{ size: 4 }}>
                            {this.state.dataProject.map(project =>
                                <Input className="rounded-0" type="text" name="devTools" value={project.project_devtool} readOnly />
                            )}
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm={{ size: 2 }} className="text-right">Project Desc :</Label>
                        <Col sm={{ size: 4 }}>
                            {this.state.dataProject.map(project =>
                                <Input className="rounded-0" type="textarea" name="projectDesc" value={project.project_desc} readOnly />
                            )}
                        </Col>
                        <Label sm={{ size: 2 }} className="text-right">Other Info :</Label>
                        <Col sm={{ size: 4 }}>
                            {this.state.dataProject.map(project =>
                                <Input className="rounded-0" type="textarea" name="otherInfo" value={project.project_otherinfo} readOnly />
                            )}
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm={{ size: 2 }} className="text-right">App Server :</Label>
                        <Col sm={{ size: 4 }}>
                            {this.state.dataProject.map(project =>
                                <Input className="rounded-0" type="text" name="appServer" value={project.project_appserver} readOnly />
                            )}
                        </Col>
                        <Label sm={{ size: 2 }} className="text-right">Technical Info :</Label>
                        <Col sm={{ size: 4 }}>
                            {this.state.dataProject.map(project =>
                                <Input className="rounded-0" type="textarea" name="technicalInfo" value={project.project_technicalinfo} readOnly />
                            )}
                        </Col>
                    </FormGroup>
                </div>
            );
        } else {
            return (
                <div key={this.props.id}>
                    Are you sure to delete?
                </div>
            );
        }

    }
}

export default ModalBodyContent;