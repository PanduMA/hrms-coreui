import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { putDatas, getDatasFinal } from '../../services/APIFunction';
import {
    Card, CardBody,
    Form, FormGroup, Label, Input, Button,
    Col, Row,
} from 'reactstrap';
let body
class UpdateClient extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            dataClients: [],
            clientName: '',
            picHandler: '',
            contactHandler: '',
            clientAddress: '',
            isChange: false
        })
        this.handleChange = this.handleChange.bind(this)
        this.handleKey = this.handleKey.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    componentDidMount() {
        this.getDataClient(this.props.match.params.id)
    }
    getDataClient(id) {
        var paramFilter = 'client_id=' + id
        getDatasFinal('http://209.97.160.40:8080/SA061118/project/MengelolaClient/readAll', '', '', paramFilter)
            .then((result) => {
                this.setState({
                    dataClients: result.data.items
                })
                this.state.dataClients.map(client =>
                    this.setState({
                        clientName: client.client_name,
                        picHandler: client.client_picclient,
                        contactHandler: client.client_mobileclient,
                        clientAddress: client.client_address
                    })
                )
            });
    }
    handleChange(e) {
        if (e.target.name === 'clientName') {
            this.setState({
                clientName: e.target.value
            })
        } else if (e.target.name === 'picHandler') {
            this.setState({
                picHandler: e.target.value
            })
        } else if (e.target.name === 'contactHandler') {
            e.target.value.replace(/[^\d]/g, ''); //ketika user inputkan selain digit maka akan direplace jadi ''
            this.setState({
                contactHandler: e.target.value
            })
        } else if (e.target.name === 'clientAddress') {
            this.setState({
                clientAddress: e.target.value
            })
        }
        this.setState({ isChange: true })
    }
    handleSubmit(event) {
        body = JSON.stringify({
            Model_ID: 'client_id=' + this.props.match.params.id,
            client_id: parseInt(this.props.match.params.id),
            client_name: this.state.clientName,
            client_picclient: this.state.picHandler,
            client_mobileclient: this.state.contactHandler,
            client_address: this.state.clientAddress
        })
        event.preventDefault();
        this.updateDataClient(body)
    }
    handleKey(e) {
        if (e.charCode < 48 || e.charCode > 57) {
            e.preventDefault();
        }
    }
    updateDataClient(body) {
        putDatas('http://209.97.160.40:8080/SA061118/project/MengelolaClient/update?client_id=' + parseInt(this.props.match.params.id), body)
            .then((result) => {
                this.setState({
                    clientName: '',
                    picHandler: '',
                    contactHandler: '',
                    clientAddress: '',
                })
                window.location = "#/projectassignment/client"
                localStorage.setItem("messageClient", result.message)
            })
    }
    render() {
        return (
            <div>
                <Card className="rounded-0 shadow-sm animated fadeIn">
                    <CardBody className="px-5 py-5">
                        <Form onSubmit={this.handleSubmit}>
                            <FormGroup row>
                                <Label sm={{ size: 3, offset: 1 }} className="text-right">Client Name<i className="text-danger">*</i></Label>
                                <Col sm={{ size: 5 }}>
                                    <Input
                                        type="text"
                                        className="rounded-0"
                                        name="clientName"
                                        onChange={this.handleChange}
                                        value={this.state.clientName}
                                        required />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 3, offset: 1 }} className="text-right">Pic Handler</Label>
                                <Col sm={{ size: 5 }}>
                                    <Input
                                        type="text"
                                        className="rounded-0"
                                        name="picHandler"
                                        onChange={this.handleChange}
                                        value={this.state.picHandler} />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 3, offset: 1 }} className="text-right">Contact Handler</Label>
                                <Col sm={{ size: 5 }}>
                                    <Input
                                        type="text"
                                        className="rounded-0"
                                        name="contactHandler"
                                        onChange={this.handleChange}
                                        value={this.state.contactHandler}
                                        onKeyPress={this.handleKey}
                                        maxLength="13"
                                    />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 3, offset: 1 }} className="text-right">Client Address</Label>
                                <Col sm={{ size: 5 }}>
                                    <Input
                                        type="textarea"
                                        className="rounded-0"
                                        name="clientAddress"
                                        onChange={this.handleChange}
                                        value={this.state.clientAddress} />
                                </Col>
                            </FormGroup>
                            <Row>
                                <Col sm={{ size: 3, offset: 3 }}>
                                    {!this.state.isChange ?
                                        <Button className="rounded-0" block outline color="primary" disabled>Update</Button> :
                                        <Button className="rounded-0" block outline type="submit" color="primary">Update</Button>}
                                </Col>
                                <Col sm={{ size: 3 }}>
                                    <Link to="/projectassignment/client" className="btn btn-outline-danger btn-block rounded-0" role="button">
                                        Back
                                    </Link>
                                </Col>
                            </Row>
                        </Form>
                    </CardBody>
                </Card>
            </div>
        );
    }
}

export default UpdateClient;