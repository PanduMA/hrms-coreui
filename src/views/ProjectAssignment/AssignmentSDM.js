import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { getDatasFinal } from '../../services/APIFunction';
import {
    Row, Col,
    Card, CardBody,
    Table,
    Input, InputGroup, InputGroupAddon, InputGroupText, Label, Button, FormGroup,
    Pagination, PaginationItem, PaginationLink, Alert , UncontrolledTooltip
} from 'reactstrap';
let skip = 0, startSlice = 0, endSlice = 10, total, start = 0, found, messageAssign
class AssignmentSDM extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            search: '',
            client: '',
            searchBy: '1',
            dataAssignmentsAll: [],
            dataAssignmentsFilter: [],
            dataAssignments: [],
            totalItems: '',
            dataClients: [],
            picClient: '...',
            clientContact: '...',
            message: '',
            visible: false
        })
        this.handleKeyDown = this.handleKeyDown.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.onDismiss = this.onDismiss.bind(this);
    }
    onDismiss() {
        localStorage.removeItem("messageAssign")
        this.setState({ visible: false });
    }
    componentDidMount() {
        skip = 0
        this.getDataAssignment(skip)
        this.getDataClients()
        messageAssign = localStorage.getItem("messageAssign")
        if (messageAssign !== null) {
            this.setState({ visible: true, message: messageAssign })
            this.setTimeoutAlert()
            localStorage.removeItem("messageAssign")
        }
    }
    getDataAssignment(skip) {
        getDatasFinal('http://209.97.160.40:8080/SA061118/project/SdmAssignment/readAll')
            .then((result) => {
                this.setState({
                    dataAssignmentsAll: result.data.items,
                })
            });
        getDatasFinal('http://209.97.160.40:8080/SA061118/project/SdmAssignment/readAll', skip, 10)
            .then((result) => {
                this.setState({
                    dataAssignments: result.data.items,
                    totalItems: result.data.totalItems
                })
            })
    }
    getDataAssignmentFilter(filter, skip) {
        var paramFilter = 'client_id=' + filter
        getDatasFinal('http://209.97.160.40:8080/SA061118/project/SdmAssignment/readAll', '', '', paramFilter)
            .then((result) => {
                this.setState({
                    dataAssignmentsFilter: result.data.items,
                })
            });
        getDatasFinal('http://209.97.160.40:8080/SA061118/project/SdmAssignment/readAll', skip, 10, paramFilter)
            .then((result) => {
                if (result.data.totalItems !== 0) {
                    this.setState({
                        dataAssignments: result.data.items,
                        totalItems: result.data.totalItems
                    })
                } else {
                    this.setState({
                        totalItems: '0',
                        dataAssignments: [],
                        picClient: '...',
                        clientContact: '...'
                    })
                }
            });
    }
    getDataClients() {
        var paramOrderBy = 'client_name ASC'
        getDatasFinal('http://209.97.160.40:8080/SA061118/lov/clients', '', '', '', paramOrderBy)
            .then((result) => {
                this.setState({
                    dataClients: result.data
                })
            })
    }
    getDataClientFilter(id) {
        var paramFilter = 'client_id=' + id
        getDatasFinal('http://209.97.160.40:8080/SA061118/project/MengelolaClient/readAll', '', '', paramFilter)
            .then((result) => {
                this.setState({
                    picClient: result.data.items[0].client_picclient,
                    clientContact: result.data.items[0].client_mobileclient
                })
            })
    }
    handleChange(e) {
        if (e.target.name === 'client') {
            skip = 0
            this.setState({ client: e.target.value, search: '' })
            this.getDataAssignmentFilter(e.target.value, skip)
            this.getDataClientFilter(e.target.value)
        } else if (e.target.name === 'searchby') {
            this.setState({ searchBy: e.target.value })
        } else {
            this.setState({ search: e.target.value })
        }
    }
    handleKeyDown(e) { //handle when user press enter
        if (e.keyCode === 13) {
            this.setState({ search: e.target.value })
            skip = 0
            startSlice = 0
            endSlice = 10
            if (this.state.searchBy === '1') {
                this.searchName(e.target.value)
            } else {
                this.searchPIC(e.target.value)
            }
        }
    }
    searchName(value) {
        let word = new RegExp(value, 'gi')
        if (this.state.client !== '') {
            found = this.state.dataAssignmentsFilter.filter(assignment => assignment.sdm_name.match(word))
        } else {
            found = this.state.dataAssignmentsAll.filter(assignment => assignment.sdm_name.match(word))
        }
        this.setState({
            dataAssignments: found.slice(startSlice, endSlice),
            totalItems: found.length
        })
    }
    searchPIC(value) {
        let word = new RegExp(value, 'gi')
        if (this.state.client !== '') {
            found = this.state.dataAssignmentsFilter.filter(assignment => assignment.sdmassign_picclient.match(word))
        } else {
            found = this.state.dataAssignmentsAll.filter(assignment => assignment.sdmassign_picclient.match(word))
        }
        this.setState({
            dataAssignments: found.slice(startSlice, endSlice),
            totalItems: found.length
        })
    }
    handleClick(e) {
        if (e.target.name === 'pagination') { // do when click pagination 
            skip = e.target.value
            startSlice = skip + start
            endSlice = parseInt(startSlice) + 10
            if (this.state.search !== '') {
                if (this.state.searchBy === '1') {
                    this.searchName(this.state.search)
                } else {
                    this.searchPIC(this.state.search)
                }
            } else if (this.state.client !== '') {
                this.setState({
                    dataAssignments: this.state.dataAssignmentsFilter.slice(startSlice, endSlice)
                })
            } else {
                this.setState({
                    dataAssignments: this.state.dataAssignmentsAll.slice(startSlice, endSlice)
                })
            }
        }
        if (e.target.type === 'reset') {
            this.getDataAssignment(skip = 0)
            this.setState({
                searchBy: '1',
                search: '',
                client: '',
                picClient: '...',
                clientContact: '...'
            })
        }
        if (e.target.name === 'previous') {
            skip = 0
            startSlice = 0
            endSlice = 10
            if (this.state.search !== '') {
                if (this.state.searchBy === '1') {
                    this.searchName(this.state.search)
                } else {
                    this.searchPIC(this.state.search)
                }
            } else if (this.state.client !== '') {
                this.setState({
                    dataAssignments: this.state.dataAssignmentsFilter.slice(startSlice, endSlice)
                })
            } else {
                this.setState({
                    dataAssignments: this.state.dataAssignmentsAll.slice(startSlice, endSlice)
                })
            }
        }
        if (e.target.name === 'next') {
            skip = parseInt(total) - 1
            startSlice = skip.toString() + start
            endSlice = parseInt(startSlice) + 10
            if (this.state.search !== '') {
                if (this.state.searchBy === '1') {
                    this.searchName(this.state.search)
                } else {
                    this.searchPIC(this.state.search)
                }
            } else if (this.state.client !== '') {
                this.setState({
                    dataAssignments: this.state.dataAssignmentsFilter.slice(startSlice, endSlice)
                })
            } else {
                this.setState({
                    dataAssignments: this.state.dataAssignmentsAll.slice(startSlice, endSlice)
                })
            }
        }
    }
    setTimeoutAlert() {
        setTimeout(() => {
            this.setState({ visible: false })
        }, 3000);
    }
    createPagination = () => {
        let element = []
        total = Math.ceil(this.state.totalItems / 10)
        for (let index = 0; index < total; index++) {
            element.push(
                <PaginationItem key={index} className={parseInt(skip) === index ? 'active' : ''}>
                    <PaginationLink value={index} onClick={this.handleClick} name="pagination">
                        {index + 1}
                    </PaginationLink>
                </PaginationItem>
            )
        }
        if (total > 5) {
            if (skip < 3) {
                return element.slice(0, 5)
            } else {
                var startMove = parseInt(skip) - 2
                var endMove = parseInt(skip) + 3
                if (startMove > (total - 5) && endMove > total) {
                    startMove = total - 5
                    endMove = total
                }
                return element.slice(startMove, endMove)
            }
        } else {
            return element.slice(0, 5)
        }
    }
    render() {
        return (
            <div>
                <Alert color="success" isOpen={this.state.visible} toggle={this.onDismiss} className="shadow-sm">
                    {this.state.message}
                </Alert>
                <Card className="rounded-0 shadow-sm animated fadeIn">
                    <CardBody className="px-5 py-5">
                        <Row>
                            <Label sm={{ size: 1 }}>Client:</Label>
                            <Col sm={{ size: 3 }}>
                                <FormGroup>
                                    <Input type="select" className="rounded-0" name="client" value={this.state.client} onChange={this.handleChange}>
                                        <option value="prompt">Please Choose...</option>
                                        {this.state.dataClients.map(client =>
                                            <option key={client.key} value={client.key}>{client.values.clients_client_name}</option>
                                        )}
                                    </Input>
                                </FormGroup>
                            </Col>
                            <Col sm={{ size: 4 }}>
                                <Label>PIC Client: {this.state.picClient}</Label>
                            </Col>
                            <Col sm={{ size: 4 }}>
                                <Label>Client Contact: {this.state.clientContact}</Label>
                            </Col>
                        </Row>
                    </CardBody>
                </Card>
                <br />
                <Card className="rounded-0 shadow-sm animated fadeIn">
                    <CardBody>
                        <Row>
                            <Label md={{ size: 2 }} className="text-right">Search by</Label>
                            <Col sm={{ size: 3 }}>
                                <FormGroup>
                                    <Input type="select" className="rounded-0" name="searchby" value={this.state.searchBy} onChange={this.handleChange}>
                                        <option value="1">Name</option>
                                        <option value="2">PIC Handler</option>
                                    </Input>
                                </FormGroup>
                            </Col>
                            <Col sm={{ size: 4 }}>
                                <FormGroup>
                                    <InputGroup>
                                        <Input type="text" className="rounded-0" onKeyDown={this.handleKeyDown} placeholder="Enter Keyword" value={this.state.search} onChange={this.handleChange} />
                                        <InputGroupAddon addonType="append">
                                            <InputGroupText style={{ backgroundColor: '#FFFFFF' }}>
                                                <i className="fa fa-search"></i>
                                            </InputGroupText>
                                        </InputGroupAddon>
                                    </InputGroup>
                                </FormGroup>
                            </Col>
                            <Col sm={{ size: 2 }}>
                                <Button className="rounded-0" type="reset" outline color="danger" block onClick={this.handleClick}>
                                    <i className="fa fa-fw fa-close" />
                                    Reset
                                </Button>
                            </Col>
                        </Row>
                        <div className="clearfix px-3">
                            <div className="float-right caption-color">
                                <p>Total : {this.state.totalItems}</p>
                            </div>
                        </div>
                        <Table responsive hover bordered className="text-center">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Contact</th>
                                    <th>Start</th>
                                    <th>End</th>
                                    <th>Location</th>
                                    <th>PIC Handler</th>
                                    <th>PIC Contact</th>
                                    <th>Method</th>
                                    <th>Notification Assigne</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.dataAssignments.length < 1 ? <tr><td colSpan="11">0 data display</td></tr> :
                                    this.state.dataAssignments.map((assignment, index) =>
                                        <tr key={assignment.sdmassign_id}>
                                            <th>{assignment.norut}</th>
                                            <td>{assignment.sdm_name}</td>
                                            <td>{assignment.sdm_phone}</td>
                                            <td>{assignment.sdmassign_startdate}</td>
                                            <td>{assignment.sdmassign_enddate}</td>
                                            <td>{assignment.sdmassign_loc}</td>
                                            <td>{assignment.sdmassign_picclient}</td>
                                            <td>{assignment.sdmassign_picclientphone}</td>
                                            <td>{assignment.method_name}</td>
                                            <td>
                                                {(assignment.sdmassign_notification === 'black') ?
                                                    <div>
                                                        <i className="fa fa-warning text-dark fa-2x" id={"black-" + index}></i>
                                                        <UncontrolledTooltip placement="bottom" target={"black-" + index}>
                                                            Non-Aktif
                                                        </UncontrolledTooltip>
                                                    </div>
                                                    : (assignment.sdmassign_notification === 'grey') ?
                                                        <div>
                                                            <i className="fa fa-warning text-secondary fa-2x" id={"grey-" + index}></i>
                                                            <UncontrolledTooltip placement="bottom" target={"grey-" + index}>
                                                                Sisa lebih dari 4 bulan
                                                            </UncontrolledTooltip>
                                                        </div>
                                                        : (assignment.sdmassign_notification === 'red') ?
                                                            <div>
                                                                <i className="fa fa-warning text-danger fa-2x" id={"red-" + index}></i>
                                                                <UncontrolledTooltip placement="bottom" target={"red-" + index}>
                                                                    Sisa 1 bulan
                                                                </UncontrolledTooltip>
                                                            </div>
                                                            : (assignment.sdmassign_notification === 'green') ?
                                                                <div>
                                                                    <i className="fa fa-warning text-success fa-2x" id={"green-" + index}></i>
                                                                    <UncontrolledTooltip placement="bottom" target={"green-" + index}>
                                                                        Sisa 4 bulan
                                                                    </UncontrolledTooltip>
                                                                </div>
                                                                : (assignment.sdmassign_notification === 'yellow') ?
                                                                    <div>
                                                                        <i className="fa fa-warning text-warning fa-2x" id={"yellow-" + index}></i>
                                                                        <UncontrolledTooltip placement="bottom" target={"yellow-" + index}>
                                                                            Sisa 2 bulan
                                                                        </UncontrolledTooltip>
                                                                    </div>
                                                                    : '-'
                                                }
                                            </td>
                                            <td>
                                                <Link to={`/projectassignment/assignment/update/${assignment.sdmassign_id}`}>
                                                    <Button className="rounded-0" outline color="primary" id="edit-sdm">
                                                        <i className="fa fa-edit"></i>
                                                    </Button>
                                                </Link>
                                            </td>
                                        </tr>
                                    )
                                }
                            </tbody>
                        </Table>
                        <div className="clearfix">
                            {Math.ceil(this.state.totalItems / 10) > 1 ?
                                <div className="float-right">
                                    <Pagination aria-label="Page navigation example">
                                        <PaginationItem className={parseInt(skip) === 0 ? "disabled ds-pagination" : ''} >
                                            <PaginationLink previous onClick={this.handleClick} name="previous" />
                                        </PaginationItem>
                                        {this.createPagination()}
                                        <PaginationItem className={parseInt(skip) === (total - 1) ? "disabled ds-pagination" : ''} >
                                            <PaginationLink next onClick={this.handleClick} name="next" />
                                        </PaginationItem>
                                    </Pagination>
                                </div>
                                : null}
                        </div>
                    </CardBody>
                </Card>
            </div>
        );
    }
}

export default AssignmentSDM;