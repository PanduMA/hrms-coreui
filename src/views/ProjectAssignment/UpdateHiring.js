import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { putDatas, getDatasFinal } from '../../services/APIFunction';
import {
    Card, CardBody,
    Form, FormGroup, Label, Input, Button,
    Col, Row,
} from 'reactstrap';
let body
class UpdateHiring extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            clientName: '',
            sdmName: '',
            statusId: '',
            dataLovStatus: [],
            dataHirings: [],
            sdmHiringId: '',
            clientId: '',
            isChange: false,
        })
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    componentDidMount() {
        this.getStatusLov()
        this.getDataHiring(this.props.match.params.id)
    }
    getStatusLov() {
        getDatasFinal('http://209.97.160.40:8080/SA061118/lov/StatusHiring')
            .then((result) => {
                this.setState({
                    dataLovStatus: result.data
                })
            })
    }
    getDataHiring(id) {
        var paramFilter = 'sdmhiring_id=' + id
        getDatasFinal('http://209.97.160.40:8080/SA061118/project/mengelolaHiring/readAll', '', '', paramFilter)
            .then((result) => {
                this.setState({
                    dataHirings: result.data.items
                })
                this.state.dataHirings.map(hiring =>
                    this.setState({
                        clientName: hiring.client_name,
                        sdmName: hiring.sdm_name,
                        sdmHiringId: hiring.sdmhiring_id,
                        clientId: hiring.client_id,
                        statusId: hiring.hirestat_id
                    })
                )
            });
    }
    handleSubmit(event) {
        event.preventDefault();
        this.updateDataHiring()
    }
    updateDataHiring() {
        putDatas('http://209.97.160.40:8080/SA061118/project/mengelolaHiring/updateSdm?sdmhiring_id=' + this.state.sdmHiringId + '&hirestat_id=' + this.state.statusId + '&client_id=' + this.state.clientId, body)
            .then((result) => {
                this.setState({
                    clientName: '',
                    sdmName: '',
                    sdmHiringId: '',
                    clientId: '',
                    statusId: '',
                })
                window.location = "#/projectassignment/hiring"
                localStorage.setItem("messageHiring", result.message)
            })
    }
    handleChange(e) {
        this.setState({
            statusId: e.target.value,
            isChange: true
        })
    }
    render() {
        return (
            <div>
                <Card className="rounded-0 shadow-sm animated fadeIn">
                    <CardBody className="px-5 py-5">
                        <Form onSubmit={this.handleSubmit}>
                            <FormGroup row>
                                <Label sm={{ size: 3, offset: 1 }} className="text-right">Client Name</Label>
                                <Col sm={{ size: 5 }}>
                                    <Input type="text" className="rounded-0" value={this.state.clientName} readOnly />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 3, offset: 1 }} className="text-right">SDM Name</Label>
                                <Col sm={{ size: 5 }}>
                                    <Input type="text" className="rounded-0" value={this.state.sdmName} readOnly />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 3, offset: 1 }} className="text-right">Status</Label>
                                <Col sm={{ size: 5 }}>
                                    <Input type="select" className="rounded-0" onChange={this.handleChange} value={this.state.statusId}>
                                        {this.state.dataLovStatus.map(status =>
                                            <option key={status.key} value={status.key}>{status.values.statushiring_hirestat_name}</option>
                                        )}
                                    </Input>
                                </Col>
                            </FormGroup>
                            <Row>
                                <Col sm={{ size: 3, offset: 3 }}>
                                    {
                                        !this.state.isChange ? <Button className="rounded-0" block outline type="submit" color="primary" disabled>Update</Button> :
                                            <Button className="rounded-0" block outline type="submit" color="primary">Update</Button>
                                    }
                                </Col>
                                <Col sm={{ size: 3 }}>
                                    <Link to="/projectassignment/hiring" className="btn btn-outline-danger btn-block rounded-0" role="button">
                                        Back
                                    </Link>
                                </Col>
                            </Row>
                        </Form>
                    </CardBody>
                </Card>
            </div>
        );
    }
}

export default UpdateHiring;