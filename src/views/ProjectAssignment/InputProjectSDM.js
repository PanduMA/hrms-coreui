import React, { Component } from 'react';
import Autosuggest from 'react-autosuggest';
import AutosuggestHighlightMatch from 'autosuggest-highlight/match';
import AutosuggestHighlightParse from 'autosuggest-highlight/parse';
import { getDatas, postDatas } from '../../services/APIFunction';
import {
    Card, CardHeader, CardBody,
    FormGroup, Label, Input, Button, Form,
    Row, Col, Alert,
} from 'reactstrap';
import moment from 'moment';
let body
class InputProjectSDM extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            value: '',
            sdmKey: '',
            suggestions: [],
            dataSDMNames: [],
            roleProject: '',
            nik: '',
            appType: '',
            projectName: '',
            devLang: '',
            customer: '',
            serverOS: '',
            projectSite: '',
            framework: '',
            periodStart: '',
            database: '',
            periodEnd: '',
            devTools: '',
            projectDesc: '',
            otherInfo: '',
            appServer: '',
            technicalInfo: '',
            message: '',
            visible: false,
        })
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleClick = this.handleClick.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.onDismiss = this.onDismiss.bind(this)
    }
    onDismiss() {
        this.setState({ visible: false });
    }
    componentDidMount() {
        this.getDataSDMName()
    }
    getDataSDMName() {
        getDatas('http://209.97.160.40:8080/SA061118/lov/Sdm?$orderby=sdm_name%20ASC')
            .then((result) => {
                this.setState({
                    dataSDMNames: result.data
                })
            });
    }
    getDataNik(id) {
        getDatas('http://209.97.160.40:8080/SA061118/lov/SdmNik?$filter=sdm_id=' + id)
            .then(result => {
                this.setState({
                    nik: Object.values(result.data)[0]['values']['sdm_sdm_nik']
                })
            })
    }
    getSuggestions = value => {
        const inputValue = value.trim().toLowerCase();
        const inputLength = inputValue.length;

        return inputLength === 0 ? [] : this.state.dataSDMNames.filter(sdm =>
            sdm.values.sdm_sdm_name.toLowerCase().slice(0, inputLength) === inputValue
        );
    };
    getSuggestionValue = suggestion => suggestion.values.sdm_sdm_name;
    onChange = (event, { newValue }) => {
        if (newValue === '') {
            this.setState({ nik: '', value: '' })
        } else {
            this.setState({ value: newValue });
        }
        var result = this.state.dataSDMNames.find(sdm => sdm.values.sdm_sdm_name === newValue)
        if (result) {
            this.setState({ sdmKey: Object.values(result)[1] })
            this.getDataNik(Object.values(result)[1])
        }
    };
    onSuggestionsFetchRequested = ({ value }) => {
        this.setState({
            suggestions: this.getSuggestions(value)
        });
    };
    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };
    renderSuggestion = (suggestion, { query }) => {
        const matches = AutosuggestHighlightMatch(suggestion.values.sdm_sdm_name, query)
        const parts = AutosuggestHighlightParse(suggestion.values.sdm_sdm_name, matches)
        return (
            <span>
                {parts.map((part, index) => {
                    const className = part.highlight ? 'react-autosuggest__suggestion-match' : null;
                    return (
                        <span className={className} key={index}>
                            {part.text}
                        </span>
                    );
                })}
            </span>
        );
    }
    handleSubmit(event) {
        body = JSON.stringify({
            sdm_nik: this.state.nik,
            sdm_name: this.state.value,
            sdm_id: parseInt(this.state.sdmKey),
            project_technicalinfo: this.state.technicalInfo,
            project_startdate: this.state.periodStart,
            project_serveros: this.state.serverOS,
            project_role: this.state.roleProject,
            project_projectsite: this.state.projectSite,
            project_otherinfo: this.state.otherInfo,
            project_name: this.state.projectName,
            project_framework: this.state.framework,
            project_enddate: this.state.periodEnd,
            project_devtool: this.state.devTools,
            project_devlanguage: this.state.devLang,
            project_desc: this.state.projectDesc,
            project_database: this.state.database,
            project_customer: this.state.customer,
            project_apptype: this.state.appType,
            project_appserver: this.state.appServer
        })
        this.createDataProject(body)
        event.preventDefault()
    }
    createDataProject(body) {
        postDatas('http://209.97.160.40:8080/SA061118/project/MengelolaProject/create', body)
            .then(result => {
                this.setState({
                    message: result.message,
                    visible: true,
                    value: '',
                    sdmKey: '',
                    roleProject: '',
                    nik: '',
                    appType: '',
                    projectName: '',
                    devLang: '',
                    customer: '',
                    serverOS: '',
                    projectSite: '',
                    framework: '',
                    periodStart: '',
                    database: '',
                    periodEnd: '',
                    devTools: '',
                    projectDesc: '',
                    otherInfo: '',
                    appServer: '',
                    technicalInfo: '',
                })
                this.setTimeoutAlert()
            })
    }
    handleClick() {
        this.setState({
            value: '',
            sdmKey: '',
            roleProject: '',
            nik: '',
            appType: '',
            projectName: '',
            devLang: '',
            customer: '',
            serverOS: '',
            projectSite: '',
            framework: '',
            periodStart: '',
            database: '',
            periodEnd: '',
            devTools: '',
            projectDesc: '',
            otherInfo: '',
            appServer: '',
            technicalInfo: '',
        })
    }
    handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    setTimeoutAlert() {
        setTimeout(() => {
            this.setState({ visible: false })
        }, 3000);
    }
    render() {
        const { value, suggestions } = this.state;
        const inputProps = {
            placeholder: 'Enter SDM Name',
            className: 'form-control rounded-0',
            value,
            onChange: this.onChange,
            required: true
        };
        return (
            <div>
                <Alert color="success" isOpen={this.state.visible} toggle={this.onDismiss} className="shadow-sm">
                    {this.state.message}
                </Alert>
                <Card className="rounded-0 shadow-sm animated fadeIn">
                    <CardHeader>
                        <h4>Add Project SDM</h4>
                    </CardHeader>
                    <CardBody className="px-5">
                        <Form onSubmit={this.handleSubmit}>
                            <FormGroup row>
                                <Label sm={{ size: 2 }} className="text-right">
                                    <i className="text-danger">*</i>SDM Name :
                                </Label>
                                <Col sm={{ size: 4 }}>
                                    <Autosuggest
                                        suggestions={suggestions}
                                        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                                        onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                                        getSuggestionValue={this.getSuggestionValue}
                                        renderSuggestion={this.renderSuggestion}
                                        inputProps={inputProps}
                                    />
                                </Col>
                                <Label sm={{ size: 2 }} className="text-right">
                                    <i className="text-danger">*</i>Role Project :
                                </Label>
                                <Col sm={{ size: 4 }}>
                                    <Input
                                        type="text"
                                        className="rounded-0"
                                        name="roleProject"
                                        placeholder="Enter Role Project"
                                        value={this.state.roleProject}
                                        onChange={this.handleChange}
                                        required />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 2 }} className="text-right">NIK :</Label>
                                <Col sm={{ size: 4 }}>
                                    <Input type="text" className="rounded-0" name="nik" placeholder="NIK Generated" value={this.state.nik} readOnly />
                                </Col>
                                <Label sm={{ size: 2 }} className="text-right">App Type :</Label>
                                <Col sm={{ size: 4 }}>
                                    <Input
                                        type="text"
                                        className="rounded-0"
                                        name="appType"
                                        placeholder="Enter App Type"
                                        value={this.state.appType}
                                        onChange={this.handleChange} />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 2 }} className="text-right">
                                    <i className="text-danger">*</i>Project Name :
                                </Label>
                                <Col sm={{ size: 4 }}>
                                    <Input
                                        type="text"
                                        className="rounded-0"
                                        name="projectName"
                                        placeholder="Enter Project Name"
                                        value={this.state.projectName}
                                        onChange={this.handleChange}
                                        required />
                                </Col>
                                <Label sm={{ size: 2 }} className="text-right">Dev Language :</Label>
                                <Col sm={{ size: 4 }}>
                                    <Input
                                        type="text"
                                        className="rounded-0"
                                        name="devLang"
                                        placeholder="Enter Dev Language"
                                        value={this.state.devLang}
                                        onChange={this.handleChange} />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 2 }} className="text-right">Customer :</Label>
                                <Col sm={{ size: 4 }}>
                                    <Input
                                        type="text"
                                        className="rounded-0"
                                        name="customer"
                                        placeholder="Enter Customer"
                                        value={this.state.customer}
                                        onChange={this.handleChange} />
                                </Col>
                                <Label sm={{ size: 2 }} className="text-right">Server OS :</Label>
                                <Col sm={{ size: 4 }}>
                                    <Input
                                        type="text"
                                        className="rounded-0"
                                        name="serverOS"
                                        placeholder="Enter Server OS"
                                        value={this.state.serverOS}
                                        onChange={this.handleChange} />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 2 }} className="text-right">
                                    <i className="text-danger">*</i>Project Site :
                                </Label>
                                <Col sm={{ size: 4 }}>
                                    <Input
                                        type="text"
                                        className="rounded-0"
                                        name="projectSite"
                                        placeholder="Enter Project Site"
                                        value={this.state.projectSite}
                                        onChange={this.handleChange}
                                        required />
                                </Col>
                                <Label sm={{ size: 2 }} className="text-right">Framework :</Label>
                                <Col sm={{ size: 4 }}>
                                    <Input
                                        type="text"
                                        className="rounded-0"
                                        name="framework"
                                        placeholder="Enter Framework"
                                        value={this.state.framework}
                                        onChange={this.handleChange} />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 2 }} className="text-right">
                                    <i className="text-danger">*</i>Period Start :
                                </Label>
                                <Col sm={{ size: 4 }}>
                                    <Input
                                        type="date"
                                        className="rounded-0"
                                        name="periodStart"
                                        value={this.state.periodStart}
                                        onChange={this.handleChange}
                                        required />
                                </Col>
                                <Label sm={{ size: 2 }} className="text-right">Database :</Label>
                                <Col sm={{ size: 4 }}>
                                    <Input
                                        type="text"
                                        className="rounded-0"
                                        name="database"
                                        placeholder="Enter Database"
                                        value={this.state.database}
                                        onChange={this.handleChange} />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 2 }} className="text-right">
                                    <i className="text-danger">*</i>Period End :
                                </Label>
                                <Col sm={{ size: 4 }}>
                                    <Input
                                        type="date"
                                        className="rounded-0"
                                        name="periodEnd"
                                        value={this.state.periodEnd}
                                        min={moment(this.state.periodStart).add(1, 'days').format('YYYY-MM-DD')}
                                        onChange={this.handleChange}
                                        required />
                                </Col>
                                <Label sm={{ size: 2 }} className="text-right">Dev Tools :</Label>
                                <Col sm={{ size: 4 }}>
                                    <Input
                                        type="text"
                                        className="rounded-0"
                                        name="devTools"
                                        placeholder="Enter Dev Tools"
                                        value={this.state.devTools}
                                        onChange={this.handleChange} />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 2 }} className="text-right">
                                    <i className="text-danger">*</i>Project Desc :
                                </Label>
                                <Col sm={{ size: 4 }}>
                                    <Input
                                        type="textarea"
                                        className="rounded-0"
                                        name="projectDesc"
                                        placeholder="Enter Project Desc"
                                        value={this.state.projectDesc}
                                        onChange={this.handleChange}
                                        required />
                                </Col>
                                <Label sm={{ size: 2 }} className="text-right">Other Info :</Label>
                                <Col sm={{ size: 4 }}>
                                    <Input
                                        type="textarea"
                                        className="rounded-0"
                                        name="otherInfo"
                                        placeholder="Enter Other Info"
                                        value={this.state.otherInfo}
                                        onChange={this.handleChange} />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 2 }} className="text-right">App Server :</Label>
                                <Col sm={{ size: 4 }}>
                                    <Input
                                        type="text"
                                        className="rounded-0"
                                        name="appServer"
                                        placeholder="Enter App Server"
                                        value={this.state.appServer}
                                        onChange={this.handleChange} />
                                </Col>
                                <Label sm={{ size: 2 }} className="text-right">Technical Info :</Label>
                                <Col sm={{ size: 4 }}>
                                    <Input
                                        type="textarea"
                                        className="rounded-0"
                                        name="technicalInfo"
                                        placeholder="Enter Technical Info"
                                        value={this.state.technicalInfo}
                                        onChange={this.handleChange}
                                    />
                                </Col>
                            </FormGroup>
                            <Row>
                                <Col sm={{ size: 3, offset: 3 }}>
                                    <Button className="rounded-0" type="submit" outline color="primary" block>
                                        Submit
                                    </Button>
                                </Col>
                                <Col sm={{ size: 3 }}>
                                    <Button className="rounded-0" type="reset" outline color="danger" block onClick={this.handleClick}>
                                        Reset
                                    </Button>
                                </Col>
                            </Row>
                        </Form>
                    </CardBody>
                </Card>
            </div>
        );
    }
}

export default InputProjectSDM;