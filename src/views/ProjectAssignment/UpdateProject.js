import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { putDatas, getDatasFinal } from '../../services/APIFunction';
import {
    Card, CardBody,
    Form, FormGroup, Label, Input, Button,
    Row, Col,
} from 'reactstrap';
import moment from 'moment';
let body
class UpdateProject extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            dataProjects: [],
            isChange: false,
            sdmName: '',
            roleProject: '',
            nik: '',
            appType: '',
            projectName: '',
            devLang: '',
            customer: '',
            serverOS: '',
            projectSite: '',
            framework: '',
            periodStart: '',
            database: '',
            periodEnd: '',
            devTools: '',
            projectDesc: '',
            otherInfo: '',
            appServer: '',
            technicalInfo: ''
        })
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    componentDidMount() {
        this.getDataProjects(this.props.match.params.id)
    }
    getDataProjects(id) {
        var paramFilter = 'project_id=' + id
        getDatasFinal('http://209.97.160.40:8080/SA061118/project/MengelolaProject/readAll', '', '', paramFilter)
            .then((result) => {
                this.setState({
                    dataProjects: result.data.items
                })
                this.state.dataProjects.map(project => {
                    this.setState({
                        sdmName: project.sdm_name,
                        roleProject: project.project_role,
                        nik: project.sdm_nik,
                        appType: project.project_apptype,
                        projectName: project.project_name,
                        devLang: project.project_devlanguage,
                        customer: project.project_customer,
                        serverOS: project.project_serveros,
                        projectSite: project.project_projectsite,
                        framework: project.project_framework,
                        periodStart: project.project_startdate,
                        database: project.project_database,
                        periodEnd: project.project_enddate,
                        devTools: project.project_devtool,
                        projectDesc: project.project_desc,
                        otherInfo: project.project_otherinfo,
                        appServer: project.project_appserver,
                        technicalInfo: project.project_technicalinfo

                    })
                })
            });
    }
    handleChange(e) {
        if (e.target.name === 'roleProject') this.setState({ roleProject: e.target.value })
        else if (e.target.name === 'appType') this.setState({ appType: e.target.value })
        else if (e.target.name === 'projectName') this.setState({ projectName: e.target.value })
        else if (e.target.name === 'devLang') this.setState({ devLang: e.target.value })
        else if (e.target.name === 'customer') this.setState({ customer: e.target.value })
        else if (e.target.name === 'serverOS') this.setState({ serverOS: e.target.value })
        else if (e.target.name === 'projectSite') this.setState({ projectSite: e.target.value })
        else if (e.target.name === 'framework') this.setState({ framework: e.target.value })
        else if (e.target.name === 'periodStart') this.setState({ periodStart: e.target.value })
        else if (e.target.name === 'database') this.setState({ database: e.target.value })
        else if (e.target.name === 'periodEnd') this.setState({ periodEnd: e.target.value })
        else if (e.target.name === 'devTools') this.setState({ devTools: e.target.value })
        else if (e.target.name === 'projectDesc') this.setState({ projectDesc: e.target.value })
        else if (e.target.name === 'otherInfo') this.setState({ otherInfo: e.target.value })
        else if (e.target.name === 'appServer') this.setState({ appServer: e.target.value })
        else if (e.target.name === 'technicalInfo') this.setState({ technicalInfo: e.target.value })
        this.setState({ isChange: true })
    }
    handleSubmit(event) {
        body = JSON.stringify({
            project_appserver: this.state.appServer,
            project_apptype: this.state.appType,
            project_customer: this.state.customer,
            project_database: this.state.database,
            project_desc: this.state.projectDesc,
            project_devlanguage: this.state.devLang,
            project_devtool: this.state.devTools,
            project_enddate: this.state.periodEnd,
            project_framework: this.state.framework,
            project_id: parseInt(this.props.match.params.id),
            project_name: this.state.projectName,
            project_otherinfo: this.state.otherInfo,
            project_projectsite: this.state.projectSite,
            project_role: this.state.roleProject,
            project_serveros: this.state.serverOS,
            project_startdate: this.state.periodStart,
            project_technicalinfo: this.state.technicalInfo
        })
        event.preventDefault()
        this.sendUpdateProject(body)
    }
    sendUpdateProject(body) {
        putDatas('http://209.97.160.40:8080/SA061118/project/MengelolaProject/update?project_id=' + parseInt(this.props.match.params.id), body)
            .then((result) => {
                this.setState({
                    isChange: false,
                    sdmName: '',
                    roleProject: '',
                    nik: '',
                    appType: '',
                    projectName: '',
                    devLang: '',
                    customer: '',
                    serverOS: '',
                    projectSite: '',
                    framework: '',
                    periodStart: '',
                    database: '',
                    periodEnd: '',
                    devTools: '',
                    projectDesc: '',
                    otherInfo: '',
                    appServer: '',
                    technicalInfo: ''
                })
                window.location = "#/projectassignment/list"
                localStorage.setItem("messageProject", result.message)
            })
    }
    render() {
        return (
            <div>
                <Card className="rounded-0 animated fadeIn">
                    <CardBody className="px-5">
                        <Form onSubmit={this.handleSubmit}>
                            <FormGroup row>
                                <Label sm={{ size: 2 }} className="text-right">SDM Name :</Label>
                                <Col sm={{ size: 4 }}>
                                    <Input value={this.state.sdmName} className="rounded-0" type="text" name="sdmName" readOnly required />
                                </Col>
                                <Label sm={{ size: 2 }} className="text-right">Role Project :</Label>
                                <Col sm={{ size: 4 }}>
                                    <Input value={this.state.roleProject} className="rounded-0" type="text" name="roleProject" required onChange={this.handleChange} />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 2 }} className="text-right">NIK :</Label>
                                <Col sm={{ size: 4 }}>
                                    <Input value={this.state.nik} className="rounded-0" type="text" name="nik" readOnly required />
                                </Col>
                                <Label sm={{ size: 2 }} className="text-right">App Type :</Label>
                                <Col sm={{ size: 4 }}>
                                    <Input value={this.state.appType} className="rounded-0" type="text" name="appType" onChange={this.handleChange} />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 2 }} className="text-right">Project Name :</Label>
                                <Col sm={{ size: 4 }}>
                                    <Input value={this.state.projectName} className="rounded-0" type="text" name="projectName" required onChange={this.handleChange} />
                                </Col>
                                <Label sm={{ size: 2 }} className="text-right">Dev Language :</Label>
                                <Col sm={{ size: 4 }}>
                                    <Input value={this.state.devLang} className="rounded-0" type="text" name="devLang" onChange={this.handleChange} />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 2 }} className="text-right">Customer :</Label>
                                <Col sm={{ size: 4 }}>
                                    <Input value={this.state.customer} className="rounded-0" type="text" name="customer" onChange={this.handleChange} />
                                </Col>
                                <Label sm={{ size: 2 }} className="text-right">Server OS :</Label>
                                <Col sm={{ size: 4 }}>
                                    <Input value={this.state.serverOS} className="rounded-0" type="text" name="serverOS" onChange={this.handleChange} />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 2 }} className="text-right">Project Site :</Label>
                                <Col sm={{ size: 4 }}>
                                    <Input value={this.state.projectSite} className="rounded-0" type="text" name="projectSite" required onChange={this.handleChange} />
                                </Col>
                                <Label sm={{ size: 2 }} className="text-right">Framework :</Label>
                                <Col sm={{ size: 4 }}>
                                    <Input value={this.state.framework} className="rounded-0" type="text" name="framework" onChange={this.handleChange} />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 2 }} className="text-right">Period Start :</Label>
                                <Col sm={{ size: 4 }}>
                                    <Input value={this.state.periodStart} className="rounded-0" type="date" name="periodStart" required onChange={this.handleChange} />
                                </Col>
                                <Label sm={{ size: 2 }} className="text-right">Database :</Label>
                                <Col sm={{ size: 4 }}>
                                    <Input value={this.state.database} className="rounded-0" type="text" name="database" onChange={this.handleChange} />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 2 }} className="text-right">Period End :</Label>
                                <Col sm={{ size: 4 }}>
                                    <Input value={this.state.periodEnd} type="date" min={moment(this.state.periodStart).add(1, 'days').format('YYYY-MM-DD')} name="periodEnd" required onChange={this.handleChange} />
                                </Col>
                                <Label sm={{ size: 2 }} className="text-right">Dev Tools :</Label>
                                <Col sm={{ size: 4 }}>
                                    <Input value={this.state.devTools} type="text" name="devTools" onChange={this.handleChange} />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 2 }} className="text-right">Project Desc :</Label>
                                <Col sm={{ size: 4 }}>
                                    <Input value={this.state.projectDesc} className="rounded-0" type="textarea" name="projectDesc" required onChange={this.handleChange} />
                                </Col>
                                <Label sm={{ size: 2 }} className="text-right">Other Info :</Label>
                                <Col sm={{ size: 4 }}>
                                    <Input value={this.state.otherInfo} className="rounded-0" type="textarea" name="otherInfo" onChange={this.handleChange} />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 2 }} className="text-right">App Server :</Label>
                                <Col sm={{ size: 4 }}>
                                    <Input value={this.state.appServer} className="rounded-0" type="text" name="appServer" onChange={this.handleChange} />
                                </Col>
                                <Label sm={{ size: 2 }} className="text-right">Technical Info :</Label>
                                <Col sm={{ size: 4 }}>
                                    <Input value={this.state.technicalInfo} className="rounded-0" type="textarea" name="technicalInfo" onChange={this.handleChange} />
                                </Col>
                            </FormGroup>
                            <Row>
                                <Col sm={{ size: 3, offset: 3 }}>
                                    {!this.state.isChange ?
                                        <Button className="rounded-0" block outline color="primary" disabled>Submit</Button> :
                                        <Button className="rounded-0" block outline type="submit" color="primary">Submit</Button>}
                                </Col>
                                <Col sm={{ size: 3 }}>
                                    <Link to={`/projectassignment/list`} style={{ textDecoration: 'none' }}>
                                        <Button className="rounded-0" outline color="danger" block>Back</Button>
                                    </Link>
                                </Col>
                            </Row>
                        </Form>
                    </CardBody>
                </Card>
            </div>
        );
    }
}

export default UpdateProject;