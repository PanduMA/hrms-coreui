import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Autosuggest from 'react-autosuggest';
import AutosuggestHighlightMatch from 'autosuggest-highlight/match';
import AutosuggestHighlightParse from 'autosuggest-highlight/parse';
import { deleteDatas, getDatasFinal } from '../../services/APIFunction';
import {
    Row, Col,
    Card, CardBody,
    Table,
    FormGroup, Label, Input, Button,
    Pagination, PaginationItem, PaginationLink,
    Modal, ModalHeader, ModalBody, ModalFooter,
    Alert, UncontrolledTooltip
} from 'reactstrap';
import ModalBodyContent from './ModalBodyContent';
let skip = 0, filter, total, messageProject
class ListDetailProjectSDM extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            value: '',
            sdmKey: '',
            suggestions: [],
            dataSDMNames: [],
            valueProject: '',
            suggestionsProject: [],
            dataProjectNames: [],
            project: '',
            name: '',
            endDate: '',
            nameButton: '',
            dataProjects: [],
            totalItems: '',
            projectId: '',
            buttonId: '',
            modal: false,
            visible: false,
            message: ''
        });
        this.handleClick = this.handleClick.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.toggle = this.toggle.bind(this);
        this.onDismiss = this.onDismiss.bind(this);
    }
    onDismiss() {
        localStorage.removeItem("messageProject")
        this.setState({ visible: false });
    }
    toggle(e) {
        if (e.target.id === 'yes') {
            deleteDatas('http://209.97.160.40:8080/SA061118/project/MengelolaProject/delete?project_id=' + this.state.projectId)
                .then((result) => {
                    this.setState({
                        message: result.message,
                        projectId: '',
                        visible: true
                    })
                    this.getDataProjects(filter = '', skip = 0)
                    this.setTimeoutAlert()
                })
        }
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
    }
    componentDidMount() {
        this.setState({
            endDate: '',
            sdmKey: '',
            value: '',
            valueProject: '',
        })
        skip = 0
        filter = ''
        this.getDataProjects(filter, skip)
        this.getDataSDMName()
        this.getDataProjectName()
        messageProject = localStorage.getItem("messageProject")
        if (messageProject !== null) {
            this.setState({ visible: true, message: messageProject })
            this.setTimeoutAlert()
            localStorage.removeItem("messageProject")
        }
    }
    getDataProjects(filter, skip) {
        if (filter != null) {
            getDatasFinal('http://209.97.160.40:8080/SA061118/project/MengelolaProject/readAll', skip, 10, filter)
                .then((result) => {
                    this.setState({
                        dataProjects: result.data.items,
                        totalItems: result.data.totalItems
                    })
                });
        } else {
            getDatasFinal('http://209.97.160.40:8080/SA061118/project/MengelolaProject/readAll', skip, 10)
                .then((result) => {
                    this.setState({
                        dataProjects: result.data.items,
                        totalItems: result.data.totalItems
                    })
                });
        }
    }
    getDataSDMName() {
        var paramOrderBy = 'sdm_name ASC'
        getDatasFinal('http://209.97.160.40:8080/SA061118/lov/Sdm', '', '', '', paramOrderBy)
            .then((result) => {
                this.setState({
                    dataSDMNames: result.data
                })
            });
    }
    getDataProjectName() {
        getDatasFinal('http://209.97.160.40:8080/SA061118/lov/Project')
            .then((result) => {
                this.setState({
                    dataProjectNames: result.data
                })
            });
    }
    handleChange(e) {
        if (e.target.value === '') {
            this.setState({
                endDate: '',
            })
        } else {
            this.setState({
                endDate: e.target.value,
            })
        }

    }
    handleClickAction = (e, param) => {
        e.preventDefault();
        if (e.target.id === 'view-sdm') {
            this.setState({ projectId: param, buttonId: e.target.id })
            this.setState(prevState => ({
                modal: !prevState.modal
            }));
        } else if (e.target.id === 'edit-sdm') {

        } else {
            this.setState({ projectId: param, buttonId: e.target.id })
            this.setState(prevState => ({
                modal: !prevState.modal
            }));
        }
    }
    handleClick(e) {
        e.preventDefault();
        this.setState({
            nameButton: e.target.name
        })
        if (e.target.name === 'clear') { // do clear click button
            this.setState({
                endDate: '',
                sdmKey: '',
                value: '',
                valueProject: '',
            })
            skip = 0
            filter = ''
            this.getDataProjects(filter, skip)
        }
        if (e.target.name === 'pagination') { // do when click pagination 
            skip = e.target.value
            this.getDataProjects(filter, skip)
        }
        if (e.target.name === 'filter') { // do filter click button
            skip = 0
            if (this.state.sdmKey !== '') { filter = 'sdm_id=' + this.state.sdmKey }
            if (this.state.valueProject !== '') { filter = 'project_name="' + this.state.valueProject + '"' }
            if (this.state.endDate !== '') { filter = 'project_enddate<="' + this.state.endDate + '"' }
            if (this.state.sdmKey !== '' && this.state.valueProject !== '') {
                filter = '(sdm_id=' + this.state.sdmKey + ' AND project_name="' + this.state.valueProject + '")'
            }
            if (this.state.sdmKey !== '' && this.state.endDate !== '') {
                filter = '(sdm_id=' + this.state.sdmKey + ' AND project_enddate<="' + this.state.endDate + '")'
            }
            if (this.state.valueProject !== '' && this.state.endDate !== '') {
                filter = '(project_name="' + this.state.valueProject + '" AND project_enddate<="' + this.state.endDate + '")'
            }
            if (this.state.sdmKey !== '' && this.state.valueProject !== '' && this.state.endDate !== '') {
                filter = '(sdm_id=' + this.state.sdmKey + ' AND project_name="' + this.state.valueProject + '" AND project_enddate<="' + this.state.endDate + '")'
            }
            this.getDataProjects(filter, skip)
        }
        if (e.target.name === 'previous') {
            skip = 0
            this.getDataProjects(filter, skip)
        }
        if (e.target.name === 'next') {
            skip = parseInt(total) - 1
            this.getDataProjects(filter, skip)
        }
    }
    getDataProjectsFilter(filter) {
        getDatasFinal('http://209.97.160.40:8080/SA061118/project/MengelolaProject/readAll', skip, 10, filter)
            .then((result) => {
                this.setState({
                    dataProjects: result.data.items,
                    totalItems: result.data.totalItems
                })
            });
    }
    setTimeoutAlert() {
        setTimeout(() => {
            this.setState({ visible: false })
        }, 3000);
    }
    createPagination = () => {
        let element = []
        total = Math.ceil(this.state.totalItems / 10)
        if (total > 1) {
            for (let index = 0; index < total; index++) {
                element.push(
                    <PaginationItem key={index} className={parseInt(skip) === index ? 'active' : ''}>
                        <PaginationLink value={index} onClick={this.handleClick} name="pagination">
                            {index + 1}
                        </PaginationLink>
                    </PaginationItem>
                )
            }
            if (total > 5) {
                if (skip < 3) {
                    return element.slice(0, 5)
                } else {
                    var startMove = parseInt(skip) - 2
                    var endMove = parseInt(skip) + 3
                    if (startMove > (total - 5) && endMove > total) {
                        startMove = total - 5
                        endMove = total
                    }
                    return element.slice(startMove, endMove)
                }
            } else {
                return element.slice(0, 5)
            }
        }
    }
    getSuggestions = value => {
        const inputValue = value.trim().toLowerCase();
        const inputLength = inputValue.length;

        return inputLength === 0 ? [] : this.state.dataSDMNames.filter(sdm =>
            sdm.values.sdm_sdm_name.toLowerCase().slice(0, inputLength) === inputValue
        );
    };
    getSuggestionValue = suggestion => suggestion.values.sdm_sdm_name;
    onChange = (event, { newValue }) => {
        this.setState({
            value: newValue,
        });
        var result = this.state.dataSDMNames.find(sdm => sdm.values.sdm_sdm_name === newValue)
        if (result) {
            this.setState({ sdmKey: Object.values(result)[1] })
        } else {
            this.setState({ sdmKey: '' })
        }
    };
    onSuggestionsFetchRequested = ({ value }) => {
        this.setState({
            suggestions: this.getSuggestions(value)
        });
    };
    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };
    renderSuggestion = (suggestion, { query }) => {
        const matches = AutosuggestHighlightMatch(suggestion.values.sdm_sdm_name, query)
        const parts = AutosuggestHighlightParse(suggestion.values.sdm_sdm_name, matches)
        return (
            <span>
                {parts.map((part, index) => {
                    const className = part.highlight ? 'react-autosuggest__suggestion-match' : null;
                    return (
                        <span className={className} key={index}>
                            {part.text}
                        </span>
                    );
                })}
            </span>
        );
    }
    getSuggestionsProject = value => {
        const inputValue = value.trim().toLowerCase();
        const inputLength = inputValue.length;

        return inputLength === 0 ? [] : this.state.dataProjectNames.filter(project =>
            project.values.project_project_name.toLowerCase().slice(0, inputLength) === inputValue
        );
    };
    getSuggestionValueProject = suggestionsProject => suggestionsProject.values.project_project_name;
    onChangeProject = (event, { newValue }) => {
        if (newValue) {
            this.setState({
                valueProject: newValue,
            });
        } else {
            this.setState({
                valueProject: '',
            });
        }
        this.state.dataProjectNames.find(project => project.values.project_project_name === newValue)
    };
    onSuggestionsFetchRequestedProject = ({ value }) => {
        this.setState({
            suggestionsProject: this.getSuggestionsProject(value)
        });
    };
    onSuggestionsClearRequestedProject = () => {
        this.setState({
            suggestionsProject: []
        });
    };
    renderSuggestionProject = (suggestionsProject, { query }) => {
        const matches = AutosuggestHighlightMatch(suggestionsProject.values.project_project_name, query)
        const parts = AutosuggestHighlightParse(suggestionsProject.values.project_project_name, matches)
        return (
            <span>
                {parts.map((part, index) => {
                    const className = part.highlight ? 'react-autosuggest__suggestion-match' : null;
                    return (
                        <span className={className} key={index}>
                            {part.text}
                        </span>
                    );
                })}
            </span>
        );
    }
    render() {
        const { value, suggestions, valueProject, suggestionsProject } = this.state;
        const inputProps = {
            placeholder: 'Enter SDM Name',
            className: 'form-control rounded-0',
            value,
            onChange: this.onChange
        };
        const inputPropsProject = {
            placeholder: 'Enter Project Name',
            className: 'form-control rounded-0',
            value: valueProject,
            onChange: this.onChangeProject
        };
        return (
            <div>
                <Alert color="success" isOpen={this.state.visible} toggle={this.onDismiss} className="shadow-sm">
                    {this.state.message}
                </Alert>
                <Row>
                    <Col sm={{ size: 12 }}>
                        <Card className="rounded-0 shadow-sm animated fadeIn">
                            <CardBody className="px-5 py-5">
                                <Row>
                                    <Col sm={{ size: 4 }}>
                                        <FormGroup>
                                            <Label>Name : </Label>
                                            <Autosuggest
                                                suggestions={suggestions}
                                                onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                                                onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                                                getSuggestionValue={this.getSuggestionValue}
                                                renderSuggestion={this.renderSuggestion}
                                                inputProps={inputProps}
                                            />
                                        </FormGroup>
                                    </Col>
                                    <Col sm={{ size: 4 }}>
                                        <FormGroup>
                                            <Label>Project Name : </Label>
                                            <Autosuggest
                                                suggestions={suggestionsProject}
                                                onSuggestionsFetchRequested={this.onSuggestionsFetchRequestedProject}
                                                onSuggestionsClearRequested={this.onSuggestionsClearRequestedProject}
                                                getSuggestionValue={this.getSuggestionValueProject}
                                                renderSuggestion={this.renderSuggestionProject}
                                                inputProps={inputPropsProject}
                                            />
                                        </FormGroup>
                                    </Col>
                                    <Col sm={{ size: 4 }}>
                                        <FormGroup>
                                            <Label>End Date : </Label>
                                            <Input type="date" className="rounded-0" onChange={this.handleChange} value={this.state.endDate} name="endDate" />
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={{ size: 2 }}>
                                        <Button className="rounded-0" outline color="primary" block onClick={this.handleClick} name="filter">
                                            <i className="fa fa-fw fa-search" /> Filter
                                        </Button>
                                    </Col>
                                    <Col sm={{ size: 2 }}>
                                        <Button className="rounded-0" outline color="danger" block onClick={this.handleClick} name="clear">
                                            <i className="fa fa-fw fa-close" /> Clear
                                        </Button>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
                <Row>
                    <Col sm={{ size: 12 }}>
                        <Card className="rounded-0 shadow-sm animated fadeIn">
                            <CardBody>
                                <div className="clearfix px-3">
                                    <div className="float-right caption-color">
                                        <p>Total : {this.state.totalItems}</p>
                                    </div>
                                </div>
                                <Table responsive hover bordered className="text-center">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Name SDM</th>
                                            <th>Project Name</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Notification Project</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.dataProjects.length < 1 ? <tr><td colSpan="7">0 data display</td></tr> :
                                            this.state.dataProjects.map((project, index) =>
                                                <tr key={project.project_id}>
                                                    <th>{project.norut}</th>
                                                    <td>{project.sdm_name}</td>
                                                    <td>{project.project_name}</td>
                                                    <td>{project.project_startdate}</td>
                                                    <td>{project.project_enddate}</td>
                                                    <td>
                                                        {(project.project_notification === 'black') ?
                                                            <div>
                                                                <i className="fa fa-warning text-dark fa-2x" id={"black-" + index}></i>
                                                                <UncontrolledTooltip placement="bottom" target={"black-" + index}>
                                                                    Non-Aktif
                                                                </UncontrolledTooltip>
                                                            </div>
                                                            : (project.project_notification === 'grey') ?
                                                                <div>
                                                                    <i className="fa fa-warning text-secondary fa-2x" id={"grey-" + index}></i>
                                                                    <UncontrolledTooltip placement="bottom" target={"grey-" + index}>
                                                                        Sisa lebih dari 4 bulan
                                                                </UncontrolledTooltip>
                                                                </div>
                                                                : (project.project_notification === 'red') ?
                                                                    <div>
                                                                        <i className="fa fa-warning text-danger fa-2x" id={"red-" + index}></i>
                                                                        <UncontrolledTooltip placement="bottom" target={"red-" + index}>
                                                                            Sisa 1 bulan
                                                                </UncontrolledTooltip>
                                                                    </div>
                                                                    : 'Belum Ada'
                                                        }
                                                    </td>
                                                    <td width="200rem;">
                                                        <Button className="rounded-0 mr-2" outline color="primary" id="view-sdm" onClick={(e) => this.handleClickAction(e, project.project_id)}>
                                                            <i className="fa fa-eye"></i>
                                                        </Button>
                                                        <Link to={`/projectassignment/list/update/${project.project_id}`}>
                                                            <Button className="rounded-0 mr-2" outline color="primary" id="edit-sdm">
                                                                <i className="fa fa-edit"></i>
                                                            </Button>
                                                        </Link>
                                                        <Button className="rounded-0" outline color="danger" id="delete-sdm" onClick={(e) => this.handleClickAction(e, project.project_id)}>
                                                            <i className="fa fa-trash"></i>
                                                        </Button>
                                                    </td>
                                                </tr>
                                            )}
                                    </tbody>
                                </Table>
                                <div className="clearfix">
                                    {Math.ceil(this.state.totalItems / 10) > 1 ?
                                        <div className="float-right">
                                            <Pagination aria-label="Page navigation example">
                                                <PaginationItem className={parseInt(skip) === 0 ? "disabled ds-pagination" : ''} >
                                                    <PaginationLink previous onClick={this.handleClick} name="previous" />
                                                </PaginationItem>
                                                {this.createPagination()}
                                                <PaginationItem className={parseInt(skip) === (total - 1) ? "disabled ds-pagination" : ''} >
                                                    <PaginationLink next onClick={this.handleClick} name="next" />
                                                </PaginationItem>
                                            </Pagination>
                                        </div>
                                        : null}
                                </div>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className} centered size={this.state.buttonId === 'view-sdm' ? "lg" : "md"}>
                    <ModalHeader toggle={this.toggle}>
                        {this.state.buttonId === 'delete-sdm' ? <div><i className="fa fa-warning text-warning"></i> Warning</div> : 'Data SDM'}
                    </ModalHeader>
                    <ModalBody key={this.state.buttonId}>
                        <ModalBodyContent key={this.state.buttonId} id={this.state.buttonId} param={this.state.projectId} />
                    </ModalBody>
                    <ModalFooter>
                        {this.state.buttonId === 'delete-sdm' ? <Button className="rounded-0" id="yes" color="primary" onClick={this.toggle}>Yes</Button> : null}
                        <Button className="rounded-0" color="danger" onClick={this.toggle}>{this.state.buttonId === 'delete-sdm' ? 'Cancel' : 'Back'}</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default ListDetailProjectSDM;