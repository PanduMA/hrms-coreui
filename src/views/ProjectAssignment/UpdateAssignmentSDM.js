import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { putDatas, getDatasFinal } from '../../services/APIFunction';
import {
    Card, CardBody,
    Form, FormGroup, Label, Input, Button,
    Col, Row,
} from 'reactstrap';
import moment from 'moment';
let body
class UpdateAssignmentSDM extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            sdmName: '',
            contact: '',
            startDate: '',
            endDate: '',
            location: '',
            picHandler: '',
            picContact: '',
            methodId: '',
            dataLovMethods: [],
            dataAssignments: [],
            isChange: false
        })
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    componentDidMount() {
        this.getMethodLov()
        this.getDataAssignments(this.props.match.params.id)
    }
    getMethodLov() {
        getDatasFinal('http://209.97.160.40:8080/SA061118/lov/method')
            .then((result) => {
                this.setState({
                    dataLovMethods: result.data
                })
            })
    }
    getDataAssignments(id) {
        var paramFilter = 'sdmassign_id=' + id
        getDatasFinal('http://209.97.160.40:8080/SA061118/project/SdmAssignment/readAll', '', '', paramFilter)
            .then((result) => {
                this.setState({
                    dataAssignments: result.data.items
                })
                this.state.dataAssignments.map(assignment =>
                    this.setState({
                        sdmName: assignment.sdm_name,
                        contact: assignment.sdm_phone,
                        startDate: assignment.sdmassign_startdate,
                        endDate: assignment.sdmassign_enddate,
                        location: assignment.sdmassign_loc,
                        picHandler: assignment.sdmassign_picclient,
                        picContact: assignment.sdmassign_picclientphone,
                        methodId: assignment.method_id,
                    })
                )
            })
    }
    handleChange(e) {
        this.setState({ isChange: true })
        if (e.target.name === 'start') {
            this.setState({ startDate: e.target.value })
        } else if (e.target.name === 'end') {
            this.setState({ endDate: e.target.value })
        } else if (e.target.name === 'location') {
            this.setState({ location: e.target.value })
        } else if (e.target.name === 'picHandler') {
            this.setState({ picHandler: e.target.value })
        } else if (e.target.name === 'picContact') {
            this.setState({ picContact: e.target.value })
        } else this.setState({ methodId: e.target.value })
    }
    handleSubmit(event) {
        event.preventDefault();
        body = JSON.stringify({
            sdmassign_startdate: this.state.startDate,
            sdmassign_enddate: this.state.endDate,
            sdmassign_loc: this.state.location,
            sdmassign_picclient: this.state.picHandler,
            sdmassign_picclientphone: this.state.picContact,
            method_id: this.state.methodId
        })
        putDatas('http://209.97.160.40:8080/SA061118/project/SdmAssignment/update?sdmassign_id=' + parseInt(this.props.match.params.id), body)
            .then((result) => {
                window.location = "#/projectassignment/assignment"
                localStorage.setItem("messageAssign", result.message)
            })
    }
    render() {
        return (
            <div>
                <Card className="rounded-0 shadow-sm animated fadeIn">
                    <CardBody className="px-5 py-5">
                        <Form onSubmit={this.handleSubmit}>
                            <FormGroup row>
                                <Label sm={{ size: 3, offset: 1 }} className="text-right">SDM Name</Label>
                                <Col sm={{ size: 5 }}>
                                    <Input type="text" className="rounded-0" value={this.state.sdmName} readOnly />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 3, offset: 1 }} className="text-right">Contact</Label>
                                <Col sm={{ size: 5 }}>
                                    <Input type="text" className="rounded-0" value={this.state.contact} readOnly />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 3, offset: 1 }} className="text-right">Start Date</Label>
                                <Col sm={{ size: 5 }}>
                                    <Input type="date" className="rounded-0" name="start" value={this.state.startDate} onChange={this.handleChange} required />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 3, offset: 1 }} className="text-right">End Date</Label>
                                <Col sm={{ size: 5 }}>
                                    <Input type="date" className="rounded-0" name="end" min={moment(this.state.startDate).add(1, 'days').format('YYYY-MM-DD')} value={this.state.endDate} onChange={this.handleChange} required />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 3, offset: 1 }} className="text-right">Location</Label>
                                <Col sm={{ size: 5 }}>
                                    <Input type="text" className="rounded-0" name="location" value={this.state.location} onChange={this.handleChange} />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 3, offset: 1 }} className="text-right">PIC Handler</Label>
                                <Col sm={{ size: 5 }}>
                                    <Input type="text" className="rounded-0" name="picHandler" value={this.state.picHandler} onChange={this.handleChange} />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 3, offset: 1 }} className="text-right">PIC Contact</Label>
                                <Col sm={{ size: 5 }}>
                                    <Input type="text" className="rounded-0" name="picContact" value={this.state.picContact} onChange={this.handleChange} />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 3, offset: 1 }} className="text-right">Method</Label>
                                <Col sm={{ size: 5 }}>
                                    <Input type="select" className="rounded-0" onChange={this.handleChange} value={this.state.methodId}>
                                        {this.state.dataLovMethods.map(method =>
                                            <option key={method.key} value={method.key}>{method.values.projectmethod_method_name}</option>
                                        )}
                                    </Input>
                                </Col>
                            </FormGroup>
                            <Row>
                                <Col sm={{ size: 3, offset: 3 }}>
                                    {
                                        !this.state.isChange ? <Button className="rounded-0" block outline type="submit" color="primary" disabled>Update</Button> :
                                            <Button className="rounded-0" block outline type="submit" color="primary">Update</Button>
                                    }
                                </Col>
                                <Col sm={{ size: 3 }}>
                                    <Link to="/projectassignment/assignment" className="btn btn-outline-danger btn-block rounded-0" role="button">
                                        Back
                                    </Link>
                                </Col>
                            </Row>
                        </Form>
                    </CardBody>
                </Card>
            </div>
        );
    }
}

export default UpdateAssignmentSDM;