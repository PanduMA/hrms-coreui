import React, { Component } from 'react';
import moment from 'moment';
import Autosuggest from 'react-autosuggest';
import AutosuggestHighlightMatch from 'autosuggest-highlight/match';
import AutosuggestHighlightParse from 'autosuggest-highlight/parse';
import { postDatas, getDatasFinal } from '../../services/APIFunction';
import {
    Row, Col,
    Card, CardBody,
    FormGroup, Button, Input, CustomInput, Label,
    Modal, ModalHeader, ModalBody, ModalFooter,
    Pagination, PaginationItem, PaginationLink,
    Table, UncontrolledTooltip
} from 'reactstrap';
var operator, cat, ski, val
let body, skip = 0, total, startSlice = 0, endSlice = 10, start = 0, list, listsdm = [], listsdmNew = [], filter, listsdm1 = [], listsdm2 = []
class MultiFilteringSDM extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            value: '',
            sdmKey: '',
            sdmId: '',
            suggestions: [],
            dataSDMNames: [],
            dataCategorys: [],
            dataMultiFilters: [],
            dataMultiFiltersAll: [],
            dataMultiFiltersView: [],
            dataSkills: [],
            sdmName: null,
            categoryId: '',
            skillId: '',
            isChecked: false,
            valueSkill: '',
            totalItemsMulti: '',
            totalItems: '',
            operator: '1',
            modal: false,
            isShow: false,
            addOption: 0,
            maxLayout: 4,
        })
        this.handleChange = this.handleChange.bind(this)
        this.handleClick = this.handleClick.bind(this)
        this.handleKey = this.handleKey.bind(this)
        this.toggle = this.toggle.bind(this)
        this.handleClickAdd = this.handleClickAdd.bind(this)
        this.handleClickMinus = this.handleClickMinus.bind(this)
        this.handleChangeNew = this.handleChangeNew.bind(this)
    }
    toggle(e) {
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
    }
    componentDidMount() {
        this.getDataSDMName()
        this.getDataLovCategory()
    }
    getDataLovCategory() {
        getDatasFinal('http://209.97.160.40:8080/SA061118/lov/SkillType')
            .then(result => {
                this.setState({ dataCategorys: result.data })
            })
    }
    getDataLovSkill(id) {
        var paramFilter = 'skilltype_id=' + id
        getDatasFinal('http://209.97.160.40:8080/SA061118/lov/Skill', '', '', paramFilter)
            .then(result => {
                this.setState({ dataSkills: result.data })
            })
    }
    handleChange(e) {
        if (e.target.name === 'category' && e.target.value !== 'prompt') {
            this.setState({ categoryId: e.target.value, skillId: '' })
            this.getDataLovSkill(e.target.value)
        } else if (e.target.name === 'skill' && e.target.value !== 'prompt') {
            this.setState({ skillId: e.target.value })
        } else if (e.target.type === 'checkbox') {
            this.setState({ isChecked: !this.state.isChecked })
        } else if (e.target.name === 'valueSkill') {
            this.setState({ valueSkill: e.target.value })
        }
    }
    handleClick(e) {
        if (e.target.name === 'filter') {
            this.setState({ isShow: true })
            listsdm = [
                {
                    skilltype_id: parseInt(this.state.categoryId),
                    skill_id: parseInt(this.state.skillId),
                    sdmskill_value: parseInt(this.state.valueSkill),
                    operator: parseInt(operator)
                },
            ]
            filter = listsdm
            console.log(Object.keys(list))
            this.postFilter()
        }
        if (e.target.name === 'pagination') { // do when click pagination 
            skip = e.target.value
            startSlice = skip + start
            endSlice = parseInt(startSlice) + 10
            this.setState({
                dataMultiFilters: this.state.dataMultiFiltersAll.slice(startSlice, endSlice)
            })
        }
        if (e.target.name === 'previous') {
            skip = 0
            startSlice = 0
            endSlice = 10
            this.setState({
                dataMultiFilters: this.state.dataMultiFiltersAll.slice(startSlice, endSlice)
            })
        }
        if (e.target.name === 'next') {
            skip = parseInt(total) - 1
            startSlice = skip.toString() + start
            endSlice = parseInt(startSlice) + 10
            this.setState({
                dataMultiFilters: this.state.dataMultiFiltersAll.slice(startSlice, endSlice)
            })
        }
        if (e.target.name === 'clear') {
            this.setState({
                skillId: '',
                categoryId: '',
                value: '',
                sdmKey: '',
                valueSkill: '',
                dataMultiFilters: [],
                isShow: false,
                dataSkills: [],
                addOption: 0,
            })
            skip = 0
            startSlice = 0
            endSlice = 10
            list = []
        }
    }
    postFilter() {
        if (this.state.categoryId === '' && this.state.skillId === '' && this.state.valueSkill === '') {
            this.setState({
                categoryId: null,
                skillId: null,
                valueSkill: null
            })
        }
        body = JSON.stringify({
            "listsdm":
                [{
                    operator: parseInt(operator),
                    sdm_id: parseInt(this.state.sdmKey),
                    skilltype_id: parseInt(this.state.categoryId),
                    skill_id: parseInt(this.state.skillId),
                    sdmskill_value: parseInt(this.state.valueSkill)
                }]
        })
        if (this.state.sdmKey === '') {
            body = JSON.stringify({
                "listsdm": filter
            })
        }
        postDatas('http://209.97.160.40:8080/SA061118/api/masterdata/MultiFiltering/multiFilter', body)
            .then((result) => {
                if (result.data != null) {
                    this.setState({
                        dataMultiFiltersAll: result.data,
                        dataMultiFilters: result.data.slice(startSlice, endSlice),
                        totalItemsMulti: result.data.length
                    })
                }
            })
    }
    handleKey(e) {
        if (e.target.value.length > 1) { //if inputed more than 2 digit (start from 0)
            e.preventDefault();
        }
    }
    getDataSDMName() {
        var paramFilter = 'sdm_status=1'
        var paramOrderBy = 'sdm_name ASC'
        getDatasFinal('http://209.97.160.40:8080/SA061118/lov/Sdm', '', '', paramFilter, paramOrderBy)
            .then((result) => {
                this.setState({
                    dataSDMNames: result.data
                })
            });
    }
    getSuggestions = value => {
        const inputValue = value.trim().toLowerCase();
        const inputLength = inputValue.length;

        return inputLength === 0 ? [] : this.state.dataSDMNames.filter(sdm =>
            sdm.values.sdm_sdm_name.toLowerCase().slice(0, inputLength) === inputValue
        );
    };
    getSuggestionValue = suggestion => suggestion.values.sdm_sdm_name;
    onChange = (event, { newValue }) => {
        this.setState({
            value: newValue,
        });
        var result = this.state.dataSDMNames.find(sdm => sdm.values.sdm_sdm_name === newValue)
        if (result) {
            this.setState({ sdmKey: Object.values(result)[1] })
        }
    };
    onSuggestionsFetchRequested = ({ value }) => {
        this.setState({
            suggestions: this.getSuggestions(value)
        });
    };
    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };
    renderSuggestion = (suggestion, { query }) => {
        const matches = AutosuggestHighlightMatch(suggestion.values.sdm_sdm_name, query)
        const parts = AutosuggestHighlightParse(suggestion.values.sdm_sdm_name, matches)
        return (
            <span>
                {parts.map((part, index) => {
                    const className = part.highlight ? 'react-autosuggest__suggestion-match' : null;
                    return (
                        <span className={className} key={index}>
                            {part.text}
                        </span>
                    );
                })}
            </span>
        );
    }
    handleClickAction = (e, param) => {
        e.preventDefault();
        this.setState({ sdmId: param })
        this.getDataBaseFilter(param)
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
    }
    getDataBaseFilter(id) {
        body = JSON.stringify({ "listsdmskill": [{ sdm_id: parseInt(id) }] })
        postDatas('http://209.97.160.40:8080/SA061118/allocation/RicianSdmSkill/filter', body)
            .then(result => {
                this.setState({
                    dataMultiFiltersView: result.data,
                    sdmName: result.data[0].sdm_name,
                    totalItems: result.data.length
                })
            })
    }
    handleClickAdd() {
        if (this.state.addOption < 4) {
            this.setState((prevState) => ({
                addOption: ++prevState.addOption,
            }))
        }
    }
    handleClickMinus() {
        this.setState((prevState) => ({
            addOption: --prevState.addOption,
        }))
    }
    handleChangeNew(e) {
        for (let index = 0; index < this.state.maxLayout; index++) {
            if (index === 1) {
                if (e.target.name === 'categoryId-1') { cat = e.target.value }
                if (e.target.name === 'skillId-1') { ski = e.target.value }
                if (e.target.name === 'valueSkill-1') { val = e.target.value }
                listsdmNew = [{ skilltype_id: parseInt(cat), skill_id: parseInt(ski), sdmskill_value: parseInt(val), operator: parseInt(operator) }]
            }
        }
    }
    createPagination = () => {
        let element = []
        total = Math.ceil(this.state.totalItemsMulti / 10)
        for (let index = 0; index < total; index++) {
            element.push(
                <PaginationItem key={index} className={parseInt(skip) === index ? 'active' : ''}>
                    <PaginationLink value={index} onClick={this.handleClick} name="pagination">
                        {index + 1}
                    </PaginationLink>
                </PaginationItem>
            )
        }
        if (total > 5) {
            if (skip < 3) {
                return element.slice(0, 5)
            } else {
                var startMove = parseInt(skip) - 2
                var endMove = parseInt(skip) + 3
                if (startMove > (total - 5) && endMove > total) {
                    startMove = total - 5
                    endMove = total
                }
                return element.slice(startMove, endMove)
            }
        } else {
            return element.slice(0, 5)
        }
    }
    render() {
        this.state.isChecked ? operator = '2' : operator = '1'
        list = [
            <FormGroup row key="0">
                <Col sm={{ size: 3, offset: 1 }}>
                    <Input type="select" className="rounded-0" value={this.state.categoryId} onChange={this.handleChange} name="category">
                        <option value="prompt">Choose Category Skill</option>
                        {this.state.dataCategorys.map(category =>
                            <option key={category.key} value={category.key}>{category.values.skilltype_skilltype_name}</option>
                        )}
                    </Input>
                </Col>
                <Col sm={{ size: 3 }}>
                    <Input type="select" className="rounded-0" value={this.state.skillId} onChange={this.handleChange} name="skill">
                        <option value="prompt">Choose List Skill</option>
                        {this.state.dataSkills != null ? this.state.dataSkills.map(skill =>
                            <option key={skill.key} value={skill.key}>{skill.values.skills_skill_name}</option>
                        ) : ''}
                    </Input>
                </Col>
                <Col sm={{ size: 2 }}>
                    <Input
                        type="number"
                        className="rounded-0"
                        name="valueSkill"
                        placeholder="Value"
                        min="1"
                        max="10"
                        value={this.state.valueSkill}
                        onKeyPress={this.handleKey}
                        onChange={this.handleChange}
                    />
                </Col>
                <Col sm={{ size: 1 }}>
                    <Button className="rounded-0" outline color="primary" onClick={this.handleClickAdd}>
                        <i className="fa fa-plus"></i>
                    </Button>
                </Col>
            </FormGroup>
        ]
        for (var i = 0; i < this.state.maxLayout; i++) {
            list.push(
                this.state.addOption > i &&
                <FormGroup row key={i + 1}>
                    <Col sm={{ size: 3, offset: 1 }}>
                        <Input type="select" className="rounded-0" onChange={this.handleChangeNew} name={"categoryId-" + (i + 1)}>
                            <option value="prompt">Choose Category Skill</option>
                            {this.state.dataCategorys.map(category =>
                                <option key={category.key} value={category.key}>{category.values.skilltype_skilltype_name}</option>
                            )}
                        </Input>
                    </Col>
                    <Col sm={{ size: 3 }}>
                        <Input type="select" className="rounded-0" onChange={this.handleChangeNew} name={"skillId-" + (i + 1)}>
                            <option value="prompt">Choose List Skill</option>
                            {this.state.dataSkills != null ? this.state.dataSkills.map(skill =>
                                <option key={skill.key} value={skill.key}>{skill.values.skills_skill_name}</option>
                            ) : ''}
                        </Input>
                    </Col>
                    <Col sm={{ size: 2 }}>
                        <Input
                            type="number"
                            className="rounded-0"
                            name={"valueSkill-" + (i + 1)}
                            placeholder="Value"
                            min="1"
                            max="10"
                            onKeyPress={this.handleKey}
                            onChange={this.handleChangeNew}
                        />
                    </Col>
                    <Col sm={{ size: 1 }}>
                        <Button className="rounded-0" outline color="primary" onClick={this.handleClickMinus}>
                            <i className="fa fa-minus"></i>
                        </Button>
                    </Col>
                </FormGroup>
            )
        }
        const { value, suggestions } = this.state;
        const inputProps = {
            placeholder: 'Enter SDM Name',
            className: 'form-control rounded-0',
            value,
            onChange: this.onChange,
        };
        return (
            <div>
                <Card className="rounded-0 shadow-sm animated fadeIn">
                    <CardBody className="px-5 py-5">
                        <FormGroup row>
                            <Col sm={{ size: 6, offset: 2 }}>
                                <Autosuggest
                                    suggestions={suggestions}
                                    onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                                    onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                                    getSuggestionValue={this.getSuggestionValue}
                                    renderSuggestion={this.renderSuggestion}
                                    inputProps={inputProps}
                                />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Col sm={{ size: 3, offset: 2 }}>
                                <CustomInput
                                    type="checkbox"
                                    id="CombineFiltering"
                                    label="Combine Filtering Mode"
                                    onChange={this.handleChange}
                                    defaultChecked={this.state.isChecked}
                                    inline />
                            </Col>
                        </FormGroup>
                        {list}
                        <Row>
                            <Col sm={{ size: 3, offset: 2 }}>
                                <Button className="rounded-0" outline color="primary" block onClick={this.handleClick} name="filter">
                                    <i className="fa fa-search"></i> Filter
                                </Button>
                            </Col>
                            <Col sm={{ size: 3 }}>
                                <Button className="rounded-0" outline color="danger" block onClick={this.handleClick} name="clear">
                                    <i className="fa fa-close"></i> Clear
                                </Button>
                            </Col>
                        </Row>
                    </CardBody>
                </Card>
                {!this.state.isShow ? null :
                    <Card>
                        <CardBody>
                            <div className="clearfix px-3">
                                <div className="float-right caption-color">
                                    <p>Total : {this.state.totalItemsMulti}</p>
                                </div>
                            </div>
                            <Table responsive hover bordered className="text-center">
                                <thead>
                                    <tr>
                                        <th>NIK</th>
                                        <th>SDM Name</th>
                                        <th>End Date Project</th>
                                        <th>Notification Contract</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.dataMultiFilters.length < 1 ? <tr><td colSpan="5">0 data display</td></tr> :
                                        this.state.dataMultiFilters.map((datas, index) =>
                                            <tr key={datas.sdm_id}>
                                                <th>{datas.sdm_nik}</th>
                                                <td>{datas.sdm_name}</td>
                                                <td>{datas.end_contractproject}</td>
                                                <td>
                                                    {(datas.sdm_notification === 'green') ?
                                                        <div>
                                                            <i className="fa fa-warning text-success fa-2x" id={"green-" + index}></i>
                                                            <UncontrolledTooltip placement="bottom" target={"green-" + index}>
                                                                Sisa 4 bulan
                                                                </UncontrolledTooltip>
                                                        </div>
                                                        : (datas.sdm_notification === 'grey') ?
                                                            <div>
                                                                <i className="fa fa-warning text-secondary fa-2x" id={"grey-" + index}></i>
                                                                <UncontrolledTooltip placement="bottom" target={"grey-" + index}>
                                                                    Sisa lebih dari 4 bulan
                                                                </UncontrolledTooltip>
                                                            </div>
                                                            : (datas.sdm_notification === 'red') ?
                                                                <div>
                                                                    <i className="fa fa-warning text-danger fa-2x" id={"red-" + index}></i>
                                                                    <UncontrolledTooltip placement="bottom" target={"red-" + index}>
                                                                        Sisa 1 bulan
                                                                        </UncontrolledTooltip>
                                                                </div>
                                                                : (datas.sdm_notification === 'yellow') ?
                                                                    <div>
                                                                        <i className="fa fa-warning text-warning fa-2x" id={"yellow-" + index}></i>
                                                                        <UncontrolledTooltip placement="bottom" target={"yellow-" + index}>
                                                                            Sisa 2 bulan
                                                                            </UncontrolledTooltip>
                                                                    </div>
                                                                    : 'Belum Ada'
                                                    }
                                                </td>
                                                <td>
                                                    <Button className="rounded-0" outline color="primary" id="view-sdm" onClick={(e) => this.handleClickAction(e, datas.sdm_id)}>
                                                        <i className="fa fa-eye"></i>
                                                    </Button>
                                                </td>
                                            </tr>
                                        )}
                                </tbody>
                            </Table>
                            <div className="clearfix">
                                {Math.ceil(this.state.totalItemsMulti / 10) > 1 ?
                                    <div className="float-right">
                                        <Pagination aria-label="Page navigation example">
                                            <PaginationItem className={parseInt(skip) === 0 ? "disabled ds-pagination" : ''} >
                                                <PaginationLink previous onClick={this.handleClick} name="previous" />
                                            </PaginationItem>
                                            {this.createPagination()}
                                            <PaginationItem className={parseInt(skip) === (total - 1) ? "disabled ds-pagination" : ''} >
                                                <PaginationLink next onClick={this.handleClick} name="next" />
                                            </PaginationItem>
                                        </Pagination>
                                    </div>
                                    : null}
                            </div>
                        </CardBody>
                    </Card>
                }
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className} size={"lg"}>
                    <ModalHeader toggle={this.toggle}>
                        Data SDM
                    </ModalHeader>
                    <ModalBody>
                        <div className="clearfix px-3">
                            <div className="float-left">
                                <Label className="font-weight-bold">SDM Name : {this.state.sdmName}</Label>
                            </div>
                            <div className="float-right">
                                <p>Total : {this.state.totalItems}</p>
                            </div>
                        </div>
                        <Table responsive hover bordered className="text-center">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Skill Type</th>
                                    <th>Skill Name</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.dataMultiFiltersView.length < 1 ? <tr><td colSpan="2">0 data display</td></tr> :
                                    this.state.dataMultiFiltersView.map((datas, index) =>
                                        <tr key={index}>
                                            <th>{index + 1}</th>
                                            <td>{datas.skilltype_name}</td>
                                            <td>{datas.sdm_skill_value}</td>
                                        </tr>
                                    )}
                            </tbody>
                        </Table>
                    </ModalBody>
                    <ModalFooter>
                        <Button className="rounded-0" color="danger" onClick={this.toggle}>Back</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default MultiFilteringSDM;