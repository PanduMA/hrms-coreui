import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { postDatas, getDatasFinal } from '../../services/APIFunction';
import {
    Row, Col,
    Card, CardBody,
    Table,
    Input, InputGroup, InputGroupAddon, InputGroupText, Label, Button,
    Modal, ModalHeader, ModalBody, ModalFooter,
    Pagination, PaginationItem, PaginationLink,
    Alert, UncontrolledTooltip
} from 'reactstrap';
let body, skip = 0, start = 0, total, startSlice = 0, endSlice = 10, messageAlloc
class ListSDMSkillAllocation extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            search: '',
            searchBy: '1',
            dataSDMAllocationsAll: [],
            dataSDMAllocations: [],
            totalItems: '',
            sdmId: '',
            message: '',
            modal: false,
            visible: false
        })
        this.handleKeyDown = this.handleKeyDown.bind(this)
        this.handleClick = this.handleClick.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.handleKeyPress = this.handleKeyPress.bind(this)
        this.toggle = this.toggle.bind(this)
        this.onDismiss = this.onDismiss.bind(this)
    }
    componentDidMount() {
        skip = 0
        startSlice = 0
        endSlice = 10
        this.getDataSDMAllocations()
        messageAlloc = localStorage.getItem("messageAlloc")
        if (messageAlloc !== null) {
            this.setState({ visible: true, message: messageAlloc })
            this.setTimeoutAlert()
            localStorage.removeItem("messageAlloc")
        }
    }
    onDismiss() {
        localStorage.removeItem("messageAlloc")
        this.setState({ visible: false });
    }
    toggle(e) {
        body = JSON.stringify({ listsdm: [{ sdm_id: this.state.sdmId }] })
        if (e.target.id === 'yes') {
            postDatas('http://209.97.160.40:8080/SA061118/allocation/MultiDeleteSdmSkill/multiDelete', body)
                .then((result) => {
                    this.setState({
                        message: result.message,
                        sdmId: '',
                        visible: true
                    })
                    skip = 0
                    startSlice = 0
                    endSlice = 10
                    this.setState({ search: '', searchBy:'1' })
                    this.getDataSDMAllocations()
                    this.setTimeoutAlert()
                })
        }
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
    }
    getDataSDMAllocations() {
        getDatasFinal('http://209.97.160.40:8080/SA061118/allocation/MengelolaSdmSkill/readAll')
            .then((result) => {
                this.setState({
                    dataSDMAllocationsAll: result.data.items,
                    dataSDMAllocations: result.data.items.slice(startSlice, endSlice),
                    totalItems: result.data.items.length
                })
            });
    }
    handleChange(e) {
        if (e.target.name === 'searchby') {
            this.setState({
                searchBy: e.target.value,
            })
        } else {
            this.setState({
                search: e.target.value
            })
        }
    }
    handleClick(e) {
        if (e.target.name === 'pagination') { // do when click pagination 
            skip = e.target.value
            startSlice = skip + start
            endSlice = parseInt(startSlice) + 10
            if (this.state.search !== '') {
                if (this.state.searchBy === '1') {
                    this.searchName(this.state.search)
                } else {
                    this.searchNIK(this.state.search)
                }
            } else {
                this.setState({
                    dataSDMAllocations: this.state.dataSDMAllocationsAll.slice(startSlice, endSlice),
                })
            }
        }
        if (e.target.name === 'reset') {
            this.getDataSDMAllocations()
            this.setState({
                search: '',
                searchBy: '1'
            })
            skip = 0
            startSlice = 0
            endSlice = 10
        }
        if (e.target.name === 'previous') {
            skip = 0
            startSlice = 0
            endSlice = 10
            if (this.state.search !== '') {
                if (this.state.searchBy === '1') {
                    this.searchName(this.state.search)
                } else {
                    this.searchNIK(this.state.search)
                }
            } else {
                this.setState({
                    dataSDMAllocations: this.state.dataSDMAllocationsAll.slice(startSlice, endSlice),
                })
            }
        }
        if (e.target.name === 'next') {
            skip = parseInt(total) - 1
            startSlice = skip.toString() + start
            endSlice = parseInt(startSlice) + 10
            if (this.state.search !== '') {
                if (this.state.searchBy === '1') {
                    this.searchName(this.state.search)
                } else {
                    this.searchNIK(this.state.search)
                }
            } else {
                this.setState({
                    dataSDMAllocations: this.state.dataSDMAllocationsAll.slice(startSlice, endSlice),
                })
            }
        }
    }
    handleKeyDown(e) {
        if (e.keyCode === 13) {
            this.setState({
                search: e.target.value
            })
            skip = 0
            startSlice = 0
            endSlice = 10
            if (this.state.searchBy === '1') {
                this.searchName(e.target.value)
            } else {
                this.searchNIK(e.target.value)
            }
        }
    }
    handleKeyPress(e) {
        if (this.state.searchBy === '2') {
            if (e.charCode < 48 || e.charCode > 57) {
                e.preventDefault();
            }
        }
    }
    searchName(value) {
        let word = new RegExp(value, 'gi')
        let found = this.state.dataSDMAllocationsAll.filter(sdm => sdm.sdm_name.match(word))
        this.setState({
            dataSDMAllocations: found.slice(startSlice, endSlice),
            totalItems: found.length
        })
    }
    searchNIK(value) {
        let word = new RegExp(value, 'gi')
        let found = this.state.dataSDMAllocationsAll.filter(sdm => sdm.sdm_nik.match(word))
        this.setState({
            dataSDMAllocations: found.slice(startSlice, endSlice),
            totalItems: found.length
        })
    }
    handleClickAction = (e, param) => {
        e.preventDefault();
        this.setState({ sdmId: param })
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
    }
    createPagination = () => {
        let element = []
        total = Math.ceil(this.state.totalItems / 10)
        for (let index = 0; index < total; index++) {
            element.push(
                <PaginationItem key={index} className={parseInt(skip) === index ? 'active' : ''}>
                    <PaginationLink value={index} onClick={this.handleClick} name="pagination">
                        {index + 1}
                    </PaginationLink>
                </PaginationItem>
            )
        }
        if (total > 5) {
            if (skip < 3) {
                return element.slice(0, 5)
            } else {
                var startMove = parseInt(skip) - 2
                var endMove = parseInt(skip) + 3
                if (startMove > (total - 5) && endMove > total) {
                    startMove = total - 5
                    endMove = total
                }
                return element.slice(startMove, endMove)
            }
        } else {
            return element.slice(0, 5)
        }
    }
    setTimeoutAlert() {
        setTimeout(() => {
            this.setState({ visible: false })
        }, 3000);
    }
    render() {
        return (
            <div>
                <Alert color="success" isOpen={this.state.visible} toggle={this.onDismiss} className="shadow-sm">
                    {this.state.message}
                </Alert>
                <Row>
                    <Col sm={{ size: 12 }}>
                        <Card className="rounded-0 shadow-sm animated fadeIn">
                            <CardBody>
                                <Row>
                                    <Label md={{ size: 1 }} sm={{ size: 2 }}>Search by</Label>
                                    <Col sm={{ size: 3 }}>
                                        <Input type="select" className="rounded-0" name="searchby" value={this.state.searchBy} onChange={this.handleChange}>
                                            <option value="1">SDM Name</option>
                                            <option value="2">NIK</option>
                                        </Input>
                                    </Col>
                                    <Col sm={{ size: 4 }}>
                                        <InputGroup>
                                            <Input
                                                type="text"
                                                className="rounded-0"
                                                placeholder="Enter keyword"
                                                onKeyPress={this.handleKeyPress}
                                                onKeyDown={this.handleKeyDown}
                                                value={this.state.search}
                                                onChange={this.handleChange} />
                                            <InputGroupAddon addonType="append">
                                                <InputGroupText style={{ backgroundColor: '#FFFFFF' }}>
                                                    <i className="fa fa-search"></i>
                                                </InputGroupText>
                                            </InputGroupAddon>
                                        </InputGroup>
                                    </Col>
                                    <Col sm={{ size: 2 }}>
                                        <Button className="rounded-0" type="reset" outline color="danger" name="reset" block onClick={this.handleClick}>
                                            <i className="fa fa-close"></i> Reset
                                        </Button>
                                    </Col>
                                    <Col sm={{ size: 2 }}>
                                        <Link to={`/sdmallocation/list/add`} style={{ textDecoration: 'none' }}>
                                            <Button className="rounded-0" outline color="primary" block>
                                                <i className="fa fa-fw fa-plus" />
                                                Add SDM Skill
                                            </Button>
                                        </Link>
                                    </Col>
                                </Row>
                                <div className="clearfix px-3">
                                    <div className="float-right caption-color">
                                        <p>Total : {this.state.totalItems}</p>
                                    </div>
                                </div>
                                <Table responsive hover bordered className="text-center">
                                    <thead>
                                        <tr>
                                            <th>NIK</th>
                                            <th>SDM Name</th>
                                            <th>End Date Project</th>
                                            <th>Notification Contract</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.dataSDMAllocations.length < 1 ? <tr><td colSpan="5">0 data display</td></tr> :
                                            this.state.dataSDMAllocations.map((datas, index) =>
                                                <tr key={datas.sdm_id}>
                                                    <th>{datas.sdm_nik}</th>
                                                    <td>{datas.sdm_name}</td>
                                                    <td>{datas.end_contractproject}</td>
                                                    <td>
                                                        {(datas.sdm_notification === 'red') ?
                                                            <div>
                                                                <i className="fa fa-warning text-danger fa-2x" id={"red-" + index}></i>
                                                                <UncontrolledTooltip placement="bottom" target={"red-" + index}>
                                                                    Sisa 1 bulan
                                                                </UncontrolledTooltip>
                                                            </div>
                                                            : (datas.sdm_notification === 'yellow') ?
                                                                <div>
                                                                    <i className="fa fa-warning text-warning fa-2x" id={"yellow-" + index}></i>
                                                                    <UncontrolledTooltip placement="bottom" target={"yellow-" + index}>
                                                                        Sisa 2 bulan
                                                                    </UncontrolledTooltip>
                                                                </div>
                                                                : (datas.sdm_notification === 'green') ?
                                                                    <div>
                                                                        <i className="fa fa-warning text-success fa-2x" id={"green-" + index}></i>
                                                                        <UncontrolledTooltip placement="bottom" target={"green-" + index}>
                                                                            Sisa 4 bulan
                                                                        </UncontrolledTooltip>
                                                                    </div>
                                                                    : <div>
                                                                        <i className="fa fa-warning text-secondary fa-2x" id={"grey-" + index}></i>
                                                                        <UncontrolledTooltip placement="bottom" target={"grey-" + index}>
                                                                            Sisa lebih dari 4 bulan
                                                                        </UncontrolledTooltip>
                                                                    </div>
                                                        }
                                                    </td>
                                                    <td>
                                                        <Link to={`/sdmallocation/list/view/${datas.sdm_id}`}>
                                                            <Button className="rounded-0 mr-2" outline color="primary" id="view-sdm">
                                                                <i className="fa fa-eye"></i>
                                                            </Button>
                                                        </Link>
                                                        <Link to={`/sdmallocation/list/update/${datas.sdm_id}`}>
                                                            <Button className="rounded-0 mr-2" outline color="primary" id="edit-skill">
                                                                <i className="fa fa-edit"></i>
                                                            </Button>
                                                        </Link>
                                                        <Button className="rounded-0" outline color="danger" id="delete-skill" onClick={(e) => this.handleClickAction(e, datas.sdm_id)}>
                                                            <i className="fa fa-trash"></i>
                                                        </Button>
                                                    </td>
                                                </tr>
                                            )}
                                    </tbody>
                                </Table>
                                <div className="clearfix">
                                    {Math.ceil(this.state.totalItems / 10) > 1 ?
                                        <div className="float-right">
                                            <Pagination aria-label="Page navigation example">
                                                <PaginationItem className={parseInt(skip) === 0 ? "disabled ds-pagination" : ''} >
                                                    <PaginationLink previous onClick={this.handleClick} name="previous" />
                                                </PaginationItem>
                                                {this.createPagination()}
                                                <PaginationItem className={parseInt(skip) === (total - 1) ? "disabled ds-pagination" : ''} >
                                                    <PaginationLink next onClick={this.handleClick} name="next" />
                                                </PaginationItem>
                                            </Pagination>
                                        </div>
                                        : null}
                                </div>
                            </CardBody>
                        </Card>
                        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className} centered>
                            <ModalHeader toggle={this.toggle}>
                                <i className="fa fa-warning text-warning"></i> Warning
                            </ModalHeader>
                            <ModalBody>
                                Are you sure want to delete?
                            </ModalBody>
                            <ModalFooter>
                                <Button className="rounded-0" id="yes" color="primary" onClick={this.toggle}>Yes</Button>
                                <Button className="rounded-0" color="danger" onClick={this.toggle}>Cancel</Button>
                            </ModalFooter>
                        </Modal>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default ListSDMSkillAllocation;