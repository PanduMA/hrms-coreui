import React, { Component } from 'react';
import { deleteDatas, putDatas, getDatasFinal } from '../../services/APIFunction';
import { Link } from 'react-router-dom';
import {
    Card, CardBody, CardFooter,
    Label, Table, Button, Input,
    Pagination, PaginationItem, PaginationLink,
    Modal, ModalHeader, ModalBody, ModalFooter,
    FormGroup, Col,
    Alert
} from 'reactstrap';
let skip = 0, body, total
class UpdateSDMSkillAllocation extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            dataSDMSkills: [],
            totalItems: '',
            sdmName: '',
            sdmSkillId: '',
            skillTypeName: '',
            skillName: '',
            skillValue: '',
            isVisible: false,
            modal: false,
            message: '',
            isDisplay: false,
            isChange: false
        })
        this.handleClick = this.handleClick.bind(this)
        this.toggle = this.toggle.bind(this)
        this.onDismiss = this.onDismiss.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }
    componentDidMount() {
        this.getDataSDMSkill(this.props.match.params.id, skip)
    }
    onDismiss() {
        this.setState({ isVisible: false });
    }
    toggle(e) {
        if (e.target.id === 'yes') {
            deleteDatas('http://209.97.160.40:8080/SA061118/allocation/MengelolaSkillSdm/delete?sdmskill_id=' + this.state.sdmSkillId)
                .then((result) => {
                    this.setState({
                        message: result.message,
                        sdmSkillId: '',
                        isVisible: true
                    })
                    this.getDataSDMSkill(this.props.match.params.id, skip = 0)
                    this.setTimeoutAlert()
                })
        }
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
    }
    getDataSDMSkill(id, skip) {
        var paramFilter = 'sdm_id=' + id
        getDatasFinal('http://209.97.160.40:8080/SA061118/allocation/MengelolaSkillSdm/readAll', skip, 10, paramFilter)
            .then(result => {
                this.setState({
                    dataSDMSkills: result.data.items,
                    totalItems: result.data.totalItems,
                    sdmName: result.data.items[0].sdm_name
                })
            })
    }
    handleClick(e) {
        e.preventDefault();
        if (e.target.name === 'pagination') {
            skip = e.target.value
            this.getDataSDMSkill(this.props.match.params.id, skip)
        }
        if (e.target.name === 'previous') {
            this.getDataSDMSkill(this.props.match.params.id, skip = 0)
        }
        if (e.target.name === 'next') {
            skip = parseInt(total) - 1
            this.getDataSDMSkill(this.props.match.params.id, skip)
        }
        if (e.target.name === 'update') {
            body = JSON.stringify({ sdmskill_value: this.state.skillValue })
            putDatas('http://209.97.160.40:8080/SA061118/allocation/MengelolaSkillSdm/update?sdmskill_id=' + this.state.sdmSkillId, body)
                .then((result) => {
                    this.setState({
                        message: result.message,
                        skillValue: '',
                        isDisplay: false,
                        isChange: false,
                        isVisible: true
                    })
                    this.getDataSDMSkill(this.props.match.params.id, skip = 0)
                    this.setTimeoutAlert()
                })
        }
    }
    handleChange(e) {
        this.setState({
            skillValue: e.target.value,
            isChange: true
        })
    }
    handleClickAction = (e, param) => {
        e.preventDefault();
        if (e.target.id === 'edit-skill') {
            this.state.dataSDMSkills.map(skill =>
                skill.sdmskill_id === param ? this.setState({ sdmSkillId: param, isDisplay: true, skillTypeName: skill.skilltype_name, skillName: skill.skill_name, skillValue: skill.sdmskill_value }) : null
            )
        } else {
            this.setState({ sdmSkillId: param })
            this.setState(prevState => ({
                modal: !prevState.modal
            }));
        }
    }
    createPagination = () => {
        let element = []
        total = Math.ceil(this.state.totalItems / 10)
        for (let index = 0; index < total; index++) {
            element.push(
                <PaginationItem key={index} className={parseInt(skip) === index ? 'active' : ''}>
                    <PaginationLink value={index} onClick={this.handleClick} name="pagination">
                        {index + 1}
                    </PaginationLink>
                </PaginationItem>
            )
        }
        return element
    }
    setTimeoutAlert() {
        setTimeout(() => {
            this.setState({ isVisible: false })
        }, 3000);
    }
    render() {
        return (
            <div>
                <Alert color="success" isOpen={this.state.isVisible} toggle={this.onDismiss} className="shadow-sm">
                    {this.state.message}
                </Alert>
                <Card className={!this.state.isDisplay ? 'd-none' : 'rounded-0 shadow-sm animated fadeIn'}>
                    <CardBody>
                        <FormGroup row>
                            <Label sm={{ size: 2 }} className="text-right font-weight-bold">
                                {this.state.skillTypeName + ' ' + this.state.skillName}
                            </Label>
                            <Col sm={{ size: 2 }}>
                                <Input type="number" className="rounded-0" min="1" max="10" value={this.state.skillValue} onChange={this.handleChange} />
                            </Col>
                            <Col sm={{ size: 2 }}>
                                {!this.state.isChange ?
                                    <Button className="rounded-0" block outline color="primary" disabled>Update</Button> :
                                    <Button className="rounded-0" name="update" block outline color="primary" onClick={this.handleClick}>Update</Button>
                                }
                            </Col>
                        </FormGroup>
                    </CardBody>
                </Card>
                <Card className="rounded-0 shadow-sm animated fadeIn">
                    <CardBody>
                        <div className="clearfix px-3">
                            <div className="float-left font-weight-bold">
                                <Label>SDM Name : {this.state.sdmName}</Label>
                            </div>
                            <div className="float-right caption-color">
                                <p>Total : {this.state.totalItems}</p>
                            </div>
                        </div>
                        <Table responsive hover bordered className="text-center">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Skill Type Name</th>
                                    <th>Skill Name</th>
                                    <th>Skill Value</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.dataSDMSkills.map(skill =>
                                    <tr key={skill.sdmskill_id}>
                                        <th>{skill.norut}</th>
                                        <td>{skill.skilltype_name}</td>
                                        <td>{skill.skill_name}</td>
                                        <td>{skill.sdmskill_value}</td>
                                        <td>
                                            <Button className="rounded-0 mr-2" outline color="primary" id="edit-skill" onClick={(e) => this.handleClickAction(e, skill.sdmskill_id)}>
                                                <i className="fa fa-edit"></i>
                                            </Button>
                                            <Button className="rounded-0" outline color="danger" id="delete-skill" onClick={(e) => this.handleClickAction(e, skill.sdmskill_id)}>
                                                <i className="fa fa-trash"></i>
                                            </Button>
                                        </td>
                                    </tr>
                                )}
                            </tbody>
                        </Table>
                        <div className="clearfix">
                            {Math.ceil(this.state.totalItems / 10) > 1 ?
                                <div className="float-right">
                                    <Pagination aria-label="Page navigation example">
                                        <PaginationItem className={parseInt(skip) === 0 ? "disabled ds-pagination" : ''} >
                                            <PaginationLink previous onClick={this.handleClick} name="previous" />
                                        </PaginationItem>
                                        {this.createPagination()}
                                        <PaginationItem className={parseInt(skip) === (total - 1) ? "disabled ds-pagination" : ''} >
                                            <PaginationLink next onClick={this.handleClick} name="next" />
                                        </PaginationItem>
                                    </Pagination>
                                </div>
                                : null}
                        </div>
                    </CardBody>
                    <CardFooter>
                        <Link to={`/sdmallocation/list`}>
                            <Button className="rounded-0" outline color="primary">Back</Button>
                        </Link>
                    </CardFooter>
                </Card>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className} centered>
                    <ModalHeader toggle={this.toggle}>
                        <i className="fa fa-warning text-warning"></i> Warning
                    </ModalHeader>
                    <ModalBody>
                        Are you sure want to delete?
                    </ModalBody>
                    <ModalFooter>
                        <Button className="rounded-0" id="yes" color="primary" onClick={this.toggle}>Yes</Button>
                        <Button className="rounded-0" color="danger" onClick={this.toggle}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default UpdateSDMSkillAllocation;