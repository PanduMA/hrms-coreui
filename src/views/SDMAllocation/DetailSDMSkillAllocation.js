import React, { Component } from 'react';
import { postDatas } from '../../services/APIFunction';
import { Link } from 'react-router-dom';
import {
    Card, CardBody, CardFooter,
    Label,
    Table,
    Button
} from 'reactstrap';
let body
class DetailSDMSkillAllocation extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            dataSDMAllocations: [],
            sdmName: '',
            totalItems: ''
        })
    }
    componentDidMount() {
        this.getDataSDMAllocations(this.props.match.params.id)
    }
    getDataSDMAllocations(id) {
        body = JSON.stringify({ listsdmskill: [{ sdm_id: id }] })
        postDatas('http://209.97.160.40:8080/SA061118/allocation/RicianSdmSkill/filter', body)
            .then((result) => {
                this.setState({
                    dataSDMAllocations: result.data,
                    sdmName: result.data[0].sdm_name,
                    totalItems: result.data.length
                })
            });
    }
    render() {
        return (
            <div>
                <Card className="rounded-0 shadow-sm animated fadeIn">
                    <CardBody>
                        <div className="clearfix px-3">
                            <div className="float-left">
                                <Label className="font-weight-bold">SDM Name : {this.state.sdmName}</Label>
                            </div>
                            <div className="float-right caption-color">
                                <p>Total : {this.state.totalItems}</p>
                            </div>
                        </div>
                        <Table responsive hover bordered className="table-outline">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Skill Type Name</th>
                                    <th>Skill Name</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.dataSDMAllocations.map((sdm, index) =>
                                    <tr key={index}>
                                        <th>{index+1}</th>
                                        <td>{sdm.skilltype_name}</td>
                                        <td>{sdm.sdm_skill_value}</td>
                                    </tr>
                                )}
                            </tbody>
                        </Table>
                    </CardBody>
                    <CardFooter>
                        <Link to={`/sdmallocation/list`}>
                            <Button className="rounded-0" outline color="primary">Back</Button>
                        </Link>
                    </CardFooter>
                </Card>
            </div>
        );
    }
}

export default DetailSDMSkillAllocation;