import React, { Component } from 'react';
import { postDatas, putDatas, deleteDatas, getDatasFinal } from '../../services/APIFunction';
import {
    Card, CardBody,
    FormGroup, Input, Label, Button, Form,
    Row, Col,
    Table,
    Pagination, PaginationItem, PaginationLink,
    Modal, ModalHeader, ModalBody, ModalFooter,
    Alert
} from 'reactstrap';
let skip = 0, body, total
class ListSkill extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            categorySkill: '',
            skillName: '',
            skillId: '',
            skillTypeId: '',
            dataSkills: [],
            totalItems: '',
            dataCategoryNames: [],
            buttonName: 'Submit',
            visible: false,
            modal: false,
            message: '',
        })
        this.handleClick = this.handleClick.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.onDismiss = this.onDismiss.bind(this)
        this.toggle = this.toggle.bind(this)
        this.handleClickAction = this.handleClickAction.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    onDismiss() {
        this.setState({ visible: false });
    }
    toggle(e) {
        if (e.target.id === 'yes') {
            deleteDatas('http://209.97.160.40:8080/SA061118/allocation/MengelolaSkill/delete?skill_id=' + this.state.skillId)
                .then((result) => {
                    this.setState({
                        message: result.message,
                        skillId: '',
                        visible: true
                    })
                    this.getDataSkills(skip = 0)
                    this.setTimeoutAlert()
                })
        }
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
    }
    componentDidMount() {
        skip = 0
        this.getDataSkills(skip)
        this.getDataLovCategorys()
    }
    getDataSkills(skip) {
        getDatasFinal('http://209.97.160.40:8080/SA061118/allocation/MengelolaSkill/readAll', skip, 10)
            .then((result) => {
                if (result.data != null) {
                    this.setState({
                        dataSkills: result.data.items,
                        totalItems: result.data.totalItems,
                    })
                } else {
                    this.setState({
                        dataSkills: []
                    })
                }
            });
    }
    getDataLovCategorys() { // get Lov data
        getDatasFinal('http://209.97.160.40:8080/SA061118/lov/SkillType')
            .then((result) => {
                this.setState({
                    dataCategoryNames: result.data
                })
            });
    }
    handleClickAction = (e, param) => {
        e.preventDefault();
        if (e.target.id === 'edit-skill') {
            this.state.dataSkills.map(skill =>
                skill.skill_id === param ? this.setState({ skillName: skill.skill_name, categorySkill: skill.skilltype_id, skillId: skill.skill_id, buttonName: 'Update' }) : null
            )
        } else if (e.target.id === 'delete-skill') {
            this.setState({ skillId: param })
            this.setState(prevState => ({
                modal: !prevState.modal
            }));
        }
    }
    handleClick(e) {
        if (e.target.name === 'reset') {
            skip = 0
            this.getDataSkills(skip)
            this.setState({
                categorySkill: '',
                skillName: '',
                isChange: false,
                buttonName: 'Submit'
            })
        }
        if (e.target.name === 'pagination') { // do when click pagination 
            skip = e.target.value
            this.getDataSkills(skip)
        }
        if (e.target.name === 'previous') {
            skip = 0
            this.getDataSkills(skip)
        }
        if (e.target.name === 'next') {
            skip = parseInt(total) - 1
            this.getDataSkills(skip)
        }
    }
    handleSubmit(event) {
        if (this.state.buttonName === 'Update') {
            body = JSON.stringify({ Model_ID: 'skill_id=' + this.state.skillId, skill_id: this.state.skillId, skill_name: this.state.skillName, skilltype_id: this.state.categorySkill })
            putDatas('http://209.97.160.40:8080/SA061118/allocation/MengelolaSkill/update?skill_id=' + this.state.skillId, body)
                .then((result) => {
                    this.setState({
                        message: result.message,
                        skillName: '',
                        categorySkill: '',
                        skillId: '',
                        buttonName: 'Submit',
                        visible: true
                    })
                    this.setTimeoutAlert()
                    this.getDataSkills(skip = 0)
                })
        } else if (this.state.buttonName === 'Submit') {
            body = JSON.stringify({ skilltype_id: parseInt(this.state.skillId, 10), skill_name: this.state.skillName })
            postDatas('http://209.97.160.40:8080/SA061118/allocation/MengelolaSkill/create', body)
                .then((result) => {
                    this.setState({
                        message: result.message,
                        categorySkill: '',
                        skillName: '',
                        visible: true
                    })
                    this.setTimeoutAlert()
                    this.getDataSkills(skip = 0)
                })
        }
        event.preventDefault()
    }
    handleChange(e) {
        if (e.target.name === 'categorySkill' && e.target.value !== '') {
            this.setState({ categorySkill: e.target.value })
        } else if (e.target.name === 'skillName') {
            this.setState({ skillName: e.target.value })
        }
    }
    setTimeoutAlert() {
        if (this.state.visible) {
            setTimeout(() => {
                this.setState({ visible: false })
            }, 3000);
        }
    }
    createPagination = () => {
        let element = []
        total = Math.ceil(this.state.totalItems / 10)
        for (let index = 0; index < total; index++) {
            element.push(
                <PaginationItem key={index} className={parseInt(skip) === index ? 'active' : ''}>
                    <PaginationLink value={index} onClick={this.handleClick} name="pagination">
                        {index + 1}
                    </PaginationLink>
                </PaginationItem>
            )
        }
        if (total > 5) {
            if (skip < 3) {
                return element.slice(0, 5)
            } else {
                var startMove = parseInt(skip) - 2
                var endMove = parseInt(skip) + 3
                if (startMove > (total - 5) && endMove > total) {
                    startMove = total - 5
                    endMove = total
                }
                return element.slice(startMove, endMove)
            }
        } else {
            return element.slice(0, 5)
        }
    }
    render() {
        return (
            <div>
                <Alert color="success" isOpen={this.state.visible} toggle={this.onDismiss} className="shadow-sm">
                    {this.state.message}
                </Alert>
                <Card className="rounded-0 shadow-sm animated fadeIn">
                    <CardBody className="px-5 py-5">
                        <Form onSubmit={this.handleSubmit}>
                            <Label>Add / Update Skill Name</Label>
                            <Row>
                                <Col sm={{ size: 4 }}>
                                    <FormGroup>
                                        <Input
                                            type="select"
                                            className="rounded-0"
                                            name="categorySkill"
                                            onChange={this.handleChange}
                                            value={this.state.categorySkill}
                                            required>
                                            <option value="">Please Choose...</option>
                                            {this.state.dataCategoryNames.map(categoryNames =>
                                                <option key={categoryNames.key} value={categoryNames.key}>{categoryNames.values.skilltype_skilltype_name}</option>
                                            )
                                            }
                                        </Input>
                                    </FormGroup>
                                </Col>
                                <Col sm={{ size: 4 }}>
                                    <FormGroup>
                                        <Input
                                            type="text"
                                            className="rounded-0"
                                            placeholder="Enter Skill Name"
                                            name="skillName"
                                            onChange={this.handleChange}
                                            value={this.state.skillName}
                                            required />
                                    </FormGroup>
                                </Col>
                                <Col sm={{ size: 2 }}>
                                    {this.state.categorySkill !== '' ? (this.state.skillName !== '' ?
                                        <Button className="rounded-0" type="submit" outline color="primary" name="submit" block>
                                            {this.state.buttonName}
                                        </Button> :
                                        <Button className="rounded-0" outline color="primary" block disabled>
                                            {this.state.buttonName}
                                        </Button>) :
                                        <Button className="rounded-0" outline color="primary" block disabled>
                                            {this.state.buttonName}
                                        </Button>
                                    }
                                </Col>
                                <Col sm={{ size: 2 }}>
                                    <Button className="rounded-0" type="reset" outline color="danger" name="reset" block onClick={this.handleClick}>
                                        Reset
                                    </Button>
                                </Col>
                            </Row>
                        </Form>
                    </CardBody>
                </Card>
                <Card className="rounded-0 shadow-sm animated fadeIn">
                    <CardBody>
                        <div className="clearfix px-3">
                            <div className="float-right caption-color">
                                <p>Total : {this.state.totalItems}</p>
                            </div>
                        </div>
                        <Table responsive hover bordered className="text-center">
                            <thead>
                                <tr>
                                    <th>Skill Id</th>
                                    <th>Category Name</th>
                                    <th>Skill Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.dataSkills.length < 1 ? <tr><td colSpan="4">0 data display</td></tr> :
                                    this.state.dataSkills.map(skill =>
                                        <tr key={skill.skill_id}>
                                            <th>{skill.skill_id}</th>
                                            <td>{skill.skilltype_name}</td>
                                            <td>{skill.skill_name}</td>
                                            <td>
                                                <Button className="rounded-0 mr-2" outline color="primary" id="edit-skill" onClick={(e) => this.handleClickAction(e, skill.skill_id)}>
                                                    <i className="fa fa-edit"></i>
                                                </Button>
                                                <Button className="rounded-0" outline color="danger" id="delete-skill" onClick={(e) => this.handleClickAction(e, skill.skill_id)}>
                                                    <i className="fa fa-trash"></i>
                                                </Button>
                                            </td>
                                        </tr>
                                    )}
                            </tbody>
                        </Table>
                        <div className="clearfix">
                            {Math.ceil(this.state.totalItems / 10) > 1 ?
                                <div className="float-right">
                                    <Pagination aria-label="Page navigation example">
                                        <PaginationItem className={parseInt(skip) === 0 ? "disabled ds-pagination" : ''} >
                                            <PaginationLink previous onClick={this.handleClick} name="previous" />
                                        </PaginationItem>
                                        {this.createPagination()}
                                        <PaginationItem className={parseInt(skip) === (total - 1) ? "disabled ds-pagination" : ''} >
                                            <PaginationLink next onClick={this.handleClick} name="next" />
                                        </PaginationItem>
                                    </Pagination>
                                </div>
                                : null}
                        </div>
                    </CardBody>
                </Card>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className} centered>
                    <ModalHeader toggle={this.toggle}>
                        <i className="fa fa-warning text-warning"></i> Warning
                    </ModalHeader>
                    <ModalBody>
                        Are you sure want to delete?
                    </ModalBody>
                    <ModalFooter>
                        <Button className="rounded-0" id="yes" color="primary" onClick={this.toggle}>Yes</Button>
                        <Button className="rounded-0" color="danger" onClick={this.toggle}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default ListSkill;