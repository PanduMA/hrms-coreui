import React, { Component } from 'react';
import { postDatas, getDatasFinal } from '../../services/APIFunction';
import Autosuggest from 'react-autosuggest';
import AutosuggestHighlightMatch from 'autosuggest-highlight/match';
import AutosuggestHighlightParse from 'autosuggest-highlight/parse';
import { Link } from 'react-router-dom';
import {
    Card,
    CardBody,
    FormGroup,
    Label,
    Col,
    Input,
    Row,
    Button,
    Form
} from 'reactstrap';
let body, list
class AddSDMSkillAllocation extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            value: '',
            sdmKey: '',
            suggestions: [],
            dataSDMNames: [],
            dataCategorys: [],
            dataSkills: [],
            categoryId: '',
            skillId: '',
            nik: '',
            valueSkill: '',
            addOption: 0,
            maxLayout: 4,
        })
        this.handleChange = this.handleChange.bind(this)
        this.handleKey = this.handleKey.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleClick = this.handleClick.bind(this)
        this.handleClickAdd = this.handleClickAdd.bind(this)
        this.handleClickMinus = this.handleClickMinus.bind(this)
        this.handleChangeNew = this.handleChangeNew.bind(this)
    }
    componentDidMount() {
        this.getDataSDMName()
        this.getDataLovCategory()
        this.setState({ layouts: [] })

    }
    getDataSDMName() {
        var paramFilter = 'sdm_status=1'
        var paramOrderBy = 'sdm_name ASC'
        getDatasFinal('http://209.97.160.40:8080/SA061118/lov/Sdm', '', '', paramFilter, paramOrderBy)
            .then((result) => {
                this.setState({
                    dataSDMNames: result.data
                })
            });
    }
    getSuggestions = value => {
        const inputValue = value.trim().toLowerCase();
        const inputLength = inputValue.length;

        return inputLength === 0 ? [] : this.state.dataSDMNames.filter(sdm =>
            sdm.values.sdm_sdm_name.toLowerCase().slice(0, inputLength) === inputValue
        );
    };
    getSuggestionValue = suggestion => suggestion.values.sdm_sdm_name;
    onChange = (event, { newValue }) => {
        this.setState({
            value: newValue,
        });
        var result = this.state.dataSDMNames.find(sdm => sdm.values.sdm_sdm_name === newValue)
        if (result) {
            this.setState({ sdmKey: Object.values(result)[1] })
            this.getDataNik(Object.values(result)[1])
        }
    };
    onSuggestionsFetchRequested = ({ value }) => {
        this.setState({
            suggestions: this.getSuggestions(value)
        });
    };
    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };
    renderSuggestion = (suggestion, { query }) => {
        const matches = AutosuggestHighlightMatch(suggestion.values.sdm_sdm_name, query)
        const parts = AutosuggestHighlightParse(suggestion.values.sdm_sdm_name, matches)
        return (
            <span>
                {parts.map((part, index) => {
                    const className = part.highlight ? 'react-autosuggest__suggestion-match' : null;
                    return (
                        <span className={className} key={index}>
                            {part.text}
                        </span>
                    );
                })}
            </span>
        );
    }
    getDataLovCategory() {
        getDatasFinal('http://209.97.160.40:8080/SA061118/lov/SkillType')
            .then(result => {
                this.setState({ dataCategorys: result.data })
            })
    }
    getDataLovSkill(id) {
        var paramFilter = 'skilltype_id=' + id
        getDatasFinal('http://209.97.160.40:8080/SA061118/lov/Skill', '', '', paramFilter)
            .then(result => {
                this.setState({ dataSkills: result.data })
            })
    }
    getDataNik(id) {
        var paramFilter = 'sdm_id=' + id
        getDatasFinal('http://209.97.160.40:8080/SA061118/lov/SdmNik', '', '', paramFilter)
            .then(result => {
                this.setState({
                    nik: Object.values(result.data)[0]['values']['sdm_sdm_nik']
                })
            })
    }
    handleSubmit(event) {
        body = JSON.stringify({
            "listsdm":
                [{
                    sdm_id: parseInt(this.state.sdmKey),
                    skilltype_id: parseInt(this.state.categoryId),
                    skill_id: parseInt(this.state.skillId),
                    sdmskill_value: parseInt(this.state.valueSkill)
                }]
        })
        this.createNewSkill(body)
        event.preventDefault()
    }
    createNewSkill(body) {
        postDatas('http://209.97.160.40:8080/SA061118/allocation/MultiInsertSdm/MultiCreate', body)
            .then((result) => {
                this.setState({
                    value: '',
                    categoryId: '',
                    skillId: '',
                    nik: '',
                    valueSkill: '',
                })
                window.location = "#/sdmallocation/list"
                localStorage.setItem("messageAlloc", result.message)
            })
    }
    handleChange(e) {
        if (e.target.name === 'category' && e.target.value !== '') {
            this.setState({ categoryId: e.target.value, skillId: '' })
            this.getDataLovSkill(e.target.value)
        } else if (e.target.name === 'skill' && e.target.value !== '') {
            this.setState({ skillId: e.target.value })
        } else {
            this.setState({ valueSkill: e.target.value })
        }
    }
    handleKey(e) {
        if (e.target.value.length > 1) { //if inputed more than 2 digit (start from 0)
            e.preventDefault();
        }
    }
    handleClick(e) {

    }
    handleClickAdd() {
        if (this.state.addOption < 4) {
            this.setState((prevState) => ({
                addOption: ++prevState.addOption,
            }))
        }
    }
    handleClickMinus() {
        this.setState((prevState) => ({
            addOption: --prevState.addOption,
        }))
    }
    handleChangeNew(e) {

    }
    render() {
        list = [
            <FormGroup row key="0">
                <Col sm={{ size: 3, offset: 1 }}>
                    <Input type="select" className="rounded-0" value={this.state.categoryId} onChange={this.handleChange} name="category" required>
                        <option value="">Choose Category Skill</option>
                        {this.state.dataCategorys.map(category =>
                            <option key={category.key} value={category.key}>{category.values.skilltype_skilltype_name}</option>
                        )}
                    </Input>
                </Col>
                <Col sm={{ size: 3 }}>
                    <Input type="select" className="rounded-0" value={this.state.skillId} onChange={this.handleChange} name="skill" required>
                        <option value="">Choose List Skill</option>
                        {this.state.dataSkills != null ? this.state.dataSkills.map(skill =>
                            <option key={skill.key} value={skill.key}>{skill.values.skills_skill_name}</option>
                        ) : ''}
                    </Input>
                </Col>
                <Col sm={{ size: 2 }}>
                    <Input
                        type="number"
                        className="rounded-0"
                        min="1"
                        max="10"
                        placeholder="Value"
                        value={this.state.valueSkill}
                        onKeyPress={this.handleKey}
                        onChange={this.handleChange}
                        required />
                </Col>
                <Col sm={{ size: 1 }}>
                    <Button className="rounded-0" outline color="primary" onClick={this.handleClickAdd}>
                        <i className="fa fa-plus"></i>
                    </Button>
                </Col>
            </FormGroup>
        ]
        for (var i = 0; i < this.state.maxLayout; i++) {
            list.push(
                this.state.addOption > i &&
                <FormGroup row key={i + 1}>
                    <Col sm={{ size: 3, offset: 1 }}>
                        <Input type="select" className="rounded-0" onChange={this.handleChangeNew} name="categoryId">
                            <option value="prompt">Choose Category Skill</option>
                            {this.state.dataCategorys.map(category =>
                                <option key={category.key} value={category.key}>{category.values.skilltype_skilltype_name}</option>
                            )}
                        </Input>
                    </Col>
                    <Col sm={{ size: 3 }}>
                        <Input type="select" className="rounded-0" onChange={this.handleChangeNew} name="skillId">
                            <option value="prompt">Choose List Skill</option>
                            {this.state.dataSkills != null ? this.state.dataSkills.map(skill =>
                                <option key={skill.key} value={skill.key}>{skill.values.skills_skill_name}</option>
                            ) : ''}
                        </Input>
                    </Col>
                    <Col sm={{ size: 2 }}>
                        <Input
                            type="number"
                            className="rounded-0"
                            name="valueSkill"
                            placeholder="Value"
                            min="1"
                            max="10"
                            onKeyPress={this.handleKey}
                            onChange={this.handleChangeNew}
                        />
                    </Col>
                    <Col sm={{ size: 1 }}>
                        <Button className="rounded-0" outline color="primary" onClick={this.handleClickMinus}>
                            <i className="fa fa-minus"></i>
                        </Button>
                    </Col>
                </FormGroup>
            )
        }
        const { value, suggestions } = this.state;
        const inputProps = {
            placeholder: 'Enter SDM Name',
            className: 'form-control rounded-0',
            value,
            onChange: this.onChange,
            required: true
        };
        return (
            <div>
                <Card className="rounded-0 shadow-sm animated fadeIn">
                    <CardBody className="px-5 py-5">
                        <Form onSubmit={this.handleSubmit}>
                            <FormGroup row>
                                <Label sm={{ size: 2, offset: 2 }} className="text-right">SDM Name</Label>
                                <Col sm={{ size: 4 }}>
                                    <Autosuggest
                                        suggestions={suggestions}
                                        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                                        onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                                        getSuggestionValue={this.getSuggestionValue}
                                        renderSuggestion={this.renderSuggestion}
                                        inputProps={inputProps}
                                    />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 2, offset: 2 }} className="text-right">NIK</Label>
                                <Col sm={{ size: 4 }}>
                                    <Input type="text" className="rounded-0" placeholder="NIK Generated" value={this.state.nik} readOnly />
                                </Col>
                            </FormGroup>
                            {list}
                            <Row>
                                <Col sm={{ size: 3, offset: 3 }}>
                                    <Button className="rounded-0" type="submit" outline color="primary" block>Save</Button>
                                </Col>
                                <Col sm={{ size: 3 }}>
                                    <Link to={`/sdmallocation/list`} style={{ textDecoration: 'none' }}>
                                        <Button className="rounded-0" outline color="danger" block>Back</Button>
                                    </Link>
                                </Col>
                            </Row>
                        </Form>
                    </CardBody>
                </Card>
            </div>
        );
    }
}

export default AddSDMSkillAllocation;