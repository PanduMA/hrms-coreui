import React, { Component } from 'react';
import {
    Card,
    CardHeader,
    CardBody,
    Form,
    FormGroup,
    Label,
    Input,
    Row,
    Col
} from 'reactstrap';
import DateTime from 'react-datetime';

class LatihanForm extends Component {
    constructor(props){
        super(props)
        this.state =({
            noRekening:'',
            nominal:'',
            jenisKartu:'',
            tipeChannel:'',
            waktuKejadian:'',
            namaPetugas:''
        })
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeJenisKartu = this.handleChangeJenisKartu.bind(this);
        this.handleChangeTipeChannel = this.handleChangeTipeChannel.bind(this);
        this.handleChangeNominal = this.handleChangeNominal.bind(this);
        this.handleChangeNamaPetugas = this.handleChangeNamaPetugas.bind(this);
        this.handleClickReset = this.handleClickReset.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(event){
        this.setState({
            noRekening : event.target.value
        })
    }
    handleChangeJenisKartu(event){
        this.setState({
            jenisKartu:event.target.value
        })
    }
    handleChangeTipeChannel(event){
        this.setState({
            tipeChannel:event.target.value
        })
    }
    handleChangeWaktuKejadian = (input) => { 
        this.setState({
            waktuKejadian:input.format('YYYY-MM-DD HH:mm:ss')
        })
    }
    handleChangeNominal(event){
        this.setState({
            nominal:event.target.value
        })
    }
    handleChangeNamaPetugas(event){
        this.setState({
            namaPetugas:event.target.value
        })
    }
    handleClickReset(){
        this.clearData();
    }
    clearData(){
        this.setState({
            noRekening:'',
            nominal:'',
            jenisKartu:'',
            tipeChannel:'',
            waktuKejadian:'',
            namaPetugas:''
        })
    }
    handleSubmit(event){
        event.preventDefault();
        this.insertData();
    }
    insertData(){
        fetch('http://10.10.10.149:5000/fraud/insert-fraud-data', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                jenisKartu: this.state.jenisKartu,
                namaPetugas: this.state.namaPetugas,
                nominal: this.state.nominal,
                noRek: this.state.noRekening,
                tipeChannel: this.state.tipeChannel,
                waktuKejadian: this.state.waktuKejadian
            })
        }).then((response) => {
            return response.json();
        }).then((result) => {
            console.log(result)
            this.clearData();
            window.location.reload();
        })
    }

    render() {
        return (
            <Card>
                <CardHeader>
                    <strong>Fraud Report </strong>
                    <small>Form</small>
                </CardHeader>
                <CardBody>
                    <Form onSubmit={this.handleSubmit}>
                        <FormGroup>
                            <Label>No Rekening</Label>
                            <Input type="text" required value={this.state.noRekening} onChange={this.handleChange} placeholder="Masukkan Nomor Rekening"/>
                        </FormGroup>
                        <FormGroup>
                            <Label>Jenis Kartu</Label>
                            <Input type="select" value={this.state.jenisKartu} onChange={this.handleChangeJenisKartu} required>
                                <option defaultValue>-- Pilih Jenis Kartu --</option>
                                <option value="SIMPEDES">SIMPEDES</option>
                                <option value="BRITAMA">BRITAMA</option>
                                <option value="PRIORITAS">PRIORITAS</option>
                            </Input>
                        </FormGroup>
                        <FormGroup>
                            <Label>Tipe Channel</Label>
                            <Input type="select" required value={this.state.tipeChannel} onChange={this.handleChangeTipeChannel}>
                                <option defaultValue>-- Pilih Tipe Channel --</option>
                                <option value="EDC">EDC</option>
                                <option value="INTERNET BANKING">INTERNET BANKING</option>
                                <option value="SMS BANKING">SMS BANKING</option>
                                <option value="ATM">ATM</option>
                            </Input>
                        </FormGroup>
                        <FormGroup>
                            <Label>Waktu Kejadian</Label>
                            <DateTime value={this.state.waktuKejadian} onBlur={this.handleChangeWaktuKejadian} timeFormat="HH:mm:ss" />
                        </FormGroup>
                        <FormGroup>
                            <Label>Nominal</Label>
                            <Input type="text" required value={this.state.nominal} onChange={this.handleChangeNominal} placeholder="Masukkan Nominal"/>
                        </FormGroup>
                        <FormGroup>
                            <Label>Nama Petugas</Label>
                            <Input type="select" required value={this.state.namaPetugas} onChange={this.handleChangeNamaPetugas}>
                                <option defaultValue>-- Pilih Nama Petugas --</option>
                                <option value="Pandu">Pandu</option>
                                <option value="Fega">Fega</option>
                                <option value="Aristo">Aristo</option>
                                <option value="Ilham">Ilham</option>
                            </Input>
                        </FormGroup>
                        <Row>
                        <Col sm={{size:2, offset:8}}>
                            <Input type="submit" value="Submit" className="btn btn-outline-primary"/>
                        </Col>
                        <Col sm={{size:2}}>
                            <Input type="reset" value="Reset" className="btn btn-outline-danger" onClick={this.handleClickReset} />
                        </Col>
                        </Row>
                    </Form>
                </CardBody>
            </Card>
        );
    }
}

export default LatihanForm;