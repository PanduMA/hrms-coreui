import React, { Component } from 'react';
import {Card,CardHeader,CardBody} from 'reactstrap';
import {Pie} from 'react-chartjs-2';

class PieChart extends Component {
    constructor(props){
        super(props)
        this.state = {
            label:[],
            data:[]
        }
    }
    componentWillMount(){
        this.readPieChart();
    }
    readPieChart(){
        fetch('http://10.10.10.149:5000/dashboard/get-data-for-chart', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: null
        }).then((response) => {
            return response.json();
        }).then((result) => {
            this.setState({
                label:result['labels'],
                data:result['data']
            });
        });
    }
    render() {
        const pie = {
            labels: (this.state.label),
            datasets: [
                {
                data: (this.state.data),
                backgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56',
                ],
                hoverBackgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56',
                ],
                }],
            };
        return (
            <Card>
                <CardHeader>
                    <strong>Pie Chart</strong>
                </CardHeader>
                <CardBody>
                    <Pie data={pie} />
                </CardBody>
            </Card>
        );
    }
}

export default PieChart;