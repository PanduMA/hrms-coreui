import React, { Component } from 'react';
import {Card,CardHeader,CardBody} from 'reactstrap';
import {Line} from 'react-chartjs-2';

class LineChart extends Component {
    constructor(props){
        super(props)
        this.state = ({
            label:[],
            data:[]
        })
    }
    componentWillMount(){
        this.readLineChart();
    }
    readLineChart() {
        fetch('http://10.10.10.149:5000/dashboard/get-data-for-chart', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: null
        }).then((response) => {
            return response.json();
        }).then((result) => {
            this.setState({
                label:result['labels'],
                data:result['data']
            });
        });
    }
    render() {
        const line = {
            labels: (this.state.label),
            datasets: [
              {
                label: 'Jenis Kartu',
                fill: false,
                lineTension: 0.1,
                backgroundColor: 'rgba(75,192,192,0.4)',
                borderColor: 'rgba(75,192,192,1)',
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: 'rgba(75,192,192,1)',
                pointBackgroundColor: '#fff',
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                pointHoverBorderColor: 'rgba(220,220,220,1)',
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: (this.state.data),
              },
            ],
          };
        return (
            <Card>
                <CardHeader>
                    <strong>Line Chart</strong>
                </CardHeader>
                <CardBody>
                    <Line data={line}/> 
                </CardBody>
            </Card>
        );
    }
}

export default LineChart;