import React, { Component } from 'react';
import {
    Card,
    CardHeader,
    CardBody,
    Table,
} from 'reactstrap';
class ReadData extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            dataFraud: [],
            dataSiswa : []
        })
    }
    componentWillMount() {
        this.loadFraudData();
        this.loadSiswa();
    }
    loadSiswa(){
        fetch('http://10.10.10.57:3000/siswa', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: null
        }).then((response) => {
            return response.json();
        }).then((result) => {
            this.setState({
                dataSiswa: result,
            });
        });
    }
    loadFraudData() {
        fetch('http://10.10.10.149:5000/fraud/get-data-fraud', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: null
        }).then((response) => {
            return response.json();
        }).then((result) => {
            this.setState({
                dataFraud: result,
            });
        });
    }
    render() {
        return (
            <Card>
                <CardHeader>
                    <strong>Tabel Data Fraud</strong>
                </CardHeader>
                <CardBody>
                    <Table className="table-responsive">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Id</th>
                                <th>Jenis Kartu</th>
                                <th>Nama Petugas</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.dataSiswa.map((row, index) =>
                                <tr key={index}>
                                    <td>{index + 1}</td>
                                    <td>{row.id}</td>
                                    <td>{row.nis}</td>
                                    <td>{row.jenkel}</td>
                                </tr>
                            )}
                        </tbody>
                    </Table>
                </CardBody>
            </Card>
        );
    }
}

export default ReadData;