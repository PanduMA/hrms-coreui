import React, { Component } from 'react';
import { 
    Card, 
    CardHeader, 
    CardBody, 
    Table, 
    Badge,
    Row,
    Col
} from 'reactstrap';
import PieChart from '../Chart/PieChart';
import LineChart from '../Chart/LineChart';
class Tabel extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            dataTransaksi: [],
            totalItem: ''
        })
    }
    componentWillMount() {
        this.loadFraudData();
    }
    loadFraudData() {
        fetch('http://10.10.10.149:5000/dashboard/get-last-twenty-trx', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: null
        }).then((response) => {
            return response.json();
        }).then((result) => {
            this.setState({
                dataTransaksi: result['data'],
                totalItem: result['totalItems'],
            });
        });
    }
    render() {
        return (
            <div className="tabel">
                <Row>
                    <Col md={{size:6}}>
                        <LineChart />
                    </Col>
                    <Col md={{size:6}}>
                        <PieChart />
                    </Col>
                </Row>
                <Card>
                    <CardHeader>
                        <strong>Tabel Data </strong>
                        <div className="card-header-actions">
                            <h5><Badge pill color="info" className="float-right">Total Transaksi : {this.state.totalItem}</Badge></h5>
                        </div>
                    </CardHeader>
                    <CardBody>
                        <Table className="table-responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Jenis Kartu</th>
                                    <th>Jenis TRX</th>
                                    <th>No Rekening</th>
                                    <th>Status</th>
                                    <th>Status Code</th>
                                    <th>Timestamp</th>
                                    <th>Tipe Channel</th>
                                    <th>TRX Amount</th>
                                    <th>Tunai</th>
                                </tr>
                            </thead>
                            <tbody>
                                    {this.state.dataTransaksi.map((row, index) =>
                                        <tr key={index}>
                                            <td>{index + 1}</td>
                                            <td>{row.jenis_kartu}</td>
                                            <td>{row.jenis_trx}</td>
                                            <td>{row.norek}</td>
                                            <td>{row.status}</td>
                                            <td>{row.status_code}</td>
                                            <td>{row.timestamp}</td>
                                            <td>{row.tipe_channel}</td>
                                            <td>{row.trx_amount}</td>
                                            <td>{row.tunai === true ? 'Ya' : 'Tidak'}</td>
                                        </tr>
                                    )}
                            </tbody>
                        </Table>
                    </CardBody>
                </Card>
            </div>
        );
    }
}

export default Tabel;