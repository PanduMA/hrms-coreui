import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { getDatasFinal } from '../../services/APIFunction';
import {
  Row, Col,
  Card, CardBody
} from 'reactstrap';
class Dashboard extends Component {
  constructor(props) {
    super(props)
    this.state = ({
      totalSDM: 0,
      totalClients: 0,
      totalPsyco: 0,
      totalAssignment: 0,
    })
  }
  componentDidMount() {
    this.getDataTotalItems()
  }
  getDataTotalItems() {
    getDatasFinal('http://209.97.160.40:8080/SA061118/sdm/MengelolaSdm/readAll') //sdm
      .then((result) => {
        if (result.data !== null) {
          this.setState({
            totalSDM: result.data.totalItems
          })
        }
      })
    getDatasFinal('http://209.97.160.40:8080/SA061118/project/MengelolaClient/readAll') //client
      .then((result) => {
        this.setState({
          totalClients: result.data.totalItems
        })
      });
    getDatasFinal('http://209.97.160.40:8080/SA061118/sdm/SdmPsycological/readAll') //pscyo
      .then((result) => {
        this.setState({
          totalPsyco: result.data.items.length
        })
      });
    getDatasFinal('http://209.97.160.40:8080/SA061118/project/SdmAssignment/readAll') //assignment
      .then((result) => {
        this.setState({
          totalAssignment: result.data.totalItems,
        })
      });
  }
  render() {
    const user = this.props.user;
    if (!user.user_id) {
      return (<Redirect to="/" />);
    }
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" sm="6" lg="3">
            <Card className="text-white bg-info rounded-0 shadow-sm">
              <CardBody>
                <div className="float-left pl-3">
                  <i className="fa fa-3x fa-group"></i>
                </div>
                <div className="float-right pr-3">
                  <div className="text-value">{this.state.totalSDM}</div>
                  <div>Jumlah SDM</div>
                </div>
              </CardBody>
            </Card>
          </Col>
          <Col xs="12" sm="6" lg="3">
            <Card className="text-white bg-success rounded-0 shadow-sm">
              <CardBody>
                <div className="float-left pl-3">
                  <i className="fa fa-3x fa-address-book-o"></i>
                </div>
                <div className="float-right pr-3">
                  <div className="text-value">{this.state.totalClients}</div>
                  <div>Jumlah Client</div>
                </div>
              </CardBody>
            </Card>
          </Col>
          <Col xs="12" sm="6" lg="3">
            <Card className="text-white bg-warning rounded-0 shadow-sm">
              <CardBody>
                <div className="float-left pl-2">
                  <i className="fa fa-3x fa-stethoscope"></i>
                </div>
                <div className="float-right pr-2">
                  <div className="text-value">{this.state.totalPsyco}</div>
                  <div>Jumlah SDM Pscology</div>
                </div>
              </CardBody>
            </Card>
          </Col>
          <Col xs="12" sm="6" lg="3">
            <Card className="text-white bg-danger rounded-0 shadow-sm">
              <CardBody>
                <div className="float-left">
                  <i className="fa fa-3x fa-send-o"></i>
                </div>
                <div className="float-right">
                  <div className="text-value">{this.state.totalAssignment}</div>
                  <div>Jumlah SDM Assignment</div>
                </div>
              </CardBody>
            </Card>
          </Col>
          <Col xs="12">
            <Card>
              <CardBody>
                <p>Ini Dashboard</p>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}
Dashboard = connect(
  state => ({
    user: state.user
  }),
)(Dashboard);
export default Dashboard;
