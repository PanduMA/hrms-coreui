import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { actionTryLogin, setUser } from '../../../services/Actions';
import {
  Card, CardBody,
  Col, Container, Row,
  FormGroup, Form, Button,
  Spinner, InputGroup, InputGroupAddon, InputGroupText
} from 'reactstrap';


const renderInput = ({ placeholder, type, input, className, maxlength }) => (
  <input
    required
    type={type}
    placeholder={placeholder}
    className={className}
    maxLength={maxlength}
    {...input}
  />
);
class Login extends Component {
  componentWillMount() {
    this.props.checkUser();
  }
  render() {
    const isFetching = this.props.isFetching;
    const user = this.props.user;
    if (user.user_id) {
      return (<Redirect to="/home" />);
    }
    return (
      <div className="app flex-row align-items-center animated fadeIn">
        <Container>
          <Row className="justify-content-center">
            <Col md="5">
              <Card className="p-5 shadow-lg bg-white rounded-0">
                <CardBody>
                  <Form onSubmit={this.props.handleSubmit}>
                    <h1 className="text-center mb-5">Login</h1>
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="append">
                          <InputGroupText style={{ backgroundColor: '#FFFFFF', color: '#20A8D8' }}>
                            <i className="fa fa-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Field
                          name="username"
                          component={renderInput}
                          maxlength="25"
                          type="text"
                          placeholder="Input username"
                          className="form-control rounded-0" />
                      </InputGroup>
                    </FormGroup>
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="append">
                          <InputGroupText style={{ backgroundColor: '#FFFFFF', color: '#20A8D8' }}>
                            <i className="fa fa-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Field
                          name="password"
                          maxlength="12"
                          component={renderInput}
                          type="password"
                          placeholder="Input correct password"
                          className="form-control rounded-0" />
                      </InputGroup>
                    </FormGroup>
                    <div>
                      {isFetching ? (
                        <div className="row justify-content-center align-items-center">
                          <Spinner color="primary" />
                        </div>
                      ) : <Button outline color="primary" className="px-4 rounded-0" block type="submit">
                          Login <i className="fa fa-angle-right ml-1"></i>
                        </Button>
                      }
                    </div>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

Login = reduxForm({
  form: 'Login'
})(Login);
Login = connect(
  state => ({
    isFetching: state.isFetching,
    user: state.user
  }),
  (dispatch, ownProps) => ({
    onSubmit: values => {
      dispatch(actionTryLogin(values));
    },
    checkUser: () => {
      const user = localStorage.getItem('user');
      if (user !== null) dispatch(setUser(JSON.parse(user)));
    }
  })
)(Login);
export default Login;
