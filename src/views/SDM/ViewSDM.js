import React, { Component } from 'react';
import {
    TabContent, TabPane,
    Nav, NavItem, NavLink,
    Card, CardBody, CardFooter, Button,
    Row, Col, Table
} from 'reactstrap';
import classnames from 'classnames';
import { getDatasFinal } from '../../services/APIFunction';
import { Link } from 'react-router-dom';
import { lang } from 'moment';
const styleTitle = {
    width: '30%'
}
const styleColon = {
    width: '1%'
}
const styleValue = {
    width: '69%'
}
let url, paramFilter
class ViewSDM extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            activeTab: '1',
            dataPribadiSDM: [],
            dataEducations: [],
            dataCourses: [],
            dataEmployments: [],
            dataProfils: [],
            dataLanguages: []

        });
        this.toggle = this.toggle.bind(this);
    }
    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }
    componentDidMount() {
        paramFilter = 'sdm_id=' + this.props.match.params.id
        this.getDataPribadiSDM()
        this.getDataEducation()
        this.getDataCourse()
        this.getDataEmployment()
        this.getDataProfil()
        this.getDataLanguage()
        if (window.location.href.search(/list/) >= 0) {
            url = true
        } else {
            url = false
        }
    }
    getDataPribadiSDM() {
        getDatasFinal('http://209.97.160.40:8080/SA061118/sdm/mengelolaSdm/readAll', '', '', paramFilter)
            .then((result) => {
                this.setState({
                    dataPribadiSDM: result.data.items
                })
            });
    }
    getDataEducation() {
        getDatasFinal('http://209.97.160.40:8080/SA061118/sdm/Education/readAll', '', '', paramFilter)
            .then((result) => {
                if (result.data.items.length > 0) {
                    this.setState({
                        dataEducations: result.data.items
                    })
                } else {
                    // masih defect sama seperti di screen hiring,assignment ketika mencari based on client
                    return (<tr><td colSpan="5">0 data display</td></tr>)
                }

            })
    }
    getDataCourse() {
        getDatasFinal('http://209.97.160.40:8080/SA061118/sdm/course/readAll', '', '', paramFilter)
            .then((result) => {
                if (result.data.items.length > 0) {
                    this.setState({
                        dataCourses: result.data.items
                    })
                } else {
                    // masih defect sama seperti di screen hiring,assignment ketika mencari based on client
                    return (<tr><td colSpan="6">0 data display</td></tr>)
                }

            })
    }
    getDataEmployment() {
        getDatasFinal('http://209.97.160.40:8080/SA061118/sdm/employment/readAll', '', '', paramFilter)
            .then((result) => {
                if (result.data.items.length > 0) {
                    this.setState({
                        dataEmployments: result.data.items
                    })
                } else {
                    // masih defect sama seperti di screen hiring,assignment ketika mencari based on client
                    return (<tr><td colSpan="4">0 data display</td></tr>)
                }

            })
    }
    getDataProfil() {
        getDatasFinal('http://209.97.160.40:8080/SA061118/sdm/profiling/readAll', '', '', paramFilter)
            .then((result) => {
                if (result.data.items.length > 0) {
                    this.setState({
                        dataProfils: result.data.items
                    })
                } else {
                    // masih defect sama seperti di screen hiring,assignment ketika mencari based on client
                    return (<tr><td colSpan="2">0 data display</td></tr>)
                }

            })
    }
    getDataLanguage() {
        getDatasFinal('http://209.97.160.40:8080/SA061118/allocation/mengelolaSDMLanguages/readAll', '', '', paramFilter)
            .then((result) => {
                if (result.data.items.length > 0) {
                    this.setState({
                        dataLanguages: result.data.items
                    })
                }
            })
    }
    render() {
        return (
            <div>
                <Card className="rounded-0 shadow-sm animated fadeIn">
                    <CardBody>
                        <Nav tabs>
                            <NavItem>
                                <NavLink
                                    className={classnames({ active: this.state.activeTab === '1' })}
                                    onClick={() => { this.toggle('1'); }}>
                                    Data Pribadi
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    className={classnames({ active: this.state.activeTab === '2' })}
                                    onClick={() => { this.toggle('2'); }}>
                                    Pendidikan
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    className={classnames({ active: this.state.activeTab === '3' })}
                                    onClick={() => { this.toggle('3'); }}>
                                    Kursus
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    className={classnames({ active: this.state.activeTab === '4' })}
                                    onClick={() => { this.toggle('4'); }}>
                                    Pekerjaan
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    className={classnames({ active: this.state.activeTab === '5' })}
                                    onClick={() => { this.toggle('5'); }}>
                                    Profil
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    className={classnames({ active: this.state.activeTab === '6' })}
                                    onClick={() => { this.toggle('6'); }}>
                                    Bahasa
                                </NavLink>
                            </NavItem>
                        </Nav>
                        <TabContent activeTab={this.state.activeTab}>
                            <TabPane tabId="1">
                                <Row>
                                    <Col sm="12">
                                        <table style={{ 'fontSize': '16px' }} className="table">
                                            <tbody>
                                                <tr>
                                                    <td style={styleTitle}>Nama Lengkap</td>
                                                    <td style={styleColon}>:</td>
                                                    <td style={styleValue}>
                                                        {this.state.dataPribadiSDM.map(sdm =>
                                                            sdm.sdm_name
                                                        )}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style={styleTitle}>NIK</td>
                                                    <td style={styleColon}>:</td>
                                                    <td style={styleValue}>
                                                        {this.state.dataPribadiSDM.map(sdm =>
                                                            sdm.sdm_nik
                                                        )}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style={styleTitle}>No. KTP</td>
                                                    <td style={styleColon}>:</td>
                                                    <td style={styleValue}>
                                                        {this.state.dataPribadiSDM.map(sdm =>
                                                            sdm.sdm_ktp
                                                        )}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style={styleTitle}>Jenis Kelamin</td>
                                                    <td style={styleColon}>:</td>
                                                    <td style={styleValue}>
                                                        {this.state.dataPribadiSDM.map(sdm =>
                                                            sdm.gender_id === 1 ? 'Laki-laki' : 'Perempuan'
                                                        )}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style={styleTitle}>Tempat Lahir</td>
                                                    <td style={styleColon}>:</td>
                                                    <td style={styleValue}>
                                                        {this.state.dataPribadiSDM.map(sdm =>
                                                            sdm.sdm_placebirth
                                                        )}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style={styleTitle}>Tanggal Lahir</td>
                                                    <td style={styleColon}>:</td>
                                                    <td style={styleValue}>
                                                        {this.state.dataPribadiSDM.map(sdm =>
                                                            sdm.sdm_datebirth
                                                        )}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style={styleTitle}>Alamat</td>
                                                    <td style={styleColon}>:</td>
                                                    <td style={styleValue}>
                                                        {this.state.dataPribadiSDM.map(sdm =>
                                                            sdm.sdm_address
                                                        )}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style={styleTitle}>Kode POS</td>
                                                    <td style={styleColon}>:</td>
                                                    <td style={styleValue}>
                                                        {this.state.dataPribadiSDM.map(sdm =>
                                                            sdm.sdm_postcode
                                                        )}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style={styleTitle}>No Telepon</td>
                                                    <td style={styleColon}>:</td>
                                                    <td style={styleValue}>
                                                        {this.state.dataPribadiSDM.map(sdm =>
                                                            sdm.sdm_phone
                                                        )}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style={styleTitle}>Email</td>
                                                    <td style={styleColon}>:</td>
                                                    <td style={styleValue}>
                                                        {this.state.dataPribadiSDM.map(sdm =>
                                                            sdm.sdm_email
                                                        )}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style={styleTitle}>LinkedIn</td>
                                                    <td style={styleColon}>:</td>
                                                    <td style={styleValue}>
                                                        {this.state.dataPribadiSDM.map(sdm =>
                                                            sdm.sdm_linkedin
                                                        )}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style={styleTitle}>Agama</td>
                                                    <td style={styleColon}>:</td>
                                                    <td style={styleValue}>
                                                        {this.state.dataPribadiSDM.map(sdm =>
                                                            sdm.religion_id === 1 ? 'Islam' :
                                                                sdm.religion_id === 2 ? 'Christian' :
                                                                    sdm.religion_id === 3 ? 'Protestan' :
                                                                        sdm.religion_id === 4 ? 'Hindu' :
                                                                            sdm.religion_id === 5 ? 'Buddhist' : null
                                                        )}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style={styleTitle}>Kesehatan</td>
                                                    <td style={styleColon}>:</td>
                                                    <td style={styleValue}>
                                                        {this.state.dataPribadiSDM.map(sdm =>
                                                            sdm.health_id === 1 ? 'Bad' :
                                                                sdm.health_id === 2 ? 'Good' :
                                                                    sdm.health_id === 3 ? 'Excellent' :
                                                                        sdm.health_id === 4 ? 'Very Good' :
                                                                            sdm.health_id === 5 ? 'Healthy' : null
                                                        )}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style={styleTitle}>Objektif</td>
                                                    <td style={styleColon}>:</td>
                                                    <td style={styleValue}>
                                                        {this.state.dataPribadiSDM.map(sdm =>
                                                            sdm.sdm_objective
                                                        )}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style={styleTitle}>Level</td>
                                                    <td style={styleColon}>:</td>
                                                    <td style={styleValue}>
                                                        {this.state.dataPribadiSDM.map(sdm =>
                                                            sdm.sdm_level
                                                        )}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style={styleTitle}>Status</td>
                                                    <td style={styleColon}>:</td>
                                                    <td style={styleValue}>
                                                        {this.state.dataPribadiSDM.map(sdm =>
                                                            sdm.contracttype
                                                        )}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style={styleTitle}>Penempatan</td>
                                                    <td style={styleColon}>:</td>
                                                    <td style={styleValue}>
                                                        {this.state.dataPribadiSDM.map(sdm =>
                                                            sdm.sdm_contractloc === '1' ? 'Bandung' : 'Luar Bandung'
                                                        )}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style={styleTitle}>Awal Kontrak</td>
                                                    <td style={styleColon}>:</td>
                                                    <td style={styleValue}>
                                                        {this.state.dataPribadiSDM.map(sdm =>
                                                            sdm.sdm_startcontract
                                                        )}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style={styleTitle}>Akhir Kontrak</td>
                                                    <td style={styleColon}>:</td>
                                                    <td style={styleValue}>
                                                        {this.state.dataPribadiSDM.map(sdm =>
                                                            sdm.sdm_endcontract
                                                        )}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style={styleTitle}>Posisi</td>
                                                    <td style={styleColon}>:</td>
                                                    <td style={styleValue}>
                                                        ???
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </Col>
                                </Row>
                            </TabPane>
                            <TabPane tabId="2">
                                <Table responsive hover bordered className="text-center">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Sekolah</th>
                                            <th>Jurusan</th>
                                            <th>Tingkat</th>
                                            <th>Tahun Masuk</th>
                                            <th>Tahun Keluar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.dataEducations.length < 1 ? <tr><td colSpan="5">0 data display</td></tr> :
                                            this.state.dataEducations.map(education =>
                                                <tr key={education.norut + 1}>
                                                    <th>{education.norut + 1}</th>
                                                    <td>{education.edu_name}</td>
                                                    <td>{education.edu_subject}</td>
                                                    <td>{education.degree_name}</td>
                                                    <td>{education.edu_startdate}</td>
                                                    <td>{education.edu_enddate}</td>
                                                </tr>
                                            )}
                                    </tbody>
                                </Table>
                            </TabPane>
                            <TabPane tabId="3">
                                <Table responsive hover bordered className="text-center">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Title</th>
                                            <th>Penyelenggara</th>
                                            <th>Tempat</th>
                                            <th>Durasi</th>
                                            <th>Tanggal</th>
                                            <th>Sertifikat</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.dataCourses.length < 1 ? <tr><td colSpan="6">0 data display</td></tr> :
                                            this.state.dataCourses.map(course =>
                                                <tr key={course.norut + 1}>
                                                    <th>{course.norut + 1}</th>
                                                    <td>{course.course_title}</td>
                                                    <td>{course.course_provider}</td>
                                                    <td>{course.course_place}</td>
                                                    <td>{course.course_duration}</td>
                                                    <td>{course.course_date}</td>
                                                    <td>{course.course_certificates}</td>
                                                </tr>
                                            )}
                                    </tbody>
                                </Table>
                            </TabPane>
                            <TabPane tabId="4">
                                <Table responsive hover bordered className="text-center">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Perusahaan</th>
                                            <th>Tanggal Masuk</th>
                                            <th>Tanggal Keluar</th>
                                            <th>Jabatan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.dataEmployments.length < 1 ? <tr><td colSpan="4">0 data display</td></tr> :
                                            this.state.dataEmployments.map(employment =>
                                                <tr key={employment.norut + 1}>
                                                    <th>{employment.norut + 1}</th>
                                                    <td>{employment.employment_corpname}</td>
                                                    <td>{employment.employment_startdate}</td>
                                                    <td>{employment.employment_enddate}</td>
                                                    <td>{employment.employment_rolejob}</td>
                                                </tr>
                                            )}
                                    </tbody>
                                </Table>
                            </TabPane>
                            <TabPane tabId="5">
                                <Table responsive hover bordered className="text-center">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Profil</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.dataProfils.length < 1 ? <tr><td colSpan="2">0 data display</td></tr> :
                                            this.state.dataProfils.map(profil =>
                                                <tr key={profil.norut + 1}>
                                                    <th>{profil.norut + 1}</th>
                                                    <td>{profil.profiling_name}</td>
                                                </tr>
                                            )}
                                    </tbody>
                                </Table>
                            </TabPane>
                            <TabPane tabId="6">
                                <Table responsive hover bordered className="text-center">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Bahasa</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.dataLanguages.length < 1 ? <tr><td colSpan="2">0 data display</td></tr> :
                                            this.state.dataLanguages.map(language =>
                                                <tr key={language.norut}>
                                                    <th>{language.norut}</th>
                                                    <td>{language.language_name}</td>
                                                </tr>
                                            )}
                                    </tbody>
                                </Table>
                            </TabPane>
                        </TabContent>
                    </CardBody>
                    <CardFooter>
                        <Link to={url ? `/sdm/list` : `/sdm/historis`}>
                            <Button className="rounded-0" outline color="primary">Back</Button>
                        </Link>
                    </CardFooter>
                </Card>
            </div>
        );
    }
}

export default ViewSDM;