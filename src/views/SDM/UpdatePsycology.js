import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { putDatas, getDatasFinal, getDatas } from '../../services/APIFunction';
import {
    Card, CardBody,
    Form, FormGroup, Label, Input, Button,
    Col, Row,
} from 'reactstrap';
let body
class UpdatePsycology extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            dataPsycologys: [],
            dataLovPsycologys: [],
            idCondition: '',
            sdmId: '',
            sdmName: '',
            description: '',
            date: '',
            sdmPsycoId: '',
            isChange: false
        })
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    componentDidMount() {
        this.getDataPsycologys(this.props.match.params.id)
        this.getDataLovPsycologys()
    }
    getDataPsycologys(id) {
        getDatas('http://209.97.160.40:8080/SA061118/sdm/sdmPsycological/readAll?sdmpsycological_id=' + id) //gabisa pake getDatasFinal
            .then((result) => {
                this.setState({
                    dataPsycologys: result.data.items
                })
                this.state.dataPsycologys.map(psyco =>
                    this.setState({
                        idCondition: psyco.psyco_id,
                        sdmId: psyco.sdm_id,
                        sdmName: psyco.sdm_name,
                        description: psyco.sdmpsycological_desc,
                        date: psyco.psycological_date,
                        sdmPsycoId: psyco.sdmpsycological_id
                    })
                )
            });
    }
    getDataLovPsycologys() {
        getDatasFinal('http://209.97.160.40:8080/SA061118/lov/psychologicals')
            .then((result) => {
                this.setState({
                    dataLovPsycologys: result.data
                })
            })
    }
    handleChange(e) {
        if (e.target.name === 'description') {
            this.setState({
                description: e.target.value
            })
        } else if (e.target.name === 'date') {
            this.setState({
                date: e.target.value
            })
        } else {
            this.setState({
                idCondition: e.target.value
            })
        }
        this.setState({ isChange: true })
    }
    handleSubmit(event) {
        body = JSON.stringify({
            Model_ID: 'sdmpsycological_id=' + this.props.match.params.id,
            psyco_id: parseInt(this.state.idCondition),
            psycological_date: this.state.date,
            sdm_id: parseInt(this.state.sdmId),
            sdmpsycological_desc: this.state.description,
            sdmpsycological_id: parseInt(this.props.match.params.id)
        })
        event.preventDefault();
        this.updateDataPsyco(body)
    }
    updateDataPsyco(body) {
        putDatas('http://209.97.160.40:8080/SA061118/sdm/sdmPsycological/update?sdmpsycological_id=' + parseInt(this.props.match.params.id), body)
            .then((result) => {
                this.setState({
                    idCondition: '',
                    sdmId: '',
                    sdmName: '',
                    description: '',
                    date: '',
                    sdmPsycoId: '',
                })
                window.location = "#/sdm/psycology"
                localStorage.setItem("messagePsco", result.message)
            })
    }
    render() {
        return (
            <div>
                <Card className="rounded-0 shadow-sm animated fadeIn">
                    <CardBody className="px-5 py-5">
                        <Form onSubmit={this.handleSubmit}>
                            <FormGroup row>
                                <Label sm={{ size: 3, offset: 1 }} className="text-right">SDM Name</Label>
                                <Col sm={{ size: 5 }}>
                                    <Input
                                        type="text"
                                        className="rounded-0"
                                        value={this.state.sdmName}
                                        readOnly />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 3, offset: 1 }} className="text-right">Condition</Label>
                                <Col sm={{ size: 5 }}>
                                    <Input
                                        type="select"
                                        className="rounded-0"
                                        onChange={this.handleChange}
                                        value={this.state.idCondition}>
                                        {this.state.dataLovPsycologys.map(psyco =>
                                            <option key={psyco.key} value={psyco.key}>{psyco.values.psychologicals_psyco_name}</option>
                                        )}
                                    </Input>
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 3, offset: 1 }} className="text-right">Description</Label>
                                <Col sm={{ size: 5 }}>
                                    <Input
                                        className="rounded-0"
                                        type="textarea"
                                        name="description"
                                        value={this.state.description}
                                        onChange={this.handleChange}
                                        required />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={{ size: 3, offset: 1 }} className="text-right">Date</Label>
                                <Col sm={{ size: 5 }}>
                                    <Input
                                        className="rounded-0"
                                        type="date"
                                        name="date"
                                        max={moment().format('YYYY-MM-DD')}
                                        value={this.state.date}
                                        onChange={this.handleChange}
                                        required />
                                </Col>
                            </FormGroup>
                            <Row>
                                <Col sm={{ size: 3, offset: 3 }}>
                                    {
                                        !this.state.isChange ? <Button className="rounded-0" block outline type="submit" color="primary" disabled>Update</Button> :
                                            <Button className="rounded-0" block outline type="submit" color="primary">Update</Button>
                                    }
                                </Col>
                                <Col sm={{ size: 3 }}>
                                    <Link to="/sdm/psycology" className="btn btn-outline-danger btn-block rounded-0" role="button">
                                        Back
                                </Link>
                                </Col>
                            </Row>
                        </Form>
                    </CardBody>
                </Card>
            </div>
        );
    }
}

export default UpdatePsycology;