import React, { Component } from 'react';
import { Redirect, Link } from 'react-router-dom';
import moment from 'moment';
import Autosuggest from 'react-autosuggest';
import AutosuggestHighlightMatch from 'autosuggest-highlight/match';
import AutosuggestHighlightParse from 'autosuggest-highlight/parse';
import { connect } from 'react-redux';
import { getDatas, getDatasFinal } from '../../services/APIFunction';
import {
    Row, Col,
    Card, CardBody, CardFooter,
    Table, FormGroup, Label, Input, Button,
    Pagination, PaginationItem, PaginationLink
} from 'reactstrap';

let skip = 0, top = 10, filter = 'sdm_status="1"', orderby = 'sdm_status DESC,sdm_endcontract ASC', total
class List extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            level: '',
            name: '',
            endDate: '',
            value: '',
            sdmKey: '',
            suggestions: [],
            dataSDM: [],
            dataSDMActive: [],
            dataLevelNames: [],
            dataSDMNames: [],
            totalItems: '',
        });
        this.handleClick = this.handleClick.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    componentDidMount() {
        skip = 0
        filter = 'sdm_status="1"'
        this.getDataSDM(filter, orderby, skip, top)
        this.getDataLov()
        this.getDataSDMName()
    }
    getDataSDM(filter, orderby, skip, top) {
        getDatasFinal('http://209.97.160.40:8080/SA061118/sdm/MengelolaSdm/readAll', skip, top, filter, orderby)
            .then((result) => {
                if (result.data !== null) {
                    this.setState({
                        dataSDM: result.data.items,
                        totalItems: result.data.totalItems
                    })
                } else {
                    this.setState({
                        dataSDM: [],
                        totalItems: '0'
                    })
                }
            });
    }
    getDataLov() {
        getDatas('http://209.97.160.40:8080/SA061118/lov/sdmLvl')
            .then((result) => {
                this.setState({
                    dataLevelNames: result.data
                })
            });
    }
    getDataSDMName() {
        getDatas('http://209.97.160.40:8080/SA061118/lov/sdm?$filter=sdm_status=1&$orderby=sdm_name%20ASC')
            .then((result) => {
                this.setState({
                    dataSDMNames: result.data
                })
            });
    }
    handleChange(e) {
        if (e.target.name === 'level' && e.target.value !== 'prompt') {
            this.setState({
                level: e.target.value,
            })
        }
        else if (e.target.name === 'name') {
            this.getDataSDMName()
            this.setState({
                name: e.target.value,
            })
        } else if (e.target.name === 'endDate') {
            this.setState({
                endDate: e.target.value
            })
        }
    }
    handleClick(e) {
        e.preventDefault();
        if (e.target.name === 'clear') { // do clear click button
            this.setState({
                level: '',
                name: '',
                endDate: '',
                value: '',
                sdmKey: ''
            })
            skip = 0
            filter = 'sdm_status="1"'
            this.getDataSDM(filter, orderby, skip, top)
        } if (e.target.name === 'pagination') { // do when click pagination 
            skip = e.target.value
            this.getDataSDM(filter, orderby, skip, top)
        } if (e.target.name === 'previous') {
            skip = 0
            this.getDataSDM(filter, orderby, skip, top)
        } if (e.target.name === 'next') {
            skip = parseInt(total) - 1
            this.getDataSDM(filter, orderby, skip, top)
        } if (e.target.name === 'filter') { // do filter click button
            if (this.state.level !== '') { filter = 'sdm_status="1" AND (sdmlvl_id=' + this.state.level + ')' }
            if (this.state.sdmKey !== '') { filter = 'sdm_status="1" AND (sdm_id=' + this.state.sdmKey + ')' }
            if (this.state.endDate !== '') { filter = 'sdm_status="1" AND (sdm_endcontract<="' + this.state.endDate + '")' }
            if (this.state.level !== '' && this.state.sdmKey !== '') {
                filter = 'sdm_status="1" AND (sdmlvl_id=' + this.state.level + ' AND sdm_id=' + this.state.sdmKey + ')'
            }
            if (this.state.level !== '' && this.state.endDate !== '') {
                filter = 'sdm_status="1" AND (sdmlvl_id=' + this.state.level + ' AND sdm_endcontract<="' + this.state.endDate + '")'
            }
            if (this.state.sdmKey !== '' && this.state.endDate !== '') {
                filter = 'sdm_status="1" AND (sdm_id=' + this.state.sdmKey + ' AND sdm_endcontract<="' + this.state.endDate + '")'
            }
            if (this.state.level !== '' && this.state.sdmKey !== '' && this.state.endDate !== '') {
                filter = 'sdm_status="1" AND (sdmlvl_id=' + this.state.level + ' AND sdm_id=' + this.state.sdmKey + ' AND sdm_endcontract<="' + this.state.endDate + '")'
            }
            this.getDataSDM(filter, orderby, skip, top)
        }
    }
    createPagination = () => {
        let element = []
        total = Math.ceil(this.state.totalItems / 10)
        for (let index = 0; index < total; index++) {
            element.push(
                <PaginationItem key={index} className={parseInt(skip) === index ? 'active' : ''}>
                    <PaginationLink value={index} onClick={this.handleClick} name="pagination">
                        {index + 1}
                    </PaginationLink>
                </PaginationItem>
            )
        }
        if (total > 5) {
            if (skip < 3) {
                return element.slice(0, 5)
            } else {
                var startMove = parseInt(skip) - 2
                var endMove = parseInt(skip) + 3
                if (startMove > (total - 5) && endMove > total) {
                    startMove = total - 5
                    endMove = total
                }
                return element.slice(startMove, endMove)
            }
        } else {
            return element.slice(0, 5)
        }
    }
    getSuggestions = value => {
        const inputValue = value.trim().toLowerCase();
        const inputLength = inputValue.length;

        return inputLength === 0 ? [] : this.state.dataSDMNames.filter(sdm =>
            sdm.values.sdm_sdm_name.toLowerCase().slice(0, inputLength) === inputValue
        );
    };
    getSuggestionValue = suggestion => suggestion.values.sdm_sdm_name;
    onChange = (event, { newValue }) => {
        this.setState({
            value: newValue,
        });
        var result = this.state.dataSDMNames.find(sdm => sdm.values.sdm_sdm_name === newValue)
        if (result) {
            this.setState({ sdmKey: Object.values(result)[1] })
        }
    };
    onSuggestionsFetchRequested = ({ value }) => {
        this.setState({
            suggestions: this.getSuggestions(value)
        });
    };
    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };
    renderSuggestion = (suggestion, { query }) => {
        const matches = AutosuggestHighlightMatch(suggestion.values.sdm_sdm_name, query)
        const parts = AutosuggestHighlightParse(suggestion.values.sdm_sdm_name, matches)
        return (
            <span>
                {parts.map((part, index) => {
                    const className = part.highlight ? 'react-autosuggest__suggestion-match' : null;
                    return (
                        <span className={className} key={index}>
                            {part.text}
                        </span>
                    );
                })}
            </span>
        );
    }
    render() {
        const { value, suggestions } = this.state;
        const inputProps = {
            placeholder: 'Enter SDM Name',
            className: 'form-control rounded-0',
            value,
            onChange: this.onChange
        };
        const user = this.props.user;
        if (!user.user_id) {
            return (<Redirect to="/" />);
        }
        return (
            <div>
                <Row>
                    <Col sm={{ size: 12 }}>
                        <Card className="rounded-0 shadow-sm animated fadeIn">
                            <CardBody className="px-5 py-5">
                                <Row>
                                    <Col sm={{ size: 4 }}>
                                        <FormGroup>
                                            <Label>Level : </Label>
                                            <Input
                                                type="select"
                                                name="level"
                                                className="rounded-0"
                                                value={this.state.level}
                                                onChange={this.handleChange}>
                                                <option value="prompt">Please Choose...</option>
                                                {this.state.dataLevelNames.map(levelNames =>
                                                    <option key={levelNames.key} value={levelNames.key}>{levelNames.values.sdmlvl_sdmlvl_name}</option>
                                                )
                                                }
                                            </Input>
                                        </FormGroup>
                                    </Col>
                                    <Col sm={{ size: 4 }}>
                                        <FormGroup>
                                            <Label>Name : </Label>
                                            <Autosuggest
                                                suggestions={suggestions}
                                                onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                                                onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                                                getSuggestionValue={this.getSuggestionValue}
                                                renderSuggestion={this.renderSuggestion}
                                                inputProps={inputProps}
                                            />
                                        </FormGroup>
                                    </Col>
                                    <Col sm={{ size: 4 }}>
                                        <FormGroup>
                                            <Label>End Date : </Label>
                                            <Input className="rounded-0" type="date" onChange={this.handleChange} value={this.state.endDate} name="endDate" />
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={{ size: 2 }}>
                                        <Button className="rounded-0" outline color="primary" block onClick={this.handleClick} name="filter">
                                            <i className="fa fa-fw fa-search" />Filter
                                        </Button>
                                    </Col>
                                    <Col sm={{ size: 2 }}>
                                        <Button className="rounded-0" outline color="danger" block onClick={this.handleClick} name="clear">
                                            <i className="fa fa-fw fa-close" />Clear
                                        </Button>
                                    </Col>
                                </Row>
                            </CardBody>
                            <CardFooter>
                                <Link to={`/sdm/list/add`}>
                                    <Button outline color="primary" className="float-right rounded-0">
                                        <i className="fa fa-fw fa-plus" />Input SDM
                                    </Button>
                                </Link>
                            </CardFooter>
                        </Card>
                    </Col>
                </Row>
                <Row>
                    <Col sm={{ size: 12 }}>
                        <Card className="rounded-0 shadow-sm animated fadeIn">
                            <CardBody>
                                <div className="clearfix px-3">
                                    <div className="float-right caption-color">
                                        <p>Total : {this.state.totalItems}</p>
                                    </div>
                                </div>
                                <Table responsive hover bordered className="text-center">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>NIK</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Status</th>
                                            <th>Ket</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.dataSDM.length < 1 ? <tr><td colSpan="8">0 data display</td></tr> :
                                            this.state.dataSDM.map(sdm =>
                                                <tr key={sdm.sdm_id}>
                                                    <th>{sdm.norut}</th>
                                                    <td>{sdm.sdm_name}</td>
                                                    <td>{sdm.sdm_nik}</td>
                                                    <td>{sdm.sdm_startcontract}</td>
                                                    <td>{sdm.sdm_endcontract}</td>
                                                    <td>{sdm.sdm_status}</td>
                                                    <td>{moment(sdm.sdm_endcontract).fromNow(true)} Left</td>
                                                    <td>
                                                        <Button className="rounded-0 mr-2" outline color="primary" id="print-cv">
                                                            <i className="fa fa-print"></i>
                                                        </Button>
                                                        <Link to={`/sdm/list/update/${sdm.sdm_id}`}>
                                                            <Button className="rounded-0 mr-2" outline color="primary" id="edit-sdm">
                                                                <i className="fa fa-edit"></i>
                                                            </Button>
                                                        </Link>
                                                        <Link to={`/sdm/list/view/${sdm.sdm_id}`}>
                                                            <Button className="rounded-0" outline color="primary" id="view-sdm">
                                                                <i className="fa fa-eye"></i>
                                                            </Button>
                                                        </Link>
                                                    </td>
                                                </tr>
                                            )}
                                    </tbody>
                                </Table>
                                <div className="clearfix">
                                    {Math.ceil(this.state.totalItems / 10) > 1 ?
                                        <div className="float-right">
                                            <Pagination aria-label="Page navigation example" className="rounded-0">
                                                <PaginationItem className={parseInt(skip) === 0 ? "disabled ds-pagination" : ''} >
                                                    <PaginationLink previous onClick={this.handleClick} name="previous" />
                                                </PaginationItem>
                                                {this.createPagination()}
                                                <PaginationItem className={parseInt(skip) === (total - 1) ? "disabled ds-pagination" : ''} >
                                                    <PaginationLink next onClick={this.handleClick} name="next" />
                                                </PaginationItem>
                                            </Pagination>
                                        </div>
                                        : null}
                                </div>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        );
    }
}
List = connect(
    state => ({
        user: state.user
    }),
)(List);
export default List;