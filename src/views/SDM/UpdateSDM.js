import React, { Component } from 'react';
import classnames from 'classnames';
import {
    Card, CardBody,
    Nav, NavItem, NavLink,
    TabContent, TabPane,
    Alert
} from 'reactstrap';
import Pribadi from './LayoutSDM/Pribadi';
import Pendidikan from './LayoutSDM/Pendidikan';
import Kursus from './LayoutSDM/Kursus';
import Pekerjaan from './LayoutSDM/Pekerjaan';
import Profil from './LayoutSDM/Profil';
import Bahasa from './LayoutSDM/Bahasa';
class UpdateSDM extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            activeTab: '1',
            sdmId: null,
            message: '',
            isVisible: false
        })
        this.toggle = this.toggle.bind(this)
        this.onDismiss = this.onDismiss.bind(this)
    }
    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({ activeTab: tab })
        }
    }
    onDismiss() {
        this.setState({ isVisible: false })
    }
    componentWillMount() {
        localStorage.setItem("sdmId", this.props.match.params.id)
        this.setState({ sdmId: this.props.match.params.id })
    }
    getMessage(newMessage) {
        this.setState({
            message: newMessage,
            isVisible: true
        })
        this.setTimeoutAlert()
    }
    setTimeoutAlert() {
        if (this.state.isVisible) {
            setTimeout(() => {
                this.setState({ isVisible: false })
            }, 3000);
        }
    }
    render() {
        return (
            <div>
                <Alert color="success" className="shadow-sm" isOpen={this.state.isVisible} toggle={this.onDismiss}>
                    {this.state.message}
                </Alert>
                <Card className="rounded-0 shadow-sm animated fadeIn">
                    <CardBody>
                        <Nav tabs>
                            <NavItem>
                                <NavLink
                                    className={classnames({ active: this.state.activeTab === '1' })}
                                    onClick={() => { this.toggle('1'); }}>
                                    Data Pribadi
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    className={classnames({ active: this.state.activeTab === '2' })}
                                    onClick={() => { this.toggle('2'); }}>
                                    Pendidikan
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    className={classnames({ active: this.state.activeTab === '3' })}
                                    onClick={() => { this.toggle('3'); }}>
                                    Kursus
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    className={classnames({ active: this.state.activeTab === '4' })}
                                    onClick={() => { this.toggle('4'); }}>
                                    Pekerjaan
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    className={classnames({ active: this.state.activeTab === '5' })}
                                    onClick={() => { this.toggle('5'); }}>
                                    Profil
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    className={classnames({ active: this.state.activeTab === '6' })}
                                    onClick={() => { this.toggle('6'); }}>
                                    Bahasa
                                </NavLink>
                            </NavItem>
                        </Nav>
                        <TabContent activeTab={this.state.activeTab}>
                            <TabPane tabId="1">
                                <Pribadi message={this.getMessage.bind(this)} />
                            </TabPane>
                            <TabPane tabId="2">
                                <Pendidikan message={this.getMessage.bind(this)} />
                            </TabPane>
                            <TabPane tabId="3">
                                <Kursus message={this.getMessage.bind(this)} />
                            </TabPane>
                            <TabPane tabId="4">
                                <Pekerjaan message={this.getMessage.bind(this)} />
                            </TabPane>
                            <TabPane tabId="5">
                                <Profil message={this.getMessage.bind(this)} />
                            </TabPane>
                            <TabPane tabId="6">
                                <Bahasa message={this.getMessage.bind(this)} />
                            </TabPane>
                        </TabContent>
                    </CardBody>
                </Card>
            </div>
        );
    }
}

export default UpdateSDM;