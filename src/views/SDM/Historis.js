import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { getDatasFinal } from '../../services/APIFunction';
import {
    Row, Col,
    Card, CardBody,
    Table,
    Input, InputGroup, InputGroupAddon, InputGroupText, Label, Button, FormGroup,
    Pagination, PaginationItem, PaginationLink
} from 'reactstrap';
let skip = 0, total, startSlice = 0, endSlice = 10, start = 0
class Historis extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            dataHistorisAll: [],
            dataHistoris: [],
            totalItems: '',
            search: ''
        })
        this.handleKeyDown = this.handleKeyDown.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }
    componentDidMount() {
        skip = 0
        this.getDataHistoris(skip)
    }
    getDataHistoris(skip) {
        getDatasFinal('http://209.97.160.40:8080/SA061118/sdm/MengelolaHistoriSdm/ReadAll/readAll', skip, 10)
            .then((result) => {
                this.setState({
                    dataHistoris: result.data.items,
                    totalItems: result.data.totalItems,
                })
            });
        getDatasFinal('http://209.97.160.40:8080/SA061118/sdm/MengelolaHistoriSdm/ReadAll/readAll')
            .then((result) => {
                this.setState({
                    dataHistorisAll: result.data.items,
                })
            });
    }
    handleChange(e) {
        this.setState({
            search: e.target.value
        })
    }
    handleKeyDown(e) { //handle when user press enter
        if (e.keyCode === 13) {
            this.setState({
                search: e.target.value
            })
            skip = 0
            startSlice = 0
            endSlice = 10
            this.searchName(e.target.value)
        }
    }
    searchName(value) {
        let word = new RegExp(value, 'gi')
        let found = this.state.dataHistorisAll.filter(histori => histori.sdm_name.match(word))
        this.setState({
            dataHistoris: found.slice(startSlice, endSlice),
            totalItems: found.length
        })
    }
    handleClick(e) {
        if (e.target.name === 'pagination') { // do when click pagination 
            skip = e.target.value
            startSlice = skip + start
            endSlice = parseInt(startSlice) + 10
            if (this.state.search !== '') {
                this.searchName(this.state.search)
            } else {
                this.getDataHistoris(skip)
            }
        }
        if (e.target.name === 'reset') {
            skip = 0
            this.getDataHistoris(skip)
            this.setState({
                search: ''
            })
        }
        if (e.target.name === 'previous') {
            skip = 0
            this.getDataHistoris(skip)
        }
        if (e.target.name === 'next') {
            skip = parseInt(total) - 1
            this.getDataHistoris(skip)
        }
    }
    createPagination = () => {
        let element = []
        total = Math.ceil(this.state.totalItems / 10)
        for (let index = 0; index < total; index++) {
            element.push(
                <PaginationItem key={index} className={parseInt(skip) === index ? 'active' : ''}>
                    <PaginationLink value={index} onClick={this.handleClick} name="pagination">
                        {index + 1}
                    </PaginationLink>
                </PaginationItem>
            )
        }
        if (total > 5) {
            if (skip < 3) {
                return element.slice(0, 5)
            } else {
                var startMove = parseInt(skip) - 2
                var endMove = parseInt(skip) + 3
                if (startMove > (total - 5) && endMove > total) {
                    startMove = total - 5
                    endMove = total
                }
                return element.slice(startMove, endMove)
            }
        } else {
            return element.slice(0, 5)
        }
    }
    render() {
        return (
            <div>
                <Row>
                    <Col sm={{ size: 12 }}>
                        <Card className="rounded-0 shadow-sm animated fadeIn">
                            <CardBody>
                                <Row>
                                    <Label md={{ size: 2 }} className="text-right">Search by</Label>
                                    <Col sm={{ size: 3 }}>
                                        <FormGroup>
                                            <Input type="select" className="rounded-0">
                                                <option>SDM Name</option>
                                            </Input>
                                        </FormGroup>
                                    </Col>
                                    <Col sm={{ size: 4 }}>
                                        <FormGroup>
                                            <InputGroup>
                                                <Input
                                                    type="text"
                                                    className="rounded-0"
                                                    onChange={this.handleChange}
                                                    onKeyDown={this.handleKeyDown}
                                                    value={this.state.search}
                                                    placeholder="Enter Keyword" />
                                                <InputGroupAddon addonType="append">
                                                    <InputGroupText style={{ backgroundColor: '#FFFFFF' }}>
                                                        <i className="fa fa-search"></i>
                                                    </InputGroupText>
                                                </InputGroupAddon>
                                            </InputGroup>
                                        </FormGroup>
                                    </Col>
                                    <Col sm={{ size: 2 }}>
                                        <Button className="rounded-0" type="reset" name="reset" outline color="danger" block onClick={this.handleClick}>
                                            <i className="fa fa-fw fa-close" />
                                            Reset
                                        </Button>
                                    </Col>
                                </Row>
                                <div className="clearfix px-3">
                                    <div className="float-right caption-color">
                                        <p>Total : {this.state.totalItems}</p>
                                    </div>
                                </div>
                                <Table responsive bordered hover className="text-center">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>NIK</th>
                                            <th>Address</th>
                                            <th>Phone Number</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.dataHistoris.length < 1 ? <tr><td colSpan="8">0 data display</td></tr> :
                                            this.state.dataHistoris.map(sdm =>
                                                <tr key={sdm.sdmhistory_id}>
                                                    <th>{sdm.norut}</th>
                                                    <td>{sdm.sdm_name}</td>
                                                    <td>{sdm.sdm_nik}</td>
                                                    <td>{sdm.sdm_address}</td>
                                                    <td>{sdm.sdm_phone}</td>
                                                    <td>{sdm.sdm_startcontract}</td>
                                                    <td>{sdm.sdm_endcontract}</td>
                                                    <td>
                                                        <Link to={`/sdm/historis/view/${sdm.sdm_id}`}>
                                                            <Button className="rounded-0" outline color="primary" id="view-sdm" onClick={this.handleClick}>
                                                                <i className="fa fa-eye"></i>
                                                            </Button>
                                                        </Link>
                                                    </td>
                                                </tr>
                                            )}
                                    </tbody>
                                </Table>
                                <div className="clearfix">
                                    {Math.ceil(this.state.totalItems / 10) > 1 ?
                                        <div className="float-right">
                                            <Pagination aria-label="Page navigation example" className="rounded-0">
                                                <PaginationItem className={parseInt(skip) === 0 ? "disabled ds-pagination" : ''} >
                                                    <PaginationLink previous onClick={this.handleClick} name="previous" />
                                                </PaginationItem>
                                                {this.createPagination()}
                                                <PaginationItem className={parseInt(skip) === (total - 1) ? "disabled ds-pagination" : ''} >
                                                    <PaginationLink next onClick={this.handleClick} name="next" />
                                                </PaginationItem>
                                            </Pagination>
                                        </div>
                                        : null}
                                </div>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default Historis;