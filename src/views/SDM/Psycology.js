import React, { Component } from 'react';
import { getDatas, getDatasFinal } from '../../services/APIFunction';
import { Link } from 'react-router-dom';
import {
    Row, Col,
    Card, CardBody, Table,
    FormGroup, Input, Label, Button, InputGroup, InputGroupAddon, InputGroupText,
    Pagination, PaginationItem, PaginationLink,
    Alert, Badge
} from 'reactstrap';
let skip = 0, start = 0, total, startSlice = 0, endSlice = 10, message, orderBy, found
class Psycology extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            client: '',
            search: '',
            dataPsycologys: [],
            dataPsycologysAll: [],
            dataPsycologysFilter: [],
            totalItems: '',
            dataClients: [],
            visible: false,
        })
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.handleKeyDown = this.handleKeyDown.bind(this);
        this.onDismiss = this.onDismiss.bind(this);
    }
    onDismiss() {
        message = null
        this.setState({ visible: false });
    }
    componentDidMount() {
        skip = 0
        startSlice = 0
        endSlice = 10
        this.getDataPsycologys()
        this.getDataClients()
        message = localStorage.getItem("messagePsco")
        if (message !== null) {
            this.setState({ visible: true })
            this.setTimeoutAlert()
            localStorage.removeItem("messagePsco")
        }
    }
    getDataPsycologys() {
        getDatasFinal('http://209.97.160.40:8080/SA061118/sdm/SdmPsycological/readAll')
            .then((result) => {
                this.setState({
                    dataPsycologysAll: result.data.items,
                    dataPsycologys: result.data.items.slice(startSlice, endSlice),
                    totalItems: result.data.items.length
                })
            });
    }
    getDataPsycologyFilter(filter) {
        getDatas('http://209.97.160.40:8080/SA061118/sdm/SdmPsycological/readAll?client_id=' + filter) //gabisa pake getDatasFinal
            .then((result) => {
                this.setState({
                    dataPsycologysFilter: result.data.items,
                    dataPsycologys: result.data.items.slice(startSlice, endSlice),
                    totalItems: result.data.items.length
                })
            });
    }
    getDataClients() {
        orderBy = 'client_name ASC'
        getDatasFinal('http://209.97.160.40:8080/SA061118/lov/clients', '', '', '', orderBy)
            .then((result) => {
                this.setState({
                    dataClients: result.data
                })
            })
    }
    handleChange(e) {
        if (e.target.name === 'client' && e.target.value !== 'prompt') {
            this.getDataPsycologyFilter(e.target.value)
            this.setState({
                client: e.target.value
            })
        } else {
            this.setState({
                search: e.target.value
            })
        }
    }
    handleClick(e) {
        if (e.target.name === 'pagination') { // do when click pagination 
            skip = e.target.value
            startSlice = skip + start
            endSlice = parseInt(startSlice) + 10
            if (this.state.search !== '') {
                this.searchName(this.state.search)
            } else {
                this.setState({
                    dataPsycologys: this.state.dataPsycologysAll.slice(startSlice, endSlice),
                })
            }
        }
        if (e.target.name === 'reset') {
            this.getDataPsycologys()
            this.setState({
                client: '',
                search: ''
            })
            skip = 0
            startSlice = 0
            endSlice = 10
        }
        if (e.target.name === 'previous') {
            skip = 0
            startSlice = 0
            endSlice = 10
            if (this.state.search !== '') {
                this.searchName(this.state.search)
            } else {
                this.setState({
                    dataPsycologys: this.state.dataPsycologysAll.slice(startSlice, endSlice),
                })
            }
        }
        if (e.target.name === 'next') {
            skip = parseInt(total) - 1
            startSlice = skip.toString() + start
            endSlice = parseInt(startSlice) + 10
            if (this.state.search !== '') {
                this.searchName(this.state.search)
            } else {
                this.setState({
                    dataPsycologys: this.state.dataPsycologysAll.slice(startSlice, endSlice),
                })
            }
        }
    }
    handleKeyDown(e) {
        if (e.keyCode === 13) {
            this.setState({
                search: e.target.value
            })
            skip = 0
            startSlice = 0
            endSlice = 10
            this.searchName(e.target.value)
        }
    }
    searchName(value) {
        let word = new RegExp(value, 'gi')
        if (this.state.client !== '') {
            found = this.state.dataPsycologysFilter.filter(client => client.sdm_name.match(word))
        } else {
            found = this.state.dataPsycologysAll.filter(client => client.sdm_name.match(word))
        }
        this.setState({
            dataPsycologys: found.slice(startSlice, endSlice),
            totalItems: found.length
        })
    }
    setTimeoutAlert() {
        setTimeout(() => {
            this.setState({ visible: false })
        }, 3000);
    }
    createPagination = () => {
        let element = []
        total = Math.ceil(this.state.totalItems / 10)
        for (let index = 0; index < total; index++) {
            element.push(
                <PaginationItem key={index} className={parseInt(skip) === index ? 'active' : ''}>
                    <PaginationLink value={index} onClick={this.handleClick} name="pagination">
                        {index + 1}
                    </PaginationLink>
                </PaginationItem>
            )
        }
        if (total > 5) {
            if (skip < 3) {
                return element.slice(0, 5)
            } else {
                var startMove = parseInt(skip) - 2
                var endMove = parseInt(skip) + 3
                if (startMove > (total - 5) && endMove > total) {
                    startMove = total - 5
                    endMove = total
                }
                return element.slice(startMove, endMove)
            }
        } else {
            return element.slice(0, 5)
        }
    }
    render() {
        return (
            <div>
                <Alert color="success" isOpen={this.state.visible} toggle={this.onDismiss} className="animated fadeIn shadow-sm">
                    {message}
                </Alert>
                <Row>
                    <Col sm={{ size: 12 }}>
                        <Card className="rounded-0 shadow-sm animated fadeIn">
                            <CardBody className="px-5 py-5">
                                <FormGroup row>
                                    <Label md={{ size: 2 }} className="text-right">Client</Label>
                                    <Col sm={{ size: 4 }}>
                                        <Input className="rounded-0" type="select" name="client" value={this.state.client} onChange={this.handleChange}>
                                            <option value="prompt">Please Choose...</option>
                                            {this.state.dataClients.map(client =>
                                                <option key={client.key} value={client.key}>{client.values.clients_client_name}</option>
                                            )}
                                        </Input>
                                    </Col>
                                </FormGroup>
                            </CardBody>
                        </Card>
                        <Card className="rounded-0 shadow-sm animated fadeIn">
                            <CardBody>
                                <FormGroup row >
                                    <Label md={{ size: 2 }} className="text-right">Search by</Label>
                                    <Col sm={{ size: 3 }}>
                                        <Input className="rounded-0" type="select">
                                            <option>SDM Name</option>
                                        </Input>
                                    </Col>
                                    <Col sm={{ size: 4 }}>
                                        <InputGroup>
                                            <Input
                                                type="text"
                                                className="rounded-0"
                                                placeholder="Enter Keyword"
                                                onKeyDown={this.handleKeyDown}
                                                onChange={this.handleChange}
                                                value={this.state.search}
                                                name="search" />
                                            <InputGroupAddon addonType="append">
                                                <InputGroupText style={{ backgroundColor: '#FFFFFF' }}>
                                                    <i className="fa fa-search"></i>
                                                </InputGroupText>
                                            </InputGroupAddon>
                                        </InputGroup>
                                    </Col>
                                    <Col sm={{ size: 2 }}>
                                        <Button className="rounded-0" type="reset" outline color="danger" block onClick={this.handleClick} name="reset">
                                            <i className="fa fa-fw fa-close" /> Reset
                                        </Button>
                                    </Col>
                                </FormGroup>
                                <div className="clearfix px-3">
                                    <div className="float-right caption-color">
                                        <p>Total : {this.state.totalItems}</p>
                                    </div>
                                </div>
                                <Table responsive bordered hover className="text-center">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Condition</th>
                                            <th>Reason</th>
                                            <th>Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.dataPsycologys.length < 1 ? <tr><td colSpan="6">0 data display</td></tr> :
                                            this.state.dataPsycologys.map(sdm =>
                                                <tr key={sdm.sdmpsycological_id}>
                                                    <th>{sdm.norut}</th>
                                                    <td>{sdm.sdm_name}</td>
                                                    <td>
                                                        {(sdm.psyco_id === 1) ?
                                                            <h5><Badge color="danger">{sdm.psyco_name}</Badge></h5>
                                                            : (sdm.psyco_id === 2) ?
                                                                <h5><Badge color="primary">{sdm.psyco_name}</Badge></h5>
                                                                : (sdm.psyco_id === 3) ?
                                                                    <h5><Badge color="success">{sdm.psyco_name}</Badge></h5>
                                                                    : '-'}
                                                    </td>
                                                    <td>{sdm.sdmpsycological_desc}</td>
                                                    <td>{sdm.psycological_date}</td>
                                                    <td>
                                                        <Link to={`/sdm/psycology/update/${sdm.sdmpsycological_id}`}>
                                                            <Button className="rounded-0" outline color="primary" name="edit-sdm">
                                                                <i className="fa fa-edit"></i>
                                                            </Button>
                                                        </Link>
                                                    </td>
                                                </tr>
                                            )
                                        }
                                    </tbody>
                                </Table>
                                <div className="clearfix">
                                    {Math.ceil(this.state.totalItems / 10) > 1 ?
                                        <div className="float-right">
                                            <Pagination aria-label="Page navigation example">
                                                <PaginationItem className={parseInt(skip) === 0 ? "disabled ds-pagination" : ''} >
                                                    <PaginationLink previous onClick={this.handleClick} name="previous" />
                                                </PaginationItem>
                                                {this.createPagination()}
                                                <PaginationItem className={parseInt(skip) === (total - 1) ? "disabled ds-pagination" : ''} >
                                                    <PaginationLink next onClick={this.handleClick} name="next" />
                                                </PaginationItem>
                                            </Pagination>
                                        </div>
                                        : null}
                                </div>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default Psycology;