import React, { Component } from 'react';
import { getDatasFinal, postDatas, deleteDatas } from '../../../services/APIFunction';
import {
    Form, Input, Button, Table,
    Row, Col,
    Modal, ModalHeader, ModalBody, ModalFooter
} from 'reactstrap';
let body
class Bahasa extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            sdmId: null,
            sdmIdNew: null,
            dataLanguange: [],
            dataLanguageNames: [],
            languageId: null,
            languageName: '',
            languageIdNew: '',
            screen: '',
            isOpen: false,
            isChange: false
        })
        this.handleChange = this.handleChange.bind(this)
        this.handleClick = this.handleClick.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.toggleModal = this.toggleModal.bind(this)
    }
    toggleModal(e) {
        if (e.target.id === 'yes') {
            this.deleteDataLanguage(this.state.languageId)
        }
        this.setState(prevState => ({ isOpen: !prevState.isOpen }))
    }
    componentDidMount() {
        this.getLovLanguage()
        if (window.location.href.search(/add/) >= 0) {
            localStorage.removeItem("sdmId")
            // localStorage.getItem("sdmIdNew")
        } else {
            this.getDataLanguage(localStorage.getItem("sdmId"))
            this.setState({
                screen: 'update',
                sdmId: localStorage.getItem("sdmId")
            })
        }
    }
    handleChange(e) {
        if (e.target.value === '') {
            this.setState({
                [e.target.name]: '',
                isChange: false
            })
        } else {
            this.setState({
                [e.target.name]: e.target.value,
                isChange: true
            })
        }
    }
    handleClickAction = (e, param) => {
        if (e.target.id === 'edit-lang') {
            this.state.dataLanguange.map(lang =>
                lang.sdmlanguage_id === param ?
                    this.setState({
                        languageId: lang.sdmlanguage_id,
                        languageName: lang.language_name
                    }) : null
            )
        } else {
            this.setState({ languageId: param })
            this.setState(prevState => ({ isOpen: !prevState.isOpen }))
        }
    }
    handleClick() {
        this.clearFields()
    }
    handleSubmit(event) {
        event.preventDefault()
        // if (this.state.screen === 'update') {
        //     if (this.state.languageId === null) {
        //         this.createNewLanguage() //nanti ganti jadi this.createNewLanguage(this.state.sdmId)
        //     }else {
        //         this.updateDataLanguage()
        //     }
        // } else {
        this.createNewLanguage() //nanti ganti jadi this.createNewLanguage(this.state.sdmIdNew)
        // }
    }
    getLovLanguage() {
        getDatasFinal('http://209.97.160.40:8080/SA061118/lov/languages')
            .then(result =>
                this.setState({
                    dataLanguageNames: result.data
                })
            )
    }
    getDataLanguage(id) {
        var paramFilter = 'sdm_id=' + id
        getDatasFinal('http://209.97.160.40:8080/SA061118/allocation/mengelolaSDMLanguages/readAll', '', '', paramFilter)
            .then(result => {
                this.setState({
                    dataLanguange: result.data.items
                })
            })
    }
    createNewLanguage() { //nanti ganti jadi createNewLanguage(id)
        body = JSON.stringify({
            sdm_id: parseInt(this.state.sdmId), //sdm_id: parseInt(id),
            language_id: parseInt(this.state.languageIdNew)
        })
        postDatas('http://209.97.160.40:8080/SA061118/allocation/mengelolaSDMLanguages/create', body)
            .then(result => {
                this.clearFields()
                this.getDataLanguage(this.state.sdmId)
                this.props.message(result.message)
                // window.location = "#/sdm/list"
            })
    }
    // updateDataLanguage() { //initeh result messagenya success tapi datanya ga keubah
    //     body = JSON.stringify({
    //         sdm_id: parseInt(this.state.sdmId),
    //         sdmlanguage_id: parseInt(this.state.languageId),
    //         language_name: this.state.languageName
    //     })
    //     putDatas('http://209.97.160.40:8080/SA061118/allocation/mengelolaSDMLanguages/update?sdmlanguage_id=' + parseInt(this.state.languageId), body)
    //         .then(result => {
    //             this.clearFields()
    //             this.getDataLanguage(this.state.sdmId)
    //             this.props.message(result.message)
    //         })
    // }
    deleteDataLanguage(id) { //delete berhasil tidak ada kendala tinggal munculin notif success ajeu
        deleteDatas('http://209.97.160.40:8080/SA061118/allocation/mengelolaSDMLanguages/delete?sdmlanguage_id=' + parseInt(id))
            .then(result => {
                this.clearFields()
                this.getDataLanguage(this.state.sdmId)
                this.props.message(result.message)
            })
    }
    clearFields() {
        this.setState({
            languageId: null,
            languageIdNew: '',
            isChange: false
        })
    }
    render() {
        return (
            <div>
                <Form onSubmit={this.handleSubmit}>
                    <table style={{ 'fontSize': '16px' }} className="table">
                        <tbody>
                            <tr>
                                <td className="td-title">Bahasa<i className="text-danger">*</i></td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <Input
                                        type="select"
                                        className="rounded-0"
                                        name="languageIdNew"
                                        value={this.state.languageIdNew}
                                        onChange={this.handleChange}
                                        required>
                                        <option value=''>Please Choose...</option>
                                        {this.state.dataLanguageNames.map(lang =>
                                            <option key={lang.key} value={lang.key}>{lang.values.languages_language_name}</option>
                                        )}
                                    </Input>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <Row>
                        <Col sm={{ size: 2, offset: 4 }}>
                            {this.state.isChange ?
                                <Button className="rounded-0" type="submit" color="primary" block outline>Save</Button>
                                :
                                <Button className="rounded-0" color="primary" block outline disabled>Save</Button>
                            }
                        </Col>
                        <Col sm={{ size: 2 }}>
                            <Button className="rounded-0" outline color="danger" block onClick={this.handleClick}>Reset</Button>
                        </Col>
                    </Row>
                </Form> <br />
                <Table responsive hover bordered className="text-center">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Bahasa</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.dataLanguange.length < 1 ? <tr><td colSpan="3">0 data display</td></tr> :
                            this.state.dataLanguange.map(lang =>
                                <tr key={lang.sdmlanguage_id}>
                                    <th>{lang.norut}</th>
                                    <td>{lang.language_name}</td>
                                    <td>
                                        {/* <Button className="rounded-0 mr-2" outline color="primary" id="edit-lang" onClick={(e) => this.handleClickAction(e, lang.sdmlanguage_id)}>
                                            <i className="fa fa-edit"></i>
                                        </Button> */}
                                        <Button className="rounded-0" outline color="danger" id="delete-lang" onClick={(e) => this.handleClickAction(e, lang.sdmlanguage_id)}>
                                            <i className="fa fa-trash"></i>
                                        </Button>
                                    </td>
                                </tr>
                            )
                        }
                    </tbody>
                </Table>
                <Modal isOpen={this.state.isOpen} toggle={this.toggleModal} className={this.props.className} centered>
                    <ModalHeader toggle={this.toggleModal}>
                        <i className="fa fa-warning text-warning"></i> Warning
                    </ModalHeader>
                    <ModalBody>
                        Are you sure want to delete?
                    </ModalBody>
                    <ModalFooter>
                        <Button className="rounded-0" id="yes" color="primary" onClick={this.toggleModal}>Yes</Button>
                        <Button className="rounded-0" color="danger" onClick={this.toggleModal}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default Bahasa;