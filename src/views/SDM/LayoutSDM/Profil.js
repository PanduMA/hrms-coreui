import React, { Component } from 'react';
import { getDatasFinal, postDatas, putDatas, deleteDatas } from '../../../services/APIFunction';
import {
    Form, Input, Button, Table,
    Row, Col,
    Modal, ModalHeader, ModalBody, ModalFooter
} from 'reactstrap';
let body
class Profil extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            sdmId: null,
            sdmIdNew: null,
            dataProfiling: [],
            profileId: null,
            profileName: '',
            screen: '',
            isOpen: false,
            isChange: false
        })
        this.handleChange = this.handleChange.bind(this)
        this.handleClick = this.handleClick.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.toggleModal = this.toggleModal.bind(this)
    }
    toggleModal(e) {
        if (e.target.id === 'yes') {
            this.deleteDataProfile(this.state.profileId)
        }
        this.setState(prevState => ({ isOpen: !prevState.isOpen }));
    }
    componentDidMount() {
        if (window.location.href.search(/add/) >= 0) {
            localStorage.removeItem("sdmId")
            // localStorage.getItem("sdmIdNew")
        } else {
            this.getDataProfile(localStorage.getItem("sdmId"))
            this.setState({
                screen: 'update',
                sdmId: localStorage.getItem("sdmId")
            })
        }
    }
    handleClickAction = (e, param) => {
        if (e.target.id === 'edit-prof') {
            this.state.dataProfiling.map(profile =>
                profile.profiling_id === param ?
                    this.setState({
                        profileId: profile.profiling_id,
                        profileName: profile.profiling_name
                    }) : null
            )
        } else {
            this.setState({ profileId: param })
            this.setState(prevState => ({ isOpen: !prevState.isOpen }))
        }
    }
    handleChange(e) {
        if (e.target.value === '') {
            this.setState({
                [e.target.name]: '',
                isChange: false
            })
        } else {
            this.setState({
                [e.target.name]: e.target.value,
                isChange: true
            })
        }
    }
    handleClick() {
        this.clearFields()
    }
    handleSubmit(event) {
        event.preventDefault()
        if (this.state.screen === 'update') {
            if (this.state.profileId === null) {
                this.createNewProfile()
            } else {
                this.updateDataProfile()
            }
        } else {
            this.createNewProfile()
        }
    }
    getDataProfile(id) {
        var paramFilter = 'sdm_id=' + id
        getDatasFinal('http://209.97.160.40:8080/SA061118/sdm/profiling/readAll', '', '', paramFilter)
            .then((result) => {
                this.setState({
                    dataProfiling: result.data.items
                })
            })
    }
    createNewProfile() {
        body = JSON.stringify({ //tinggal sdm_id sesuaikan 
            sdm_id: parseInt(this.state.sdmId),
            profiling_name: this.state.profileName
        })
        postDatas('http://209.97.160.40:8080/SA061118/sdm/profiling/create', body) // success
            .then(result => {
                this.clearFields()
                // this.props.view('6')
                this.getDataProfile(this.state.sdmId)
                this.props.message(result.message)
            })
    }
    updateDataProfile() {
        body = JSON.stringify({
            sdm_id: parseInt(this.state.sdmId),
            profiling_id: parseInt(this.state.profileId),
            profiling_name: this.state.profileName
        })
        putDatas('http://209.97.160.40:8080/SA061118/sdm/profiling/update?profiling_id=' + parseInt(this.state.profileId), body) //success
            .then(result => {
                this.clearFields()
                this.getDataProfile(this.state.sdmId)
                this.props.message(result.message)
            })
    }
    deleteDataProfile(id) {
        deleteDatas('http://209.97.160.40:8080/SA061118/sdm/profiling/delete?profiling_id=' + parseInt(id)) //success
            .then(result => {
                this.setState({ profileId: null })
                this.getDataProfile(this.state.sdmId)
                this.props.message(result.message)
            })
    }
    clearFields() {
        this.setState({
            profileName: '',
            isChange: false
        })
    }
    render() {
        return (
            <div>
                <Form onSubmit={this.handleSubmit}>
                    <table style={{ 'fontSize': '16px' }} className="table">
                        <tbody>
                            <tr>
                                <td className="td-title">Kelebihan / Kemampuan<i className="text-danger">*</i></td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <Input
                                        type="textarea"
                                        className="rounded-0"
                                        name="profileName"
                                        placeholder="Enter Kelebihan / Kemampuan"
                                        value={this.state.profileName}
                                        onChange={this.handleChange}
                                        required />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <Row>
                        <Col sm={{ size: 2, offset: 4 }}>
                            {this.state.isChange ?
                                <Button className="rounded-0" type="submit" color="primary" block outline>Save</Button>
                                :
                                <Button className="rounded-0" color="primary" block outline disabled>Save</Button>
                            }
                        </Col>
                        <Col sm={{ size: 2 }}>
                            <Button className="rounded-0" outline color="danger" block onClick={this.handleClick}>Reset</Button>
                        </Col>
                    </Row>
                </Form> <br />
                <Table responsive hover bordered className="text-center">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Keterangan</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.dataProfiling.length < 1 ? <tr><td colSpan="3">0 data display</td></tr> :
                            this.state.dataProfiling.map(profile =>
                                <tr key={profile.profiling_id}>
                                    <th>{profile.norut + 1}</th>
                                    <td>{profile.profiling_name}</td>
                                    <td>
                                        <Button className="rounded-0 mr-2" outline color="primary" id="edit-prof" onClick={(e) => this.handleClickAction(e, profile.profiling_id)}>
                                            <i className="fa fa-edit"></i>
                                        </Button>
                                        <Button className="rounded-0" outline color="danger" id="delete-prof" onClick={(e) => this.handleClickAction(e, profile.profiling_id)}>
                                            <i className="fa fa-trash"></i>
                                        </Button>
                                    </td>
                                </tr>
                            )
                        }
                    </tbody>
                </Table>
                <Modal isOpen={this.state.isOpen} toggle={this.toggleModal} className={this.props.className} centered>
                    <ModalHeader toggle={this.toggleModal}>
                        <i className="fa fa-warning text-warning"></i> Warning
                    </ModalHeader>
                    <ModalBody>
                        Are you sure want to delete?
                    </ModalBody>
                    <ModalFooter>
                        <Button className="rounded-0" id="yes" color="primary" onClick={this.toggleModal}>Yes</Button>
                        <Button className="rounded-0" color="danger" onClick={this.toggleModal}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default Profil;