import React, { Component } from 'react';
import { getDatasFinal, postDatas, putDatas, deleteDatas } from '../../../services/APIFunction';
import {
    Form, Input, Button, Table,
    Row, Col, CustomInput,
    Modal, ModalHeader, ModalBody, ModalFooter
} from 'reactstrap';
let body
class Kursus extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            dataCourses: [],
            sdmId: null,
            sdmIdNew: null,
            courseId: null,
            courseName: '',
            provider: '',
            place: '',
            date: '',
            duration: '',
            sertifikat: 'Yes',
            screen: '',
            isOpen: false,
            isChange: false
        })
        this.handleChange = this.handleChange.bind(this)
        this.handleClick = this.handleClick.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.toggleModal = this.toggleModal.bind(this)
    }
    toggleModal(e) {
        if (e.target.id === 'yes') {
            this.deleteDataCourse(this.state.courseId)
        }
        this.setState(prevState => ({ isOpen: !prevState.isOpen }));
    }
    componentDidMount() {
        if (window.location.href.search(/add/) >= 0) {
            localStorage.removeItem("sdmId")
            // localStorage.getItem("sdmIdNew")
        } else {
            this.getDataCourses(localStorage.getItem("sdmId"))
            this.setState({
                screen: 'update',
                sdmId: localStorage.getItem("sdmId")
            })
        }
    }
    handleChange(e) {
        if (e.target.value === '') {
            this.setState({
                [e.target.name]: '',
                isChange: false
            })
        } else {
            this.setState({
                [e.target.name]: e.target.value,
                isChange: true
            })
        }
    }
    handleClick() {
        this.clearFields()
    }
    handleClickAction = (e, param) => {
        if (e.target.id === 'edit-cour') {
            this.state.dataCourses.map(courses =>
                courses.course_id === param ?
                    this.setState({
                        courseId: courses.course_id,
                        courseName: courses.course_title,
                        provider: courses.course_provider,
                        place: courses.course_place,
                        duration: courses.course_duration,
                        date: courses.course_date,
                        sertifikat: courses.course_certificates
                    }) : null
            )
        } else {
            this.setState({ courseId: param })
            this.setState(prevState => ({
                isOpen: !prevState.isOpen
            }));
        }
    }
    handleSubmit(event) {
        event.preventDefault()
        if (this.state.screen === 'update') {
            if (this.state.courseId === null) {
                this.createNewCourse()
            } else {
                this.updateDataCourse()
            }
        } else {
            this.createNewCourse()
        }
    }
    getDataCourses(id) {
        var paramFilter = 'sdm_id=' + id
        getDatasFinal('http://209.97.160.40:8080/SA061118/sdm/course/readAll', '', '', paramFilter)
            .then((result) => {
                this.setState({
                    dataCourses: result.data.items
                })
            })
    }
    createNewCourse() {
        body = JSON.stringify({
            sdm_id: this.state.sdmId,
            course_title: this.state.courseName,
            course_provider: this.state.provider,
            course_place: this.state.place,
            course_date: this.state.date,
            course_duration: this.state.duration,
            course_certificates: this.state.sertifikat
        })
        postDatas('http://209.97.160.40:8080/SA061118/sdm/course/create', body)
            .then(result => {
                // this.props.view('4') //untuk pindah activeTab
                this.clearFields()
                this.getDataCourses(this.state.sdmId)
                this.props.message(result.message)
            })
    }
    updateDataCourse(body) {
        body = JSON.stringify({
            course_id: parseInt(this.state.courseId),
            course_title: this.state.courseName,
            course_provider: this.state.provider,
            course_place: this.state.place,
            course_duration: this.state.duration,
            course_date: this.state.date,
            course_certificates: this.state.sertifikat
        })
        putDatas('http://209.97.160.40:8080/SA061118/sdm/course/update?course_id=' + parseInt(this.state.courseId), body)
            .then(result => {
                this.clearFields()
                this.getDataCourses(this.state.sdmId)
                this.props.message(result.message)
            })
    }
    deleteDataCourse(id) {
        deleteDatas('http://209.97.160.40:8080/SA061118/sdm/course/delete?course_id=' + parseInt(id))
            .then(result => {
                this.setState({ courseId: null })
                this.getDataCourses(this.state.sdmId)
                this.props.message(result.message)
            })
    }
    clearFields() {
        this.setState({
            courseId: null,
            courseName: '',
            provider: '',
            place: '',
            date: '',
            duration: '',
            sertifikat: 'Yes',
            isChange: false
        })
    }
    render() {
        return (
            <div>
                <Form onSubmit={this.handleSubmit}>
                    <table style={{ 'fontSize': '16px' }} className="table">
                        <tbody>
                            <tr>
                                <td className="td-title">Kurus/Pelatihan<i className="text-danger">*</i></td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <Input
                                        type="text"
                                        className="rounded-0"
                                        name="courseName"
                                        placeholder="Enter Kursus / Pelatihan"
                                        value={this.state.courseName}
                                        onChange={this.handleChange}
                                        required />
                                </td>
                            </tr>
                            <tr>
                                <td className="td-title">Penyelenggara<i className="text-danger">*</i></td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <Input
                                        type="text"
                                        className="rounded-0"
                                        name="provider"
                                        placeholder="Enter Penyelenggara"
                                        value={this.state.provider}
                                        onChange={this.handleChange}
                                        required />
                                </td>
                            </tr>
                            <tr>
                                <td className="td-title">Tempat<i className="text-danger">*</i></td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <Input
                                        type="text"
                                        className="rounded-0"
                                        name="place"
                                        placeholder="Enter Tempat"
                                        value={this.state.place}
                                        onChange={this.handleChange}
                                        required
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td className="td-title">Waktu<i className="text-danger">*</i></td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <Input
                                        type="date"
                                        className="rounded-0"
                                        name="date"
                                        placeholder="Enter Waktu"
                                        value={this.state.date}
                                        onChange={this.handleChange}
                                        required />
                                </td>
                            </tr>
                            <tr>
                                <td className="td-title">Durasi<i className="text-danger">*</i></td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <Input
                                        type="text"
                                        className="rounded-0"
                                        name="duration"
                                        placeholder="Enter Durasi"
                                        value={this.state.duration}
                                        onChange={this.handleChange}
                                        required />
                                </td>
                            </tr>
                            <tr>
                                <td className="td-title">Sertifikat<i className="text-danger">*</i></td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <CustomInput
                                        type="radio"
                                        id="yes"
                                        value="Yes"
                                        name="sertifikat"
                                        label="Yes"
                                        onChange={this.handleChange}
                                        checked={this.state.sertifikat === 'Yes'}
                                        inline />
                                    <CustomInput
                                        type="radio"
                                        id="no"
                                        value="No"
                                        name="sertifikat"
                                        label="No"
                                        onChange={this.handleChange}
                                        checked={this.state.sertifikat === 'No'}
                                        inline />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <Row>
                        <Col sm={{ size: 2, offset: 4 }}>
                            {this.state.isChange ?
                                <Button className="rounded-0" type="submit" color="primary" block outline>Save</Button>
                                :
                                <Button className="rounded-0" color="primary" block outline disabled>Save</Button>
                            }
                        </Col>
                        <Col sm={{ size: 2 }}>
                            <Button className="rounded-0" outline color="danger" block onClick={this.handleClick}>Reset</Button>
                        </Col>
                    </Row>
                </Form> <br />
                <Table responsive hover bordered className="text-center">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kursus</th>
                            <th>Penyelenggara</th>
                            <th>Tempat</th>
                            <th>Waktu</th>
                            <th>Durasi</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.dataCourses.length < 1 ? <tr><td colSpan="7">0 data display</td></tr> :
                            this.state.dataCourses.map(courses =>
                                <tr key={courses.course_id}>
                                    <th>{courses.norut + 1}</th>
                                    <td>{courses.course_title}</td>
                                    <td>{courses.course_provider}</td>
                                    <td>{courses.course_place}</td>
                                    <td>{courses.course_date}</td>
                                    <td>{courses.course_duration}</td>
                                    <td>
                                        <Button className="rounded-0 mr-2" outline color="primary" id="edit-cour" onClick={(e) => this.handleClickAction(e, courses.course_id)}>
                                            <i className="fa fa-edit"></i>
                                        </Button>
                                        <Button className="rounded-0" outline color="danger" id="delete-cour" onClick={(e) => this.handleClickAction(e, courses.course_id)}>
                                            <i className="fa fa-trash"></i>
                                        </Button>
                                    </td>
                                </tr>
                            )
                        }
                    </tbody>
                </Table>
                <Modal isOpen={this.state.isOpen} toggle={this.toggleModal} className={this.props.className} centered>
                    <ModalHeader toggle={this.toggleModal}>
                        <i className="fa fa-warning text-warning"></i> Warning
                    </ModalHeader>
                    <ModalBody>
                        Are you sure want to delete?
                    </ModalBody>
                    <ModalFooter>
                        <Button className="rounded-0" id="yes" color="primary" onClick={this.toggleModal}>Yes</Button>
                        <Button className="rounded-0" color="danger" onClick={this.toggleModal}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default Kursus;