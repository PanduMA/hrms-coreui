import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { postDatas, putDatas, getDatasFinal } from '../../../services/APIFunction';
import moment from 'moment';
import {
    Button, Input, Form, CustomInput,
    Row, Col
} from 'reactstrap';
let body
class Pribadi extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            dataAgamaNames: [],
            dataKesehatanNames: [],
            dataLevelNames: [],
            dataStatusNames: [],
            dataPribadiSDM: [],
            sdmId: null,
            fullName: '',
            nik: '',
            noKTP: '',
            gender: '1',
            birthPlace: '',
            birthDate: '',
            address: '',
            postalCode: '',
            phoneNumber: '',
            email: '',
            linkedIn: '',
            religion: '',
            health: '',
            objektif: '',
            level: '',
            status: '',
            placement: '1',
            starDate: '',
            endDate: '',
            position: '',
            fileName: [],
            message: '',
            screen: '',
            isChange: false
        })
        this.handleKey = this.handleKey.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    componentDidMount() {
        this.getDataLov()
        if (window.location.href.search(/add/) >= 0) {
            localStorage.removeItem("sdmId")
        } else {
            this.getDataPribadi(localStorage.getItem("sdmId"))
            this.setState({
                screen: 'update',
                sdmId: localStorage.getItem("sdmId")
            })
        }
    }
    getDataPribadi(id) {
        var paramFilter = 'sdm_id=' + id
        getDatasFinal('http://209.97.160.40:8080/SA061118/sdm/mengelolaSdm/readAll', '', '', paramFilter)
            .then(result => {
                this.setState({
                    dataPribadiSDM: result.data.items,
                    fullName: result.data.items[0].sdm_name,
                    nik: result.data.items[0].sdm_nik,
                    noKTP: result.data.items[0].sdm_ktp,
                    gender: result.data.items[0].gender_id.toString(),
                    birthPlace: result.data.items[0].sdm_placebirth,
                    birthDate: result.data.items[0].sdm_datebirth,
                    address: result.data.items[0].sdm_address,
                    postalCode: result.data.items[0].sdm_postcode,
                    phoneNumber: result.data.items[0].sdm_phone,
                    email: result.data.items[0].sdm_email,
                    linkedIn: result.data.items[0].sdm_linkedin,
                    religion: result.data.items[0].religion_id,
                    health: result.data.items[0].health_id,
                    objektif: result.data.items[0].sdm_objective,
                    level: result.data.items[0].sdmlvl_id,
                    status: result.data.items[0].contracttype_id,
                    placement: result.data.items[0].sdm_contractloc,
                    starDate: result.data.items[0].sdm_startcontract,
                    endDate: result.data.items[0].sdm_endcontract,
                    posisi: result.data.items[0].posisi,
                    image: result.data.items[0].sdm_image
                })
            })
    }
    getDataLov() {
        getDatasFinal('http://209.97.160.40:8080/SA061118/lov/religion') //agama
            .then((result) => {
                this.setState({
                    dataAgamaNames: result.data
                })
            });
        getDatasFinal('http://209.97.160.40:8080/SA061118/lov/health') //kesehatan
            .then((result) => {
                this.setState({
                    dataKesehatanNames: result.data
                })
            });
        getDatasFinal('http://209.97.160.40:8080/SA061118/lov/sdmLvl') //level
            .then((result) => {
                this.setState({
                    dataLevelNames: result.data
                })
            });
        getDatasFinal('http://209.97.160.40:8080/SA061118/lov/contractType') //status
            .then((result) => {
                this.setState({
                    dataStatusNames: result.data
                })
            });
    }
    handleKey(e) {
        if (e.charCode < 48 || e.charCode > 57) {
            e.preventDefault();
        }
    }
    handleChange(e) {
        if (e.target.name === 'starDate') {
            this.setState({ isContractChange: true })
        } else if (e.target.name === 'file') {
            this.setState({ fileName: e.target.files[0].name })
        }
        if (e.target.value === '') {
            this.setState({
                [e.target.name]: '',
                isChange: false
            })
        } else {
            this.setState({
                [e.target.name]: e.target.value,
                isChange: true
            })
        }
    }
    handleSubmit(event) {
        event.preventDefault()
        if (this.state.screen === 'update') {
            body = JSON.stringify({
                Model_ID: 'sdm_id=' + parseInt(this.state.sdmId),
                contracttype_id: parseInt(this.state.status),
                gender_id: parseInt(this.state.gender),
                health_id: parseInt(this.state.health),
                posisi: this.state.posisi,
                religion_id: parseInt(this.state.religion),
                sdm_address: this.state.address,
                sdm_contractloc: this.state.placement,
                sdm_datebirth: this.state.birthDate,
                sdm_email: this.state.email,
                sdm_endcontract: this.state.endDate,
                sdm_id: parseInt(this.state.sdmId),
                sdm_ktp: this.state.noKTP,
                sdm_linkedin: this.state.linkedIn,
                sdm_name: this.state.fullName,
                sdm_nik: this.state.nik,
                sdm_objective: this.state.objektif,
                sdm_phone: this.state.phoneNumber,
                sdm_placebirth: this.state.birthPlace,
                sdm_postcode: this.state.postalCode,
                sdm_startcontract: this.state.starDate,
                sdmlvl_id: parseInt(this.state.level),
            })
            this.updateData(body)
        } else {
            body = JSON.stringify({
                sdm_name: this.state.fullName,
                sdm_nik: this.state.nik,
                sdm_ktp: this.state.noKTP,
                gender_id: parseInt(this.state.gender),
                sdm_placebirth: this.state.birthPlace,
                sdm_datebirth: this.state.birthDate,
                sdm_address: this.state.address,
                sdm_postcode: this.state.postalCode,
                sdm_phone: this.state.phoneNumber,
                sdm_email: this.state.email,
                sdm_linkedin: this.state.linkedIn,
                religion_id: parseInt(this.state.religion),
                health_id: parseInt(this.state.health),
                sdm_objective: this.state.objektif,
                sdmlvl_id: parseInt(this.state.level),
                sdm_status: parseInt(this.state.status),
                sdm_contractloc: parseInt(this.state.placement),
                sdm_startcontract: this.state.starDate,
                sdm_endcontract: this.state.endDate,
                posisi: this.state.position,
                sdm_image: this.state.fileName
            })
            this.createNewSDM(body)
        }
    }
    createNewSDM(body) { //bisa dan sdm_idnya sudah ke ada tapi gimana caranya ngirim ke beberapa componet
        postDatas('http://209.97.160.40:8080/SA061118/sdm/mengelolaSdm/create', body)
            .then(result => {
                // this.setState({
                //     dataSDM: result.data,
                //     message: result.message,
                // this.props.view('2')
                // })
                // localStorage.setItem("sdmIdNew",result.data.sdm_id)
                this.props.view('2',result.data.sdm_id)
                this.props.message(result.message)
                this.props.id(result.data.sdm_id)
                console.log(result.message, result.data.sdm_id)
            })
        console.log('you create new sdm', body)
    }
    updateData(body) { //gatau belum bisa
        // putDatas('http://209.97.160.40:8080/SA061118/sdm/mengelolaSdm/update?sdm_id=' + parseInt(this.state.sdmId), body)
        // .then(result => {
        //     this.setState({
        //         activeTab: '2',
        //     })
        // })
        console.log('you update data sdm', body)
    }
    render() {
        return (
            <div>
                <Form onSubmit={this.handleSubmit}>
                    <table style={{ 'fontSize': '16px' }} className="table">
                        <tbody>
                            <tr>
                                <td className="td-title">Nama Lengkap<i className="text-danger">*</i></td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <Input
                                        type="text"
                                        className="rounded-0"
                                        name="fullName"
                                        placeholder="Enter Nama Lengkap"
                                        value={this.state.fullName}
                                        onChange={this.handleChange}
                                        required />
                                </td>
                            </tr>
                            <tr>
                                <td className="td-title">NIK<i className="text-danger">*</i></td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    {this.state.screen === 'update' ?
                                        <Input
                                            type="text"
                                            className="rounded-0"
                                            name="nik"
                                            value={this.state.nik}
                                            readOnly
                                        />
                                        :
                                        <Input
                                            type="text"
                                            className="rounded-0"
                                            name="nik"
                                            placeholder="Enter NIK"
                                            maxLength="9"
                                            value={this.state.nik}
                                            onChange={this.handleChange}
                                            onKeyPress={this.handleKey}
                                            required
                                        />
                                    }
                                </td>
                            </tr>
                            <tr>
                                <td className="td-title">No. KTP<i className="text-danger">*</i></td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <Input
                                        type="text"
                                        className="rounded-0"
                                        name="noKTP"
                                        placeholder="Enter No KTP"
                                        maxLength="16"
                                        value={this.state.noKTP}
                                        onKeyPress={this.handleKey}
                                        onChange={this.handleChange}
                                        required />
                                </td>
                            </tr>
                            <tr>
                                <td className="td-title">Jenis Kelamin</td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <CustomInput
                                        type="radio"
                                        id="male"
                                        value="1"
                                        name="gender"
                                        label="Laki-laki"
                                        onChange={this.handleChange}
                                        checked={this.state.gender === '1'}
                                        inline />
                                    <CustomInput
                                        type="radio"
                                        id="female"
                                        value="2"
                                        name="gender"
                                        label="Perempuan"
                                        onChange={this.handleChange}
                                        checked={this.state.gender === '2'}
                                        inline />
                                </td>
                            </tr>
                            <tr>
                                <td className="td-title">Tempat Lahir</td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <Input
                                        type="text"
                                        className="rounded-0"
                                        name="birthPlace"
                                        placeholder="Enter Tempat Lahir"
                                        value={this.state.birthPlace}
                                        onChange={this.handleChange}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td className="td-title">Tanggal Lahir<i className="text-danger">*</i></td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <Input
                                        type="date"
                                        className="rounded-0"
                                        name="birthDate"
                                        max={moment().add(-1, 'day').format('YYYY-MM-DD').toString()}
                                        value={this.state.birthDate}
                                        onChange={this.handleChange}
                                        required />
                                </td>
                            </tr>
                            <tr>
                                <td className="td-title">Alamat<i className="text-danger">*</i></td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <Input
                                        type="textarea"
                                        className="rounded-0"
                                        name="address"
                                        placeholder="Enter Alamat"
                                        value={this.state.address}
                                        onChange={this.handleChange}
                                        required />
                                </td>
                            </tr>
                            <tr>
                                <td className="td-title">Kode POS</td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <Input
                                        type="text"
                                        name="postalCode"
                                        placeholder="Enter Kode POS"
                                        className="rounded-0"
                                        maxLength="5"
                                        value={this.state.postalCode}
                                        onChange={this.handleChange}
                                        onKeyPress={this.handleKey}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td className="td-title">No Telepon<i className="text-danger">*</i></td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <Input
                                        type="text"
                                        className="rounded-0"
                                        name="phoneNumber"
                                        placeholder="Enter No Telepon"
                                        maxLength="13"
                                        value={this.state.phoneNumber}
                                        onChange={this.handleChange}
                                        onKeyPress={this.handleKey}
                                        required />
                                </td>
                            </tr>
                            <tr>
                                <td className="td-title">Email<i className="text-danger">*</i></td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <Input
                                        type="email"
                                        className="rounded-0"
                                        name="email"
                                        placeholder="Enter Email"
                                        value={this.state.email}
                                        onChange={this.handleChange}
                                        required />
                                </td>
                            </tr>
                            <tr>
                                <td className="td-title">LinkedIn</td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <Input
                                        type="text"
                                        className="rounded-0"
                                        name="linkedIn"
                                        placeholder="Enter Linked In"
                                        value={this.state.linkedIn}
                                        onChange={this.handleChange}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td className="td-title">Agama<i className="text-danger">*</i></td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <Input
                                        type="select"
                                        className="rounded-0"
                                        name="religion"
                                        value={this.state.religion}
                                        onChange={this.handleChange}
                                        required>
                                        <option value="">Please Choose...</option>
                                        {this.state.dataAgamaNames.map(religion =>
                                            <option key={religion.key} value={religion.key}>{religion.values.religion_religion_name}</option>
                                        )
                                        }
                                    </Input>
                                </td>
                            </tr>
                            <tr>
                                <td className="td-title">Kesehatan<i className="text-danger">*</i></td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <Input
                                        type="select"
                                        className="rounded-0"
                                        name="health"
                                        value={this.state.health}
                                        onChange={this.handleChange}
                                        required>
                                        <option value="">Please Choose...</option>
                                        {this.state.dataKesehatanNames.map(kesehatan =>
                                            <option key={kesehatan.key} value={kesehatan.key}>{kesehatan.values.health_health_status}</option>
                                        )
                                        }
                                    </Input>
                                </td>
                            </tr>
                            <tr>
                                <td className="td-title">Objektif</td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <Input
                                        type="text"
                                        className="rounded-0"
                                        name="objektif"
                                        placeholder="Enter Objektif"
                                        value={this.state.objektif}
                                        onChange={this.handleChange}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td className="td-title">Level<i className="text-danger">*</i></td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <Input
                                        type="select"
                                        className="rounded-0"
                                        name="level"
                                        value={this.state.level}
                                        onChange={this.handleChange}
                                        required>
                                        <option value="">Please Choose...</option>
                                        {this.state.dataLevelNames.map(level =>
                                            <option key={level.key} value={level.key}>{level.values.sdmlvl_sdmlvl_name}</option>
                                        )
                                        }
                                    </Input>
                                </td>
                            </tr>
                            <tr>
                                <td className="td-title">Status<i className="text-danger">*</i></td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <Input
                                        type="select"
                                        className="rounded-0"
                                        name="status"
                                        value={this.state.status}
                                        onChange={this.handleChange}
                                        required>
                                        <option value="">Please Choose...</option>
                                        {this.state.dataStatusNames.map(status =>
                                            <option key={status.key} value={status.key}>{status.values.contracttypes_contracttype_name}</option>
                                        )
                                        }
                                    </Input>
                                </td>
                            </tr>
                            <tr>
                                <td className="td-title">Penempatan<i className="text-danger">*</i></td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <CustomInput
                                        type="radio"
                                        id="bandung"
                                        value="1"
                                        name="placement"
                                        label="Bandung"
                                        onChange={this.handleChange}
                                        checked={this.state.placement === '1'}
                                        inline
                                        required />
                                    <CustomInput
                                        type="radio"
                                        id="luar"
                                        value="2"
                                        name="placement"
                                        label="Luar Bandung"
                                        onChange={this.handleChange}
                                        checked={this.state.placement === '2'}
                                        inline
                                        required />
                                </td>
                            </tr>
                            <tr>
                                <td className="td-title">Awal Kontrak<i className="text-danger">*</i></td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <Input
                                        type="date"
                                        className="rounded-0"
                                        name="starDate"
                                        value={this.state.starDate}
                                        onChange={this.handleChange}
                                        required />
                                </td>
                            </tr>
                            <tr>
                                <td className="td-title">Akhir Kontrak<i className="text-danger">*</i></td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <Input
                                        type="date"
                                        className="rounded-0"
                                        name="endDate"
                                        min={moment(this.state.starDate).add(1, 'days').format("YYYY-MM-DD")}
                                        value={this.state.endDate}
                                        onChange={this.handleChange}
                                        required />
                                </td>
                            </tr>
                            <tr>
                                <td className="td-title">Posisi<i className="text-danger">*</i></td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <CustomInput
                                        type="radio"
                                        id="nonteknis"
                                        value="1"
                                        name="position"
                                        label="Non Teknis"
                                        onChange={this.handleChange}
                                        checked={this.state.position === '1'}
                                        disabled={this.state.isContractChange ? false : true}
                                        inline
                                        required />
                                    <CustomInput
                                        type="radio"
                                        id="teknis"
                                        value="2"
                                        name="position"
                                        label="Teknis (Software Developer)"
                                        onChange={this.handleChange}
                                        checked={this.state.position === '2'}
                                        disabled={this.state.isContractChange ? false : true}
                                        inline
                                        required />
                                </td>
                            </tr>
                            <tr>
                                <td className="td-title">{this.state.screen === 'update' ? 'Gambar' : 'File'}<i className="text-danger">*</i></td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    {this.state.screen === 'update' ?
                                        this.state.image
                                        : <Input
                                            type="file"
                                            className="rounded-0"
                                            name="file"
                                            id="exampleFile"
                                            onChange={this.handleChange}
                                            required />
                                    }
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <Row>
                        <Col sm={{ size: 2, offset: 4 }}>
                            {this.state.isChange ?
                                <Button className="rounded-0" type="submit" color="primary" block outline>Save</Button>
                                :
                                <Button className="rounded-0" color="primary" block outline disabled>Save</Button>
                            }
                        </Col>
                        <Col sm={{ size: 2 }}>
                            <Link to={`/sdm/list`} style={{ textDecoration: 'none' }}>
                                <Button className="rounded-0" outline color="danger" block>Back</Button>
                            </Link>
                        </Col>
                    </Row>
                </Form>
            </div>
        );
    }
}

export default Pribadi;