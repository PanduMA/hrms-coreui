import React, { Component } from 'react';
import { getDatasFinal, postDatas, putDatas, deleteDatas } from '../../../services/APIFunction';
import {
    Form, Input, Button, Table,
    Row, Col,
    Modal, ModalHeader, ModalBody, ModalFooter,
} from 'reactstrap';
let body
class Pendidikan extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            dataDegreeNames: [],
            dataEducations: [],
            educationId: null,
            sdmId: null,
            sdmIdNew: null,
            schoolName: '',
            degree: '',
            subject: '',
            yearIn: '',
            yearOut: '',
            screen: '',
            isOpen: false,
            isChange: false,
        })
        this.handleChange = this.handleChange.bind(this)
        this.handleKey = this.handleKey.bind(this)
        this.handleClick = this.handleClick.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.toggleModal = this.toggleModal.bind(this)
    }
    toggleModal(e) {
        if (e.target.id === 'yes') {
            this.deleteDataEducation(this.state.educationId)
        }
        this.setState(prevState => ({ isOpen: !prevState.isOpen }));
    }
    componentDidMount() {
        console.log(this.props.sdmIdNew)
        this.getDataDegree()
        if (window.location.href.search(/add/) >= 0) {
            localStorage.removeItem("sdmId")
            // console.log(localStorage.getItem("sdmIdNew"))
            this.getDataEducation(this.props.sdmIdNew)
        } else {
            this.getDataEducation(localStorage.getItem("sdmId"))
            this.setState({
                screen: 'update',
                sdmId: localStorage.getItem("sdmId")
            })
        }
    }
    handleChange(e) {
        if (e.target.value === '') {
            this.setState({
                [e.target.name]: e.target.value,
                isChange: false
            })
        } else {
            this.setState({
                [e.target.name]: e.target.value,
                isChange: true
            })
        }
    }
    handleKey(e) {
        if (e.charCode < 48 || e.charCode > 57) {
            e.preventDefault();
        }
    }
    handleClick() {
        this.clearFields()
    }
    handleClickAction = (e, param) => {
        if (e.target.id === 'edit-edu') {
            this.state.dataEducations.map(education =>
                education.edu_id === param ?
                    this.setState({
                        educationId: education.edu_id,
                        schoolName: education.edu_name,
                        degree: education.degree_id,
                        subject: education.edu_subject,
                        yearIn: education.edu_startdate,
                        yearOut: education.edu_enddate
                    }) : null
            )
        } else {
            this.setState({ educationId: param })
            this.setState(prevState => ({
                isOpen: !prevState.isOpen
            }));
        }
    }
    handleSubmit(event) {
        event.preventDefault()
        if (parseInt(this.state.yearOut) < parseInt(this.state.yearIn)) {
            alert("Tahun Keluar Tidak Boleh Lebih Kecil Dari Tahun Masuk!");
            return (false);
        }
        if (this.state.screen === 'update') {
            if (this.state.educationId === null) {
                this.createNewEducation()
            } else {
                this.updateDataEducation()
            }
        } else {
            this.createNewEducation()
        }
    }
    getDataEducation(id) {
        var paramFilter = 'sdm_id=' + id
        var paramOrderBy = 'degree_id ASC'
        getDatasFinal('http://209.97.160.40:8080/SA061118/sdm/Education/readAll', '', '', paramFilter, paramOrderBy)
            .then(result => {
                this.setState({
                    dataEducations: result.data.items,
                })
            })
    }
    getDataDegree() {
        getDatasFinal('http://209.97.160.40:8080/SA061118/lov/degree')
            .then(result => {
                this.setState({
                    dataDegreeNames: result.data
                })
            })
    }
    createNewEducation() { //bisa
        body = JSON.stringify({
            sdm_id: parseInt(this.state.sdmId),
            edu_name: this.state.schoolName,
            degree_id: parseInt(this.state.degree),
            edu_subject: this.state.subject,
            edu_startdate: this.state.yearIn,
            edu_enddate: this.state.yearOut
        })
        postDatas('http://209.97.160.40:8080/SA061118/sdm/Education/create', body) //success
            .then(result => {
                // this.props.view('3') //untuk pindah activeTab
                this.clearFields()
                this.getDataEducation(this.state.sdmId)
                this.props.message(result.message)
            })
    }
    updateDataEducation() {
        body = JSON.stringify({
            sdm_id: parseInt(this.state.sdmId),
            edu_id: parseInt(this.state.educationId),
            edu_name: this.state.schoolName,
            degree_id: parseInt(this.state.degree),
            edu_subject: this.state.subject,
            edu_startdate: this.state.yearIn,
            edu_enddate: this.state.yearOut
        })
        putDatas('http://209.97.160.40:8080/SA061118/sdm/Education/update?edu_id=' + parseInt(this.state.educationId), body)
            .then(result => {
                this.clearFields()
                this.getDataEducation(this.state.sdmId)
                this.props.message(result.message)
            })
    }
    deleteDataEducation(id) {
        deleteDatas('http://209.97.160.40:8080/SA061118/sdm/Education/delete?edu_id=' + parseInt(id))
            .then(result => {
                this.setState({ educationId: null })
                this.getDataEducation(this.state.sdmId)
                this.props.message(result.message)
            })
    }
    clearFields() {
        this.setState({
            educationId: null,
            schoolName: '',
            degree: '',
            subject: '',
            yearIn: '',
            yearOut: '',
            isChange: false
        })
    }
    render() {
        return (
            <div>
                <h5>{this.props.sdmIdNew}</h5>
                <Form onSubmit={this.handleSubmit}>
                    <table style={{ 'fontSize': '16px' }} className="table">
                        <tbody>
                            <tr>
                                <td className="td-title">Nama Sekolah<i className="text-danger">*</i></td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <Input
                                        type="text"
                                        className="rounded-0"
                                        name="schoolName"
                                        placeholder="Enter Nama Sekolah"
                                        value={this.state.schoolName}
                                        onChange={this.handleChange}
                                        required />
                                </td>
                            </tr>
                            <tr>
                                <td className="td-title">Tingkat<i className="text-danger">*</i></td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <Input
                                        type="select"
                                        name="degree"
                                        value={this.state.degree}
                                        onChange={this.handleChange}
                                        required >
                                        <option value="">Please Choose...</option>
                                        {this.state.dataDegreeNames.map(degree =>
                                            <option key={degree.key} value={degree.key}>{degree.values.degree_degree_name}</option>
                                        )
                                        }
                                    </Input>
                                </td>
                            </tr>
                            <tr>
                                <td className="td-title">Jurusan</td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <Input
                                        type="text"
                                        className="rounded-0"
                                        name="subject"
                                        placeholder="Enter Jurusan"
                                        value={this.state.subject}
                                        onChange={this.handleChange}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td className="td-title">Tahun Masuk<i className="text-danger">*</i></td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <Input
                                        type="text"
                                        className="rounded-0"
                                        name="yearIn"
                                        placeholder="Enter Tahun Masuk"
                                        maxLength="4"
                                        value={this.state.yearIn}
                                        onChange={this.handleChange}
                                        onKeyPress={this.handleKey}
                                        required />
                                </td>
                            </tr>
                            <tr>
                                <td className="td-title">Tahun Keluar<i className="text-danger">*</i></td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <Input
                                        type="text"
                                        className="rounded-0"
                                        name="yearOut"
                                        placeholder="Enter Tahun Keluar"
                                        maxLength="4"
                                        value={this.state.yearOut}
                                        onChange={this.handleChange}
                                        onKeyPress={this.handleKey}
                                        required />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <Row>
                        <Col sm={{ size: 2, offset: 4 }}>
                            {this.state.isChange ?
                                <Button className="rounded-0" type="submit" color="primary" block outline>Save</Button>
                                :
                                <Button className="rounded-0" color="primary" block outline disabled>Save</Button>
                            }
                        </Col>
                        <Col sm={{ size: 2 }}>
                            <Button className="rounded-0" outline color="danger" block onClick={this.handleClick}>Reset</Button>
                        </Col>
                    </Row>
                </Form> <br />
                <Table responsive hover bordered className="text-center">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Sekolah</th>
                            <th>Jurusan</th>
                            <th>Tingkat</th>
                            <th>Tahun Masuk</th>
                            <th>Tahun Keluar</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.dataEducations.length < 1 ? <tr><td colSpan="7">0 data display</td></tr> :
                            this.state.dataEducations.map(education =>
                                <tr key={education.edu_id}>
                                    <th>{education.norut + 1}</th>
                                    <td>{education.edu_name}</td>
                                    <td>{education.edu_subject}</td>
                                    <td>{education.degree_name}</td>
                                    <td>{education.edu_startdate}</td>
                                    <td>{education.edu_enddate}</td>
                                    <td>
                                        <Button className="rounded-0 mr-2" outline color="primary" id="edit-edu" onClick={(e) => this.handleClickAction(e, education.edu_id)}>
                                            <i className="fa fa-edit"></i>
                                        </Button>
                                        <Button className="rounded-0" outline color="danger" id="delete-edu" onClick={(e) => this.handleClickAction(e, education.edu_id)}>
                                            <i className="fa fa-trash"></i>
                                        </Button>
                                    </td>
                                </tr>
                            )
                        }
                    </tbody>
                </Table>
                <Modal isOpen={this.state.isOpen} toggle={this.toggleModal} className={this.props.className} centered>
                    <ModalHeader toggle={this.toggleModal}>
                        <i className="fa fa-warning text-warning"></i> Warning
                    </ModalHeader>
                    <ModalBody>
                        Are you sure want to delete?
                    </ModalBody>
                    <ModalFooter>
                        <Button className="rounded-0" id="yes" color="primary" onClick={this.toggleModal}>Yes</Button>
                        <Button className="rounded-0" color="danger" onClick={this.toggleModal}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default Pendidikan;