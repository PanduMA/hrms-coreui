import React, { Component } from 'react';
import moment from 'moment';
import { getDatasFinal, postDatas, putDatas, deleteDatas } from '../../../services/APIFunction';
import {
    Form, Input, Button, Table,
    Row, Col,
    Modal, ModalHeader, ModalBody, ModalFooter
} from 'reactstrap';
let body
class Pekerjaan extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            sdmId: null,
            sdmIdNew: null,
            dataEmployment: [],
            employeId: null,
            corpName: '',
            corpStart: '',
            corpEnd: '',
            role: '',
            screen: '',
            isOpen: false,
            isChange: false
        })
        this.handleChange = this.handleChange.bind(this)
        this.handleClick = this.handleClick.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.toggleModal = this.toggleModal.bind(this)
    }
    toggleModal(e) {
        if (e.target.id === 'yes') {
            this.deleteDataEmployment(this.state.employeId)
        }
        this.setState(prevState => ({ isOpen: !prevState.isOpen }));
    }
    componentDidMount() {
        if (window.location.href.search(/add/) >= 0) {
            localStorage.removeItem("sdmId")
            // localStorage.getItem("sdmIdNew")
        } else {
            this.getDataEmployment(localStorage.getItem("sdmId"))
            this.setState({
                screen: 'update',
                sdmId: localStorage.getItem("sdmId")
            })
        }
    }
    handleClickAction = (e, param) => {
        if (e.target.id === 'edit-emp') {
            this.state.dataEmployment.map(employe =>
                employe.employment_id === param ?
                    this.setState({
                        employeId: employe.employment_id,
                        corpName: employe.employment_corpname,
                        corpStart: employe.employment_startdate,
                        corpEnd: employe.employment_enddate,
                        role: employe.employment_rolejob,
                    }) : null
            )
        } else {
            this.setState({ employeId: param })
            this.setState(prevState => ({
                isOpen: !prevState.isOpen
            }));
        }
    }
    handleChange(e) {
        if (e.target.value === '') {
            this.setState({
                [e.target.name]: '',
                isChange: false
            })
        } else {
            this.setState({
                [e.target.name]: e.target.value,
                isChange: true,
            })
        }
    }
    handleClick() {
        this.clearFields()
    }
    handleSubmit(event) {
        event.preventDefault()
        if (this.state.screen === 'update') {
            if (this.state.employeId === null) {
                this.createNewEmployment()
            } else {
                this.updateDataEmployment()
            }
        } else {
            this.createNewEmployment()
        }
    }
    getDataEmployment(id) {
        var paramFilter = 'sdm_id=' + id
        getDatasFinal('http://209.97.160.40:8080/SA061118/sdm/employment/readAll', '', '', paramFilter)
            .then((result) => {
                this.setState({
                    dataEmployment: result.data.items
                })
            })
    }
    createNewEmployment() {
        body = JSON.stringify({ //tinggal sdm_id
            sdm_id: parseInt(this.state.sdmId),
            employment_corpname: this.state.corpName,
            employment_startdate: this.state.corpStart,
            employment_enddate: this.state.corpEnd,
            employment_rolejob: this.state.role
        })
        postDatas('http://209.97.160.40:8080/SA061118/sdm/employment/create', body) //success
            .then(result => {
                // this.props.view('5') //untuk pindah activeTab
                this.clearFields()
                this.getDataEmployment(this.state.sdmId)
                this.props.message(result.message)
            })
    }
    updateDataEmployment(body) {
        body = JSON.stringify({
            sdm_id: parseInt(this.state.sdmId),
            employment_id: parseInt(this.state.employeId),
            employment_corpname: this.state.corpName,
            employment_startdate: this.state.corpStart,
            employment_enddate: this.state.corpEnd,
            employment_rolejob: this.state.role
        })
        putDatas('http://209.97.160.40:8080/SA061118/sdm/employment/update?employment_id=' + parseInt(this.state.employeId), body) //success
            .then(result => {
                this.getDataEmployment(this.state.sdmId)
                this.clearFields()
                this.props.message(result.message)
            })
    }
    deleteDataEmployment(id) {
        deleteDatas('http://209.97.160.40:8080/SA061118/sdm/employment/delete?employment_id=' + parseInt(id)) //success
            .then(result => {
                this.setState({ employeId: null })
                this.getDataEmployment(this.state.sdmId)
                this.props.message(result.message)
            })
    }
    clearFields() {
        this.setState({
            employeId: null,
            corpName: '',
            corpStart: '',
            corpEnd: '',
            role: '',
            isChange: false
        })
    }
    render() {
        return (
            <div>
                <Form onSubmit={this.handleSubmit}>
                    <table style={{ 'fontSize': '16px' }} className="table">
                        <tbody>
                            <tr>
                                <td className="td-title">Nama Perusahaan<i className="text-danger">*</i></td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <Input
                                        type="text"
                                        className="rounded-0"
                                        name="corpName"
                                        placeholder="Enter Nama Perusahaan"
                                        value={this.state.corpName}
                                        onChange={this.handleChange}
                                        required />
                                </td>
                            </tr>
                            <tr>
                                <td className="td-title">Dari<i className="text-danger">*</i></td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <Input
                                        type="date"
                                        className="rounded-0"
                                        name="corpStart"
                                        placeholder="Enter Dari"
                                        value={this.state.corpStart}
                                        onChange={this.handleChange}
                                        required />
                                </td>
                            </tr>
                            <tr>
                                <td className="td-title">Sampai<i className="text-danger">*</i></td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <Input
                                        type="date"
                                        className="rounded-0"
                                        name="corpEnd"
                                        placeholder="Enter Sampai"
                                        min={moment(this.state.corpStart).add(1, 'days').format("YYYY-MM-DD")}
                                        value={this.state.corpEnd}
                                        onChange={this.handleChange}
                                        required />
                                </td>
                            </tr>
                            <tr>
                                <td className="td-title">Role<i className="text-danger">*</i></td>
                                <td className="td-colon">:</td>
                                <td className="td-value">
                                    <Input
                                        type="text"
                                        className="rounded-0"
                                        name="role"
                                        placeholder="Enter Role"
                                        value={this.state.role}
                                        onChange={this.handleChange}
                                        required />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <Row>
                        <Col sm={{ size: 2, offset: 4 }}>
                            {this.state.isChange ?
                                <Button className="rounded-0" type="submit" color="primary" block outline>Save</Button>
                                :
                                <Button className="rounded-0" color="primary" block outline disabled>Save</Button>
                            }
                        </Col>
                        <Col sm={{ size: 2 }}>
                            <Button className="rounded-0" outline color="danger" block onClick={this.handleClick}>Reset</Button>
                        </Col>
                    </Row>
                </Form> <br />
                <Table responsive hover bordered className="text-center">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Perusahaan</th>
                            <th>Dari</th>
                            <th>Sampai</th>
                            <th>Role</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.dataEmployment.length < 1 ? <tr><td colSpan="6">0 data display</td></tr> :
                            this.state.dataEmployment.map(employe =>
                                <tr key={employe.employment_id}>
                                    <th>{employe.norut + 1}</th>
                                    <td>{employe.employment_corpname}</td>
                                    <td>{employe.employment_startdate}</td>
                                    <td>{employe.employment_enddate}</td>
                                    <td>{employe.employment_rolejob}</td>
                                    <td>
                                        <Button className="rounded-0 mr-2" outline color="primary" id="edit-emp" onClick={(e) => this.handleClickAction(e, employe.employment_id)}>
                                            <i className="fa fa-edit"></i>
                                        </Button>
                                        <Button className="rounded-0" outline color="danger" id="delete-emp" onClick={(e) => this.handleClickAction(e, employe.employment_id)}>
                                            <i className="fa fa-trash"></i>
                                        </Button>
                                    </td>
                                </tr>
                            )
                        }
                    </tbody>
                </Table>
                <Modal isOpen={this.state.isOpen} toggle={this.toggleModal} className={this.props.className} centered>
                    <ModalHeader toggle={this.toggleModal}>
                        <i className="fa fa-warning text-warning"></i> Warning
                    </ModalHeader>
                    <ModalBody>
                        Are you sure want to delete?
                    </ModalBody>
                    <ModalFooter>
                        <Button className="rounded-0" id="yes" color="primary" onClick={this.toggleModal}>Yes</Button>
                        <Button className="rounded-0" color="danger" onClick={this.toggleModal}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default Pekerjaan;