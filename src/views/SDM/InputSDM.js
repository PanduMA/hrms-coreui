import React, { Component } from 'react';
import {
    TabContent, TabPane,
    Nav, NavItem, NavLink,
    Card, CardBody,
    Alert
} from 'reactstrap';
import classnames from 'classnames';
import Pendidikan from './LayoutSDM/Pendidikan';
import Kursus from './LayoutSDM/Kursus';
import Pribadi from './LayoutSDM/Pribadi';
import Pekerjaan from './LayoutSDM/Pekerjaan';
import Profil from './LayoutSDM/Profil';
import Bahasa from './LayoutSDM/Bahasa';
class InputSDM extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            activeTab: '1',
            message: '',
            isVisible: false,
            sdmIdNew: null
        })
        this.toggle = this.toggle.bind(this)
        this.onDismiss = this.onDismiss.bind(this)
    }
    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({ activeTab: tab });
        }
    }
    onDismiss() {
        this.setState({ isVisible: false })
    }
    changeView(newActiveTab) {
        this.setState({ activeTab: newActiveTab })
    }
    changeViewId(newActiveTab,newId){
        this.setState({
            activeTab:newActiveTab,
            sdmIdNew:newId
        })
    }
    getMessage(newMessage) {
        this.setState({
            message: newMessage,
            isVisible: true
        })
        this.setTimeoutAlert()
    }
    setTimeoutAlert() {
        setTimeout(() => {
            this.setState({ isVisible: false })
        }, 3000);
    }
    setNewsdmID(newId) {
        this.setState({
            sdmIdNew: newId
        })
    }
    render() {
        return (
            <div>
                <Alert color="success" className="shadow-sm" isOpen={this.state.isVisible} toggle={this.onDismiss}>
                    {this.state.message}
                </Alert>
                <Card>
                    <CardBody>
                        <Nav tabs>
                            <NavItem>
                                {this.state.activeTab === '1' ?
                                    <NavLink className={classnames({ active: this.state.activeTab === '1' })}
                                        onClick={() => { this.toggle('1'); }}>Data Pribadi
                                    </NavLink>
                                    :
                                    <NavLink className={classnames({ active: this.state.activeTab === '1' })}
                                        style={{ cursor: 'no-drop', color: '#777' }}>Data Pribadi
                                    </NavLink>
                                }
                            </NavItem>
                            <NavItem>
                                {this.state.activeTab === '2' ?
                                    <NavLink className={classnames({ active: this.state.activeTab === '2' })}
                                        onClick={() => { this.toggle('2'); }}>Pendidikan
                                    </NavLink>
                                    :
                                    <NavLink className={classnames({ active: this.state.activeTab === '2' })}
                                    onClick={() => { this.toggle('2'); }}
                                        style={{ cursor: 'no-drop', color: '#777' }}>Pendidikan
                                    </NavLink>
                                }
                            </NavItem>
                            <NavItem>
                                {this.state.activeTab === '3' ?
                                    <NavLink className={classnames({ active: this.state.activeTab === '3' })}
                                        onClick={() => { this.toggle('3'); }}>Kursus
                                    </NavLink>
                                    :
                                    <NavLink className={classnames({ active: this.state.activeTab === '3' })}
                                        style={{ cursor: 'no-drop', color: '#777' }}>Kursus
                                    </NavLink>
                                }
                            </NavItem>
                            <NavItem>
                                {this.state.activeTab === '4' ?
                                    <NavLink className={classnames({ active: this.state.activeTab === '4' })}
                                        onClick={() => { this.toggle('4'); }}>Pekerjaan
                                    </NavLink>
                                    :
                                    <NavLink className={classnames({ active: this.state.activeTab === '4' })}
                                        style={{ cursor: 'no-drop', color: '#777' }}>Pekerjaan
                                    </NavLink>
                                }
                            </NavItem>
                            <NavItem>
                                {this.state.activeTab === '5' ?
                                    <NavLink className={classnames({ active: this.state.activeTab === '5' })}
                                        onClick={() => { this.toggle('5'); }}>Profil
                                    </NavLink>
                                    :
                                    <NavLink className={classnames({ active: this.state.activeTab === '5' })}
                                        style={{ cursor: 'no-drop', color: '#777' }}>Profil
                                    </NavLink>
                                }
                            </NavItem>
                            <NavItem >
                                {this.state.activeTab === '6' ?
                                    <NavLink className={classnames({ active: this.state.activeTab === '6' })}
                                        onClick={() => { this.toggle('6'); }}>Bahasa
                                    </NavLink>
                                    :
                                    <NavLink className={classnames({ active: this.state.activeTab === '6' })}
                                        style={{ cursor: 'no-drop', color: '#777' }}>Bahasa
                                    </NavLink>
                                }
                            </NavItem>
                        </Nav>
                        <TabContent activeTab={this.state.activeTab}>
                            <TabPane tabId="1">
                                <Pribadi
                                    view={this.changeViewId.bind(this)}
                                    message={this.getMessage.bind(this)}
                                    id={this.setNewsdmID.bind(this)}
                                />
                            </TabPane>
                            <TabPane tabId="2">
                                <Pendidikan
                                    view={this.changeView.bind(this)}
                                    message={this.getMessage.bind(this)}
                                    sdmIdNew={this.state.sdmIdNew}
                                    activeTab={this.state.activeTab}
                                />
                            </TabPane>
                            <TabPane tabId="3">
                                <Kursus view={this.changeView.bind(this)} message={this.getMessage.bind(this)} />
                            </TabPane>
                            <TabPane tabId="4">
                                <Pekerjaan view={this.changeView.bind(this)} message={this.getMessage.bind(this)} />
                            </TabPane>
                            <TabPane tabId="5">
                                <Profil view={this.changeView.bind(this)} message={this.getMessage.bind(this)} />
                            </TabPane>
                            <TabPane tabId="6">
                                <Bahasa message={this.getMessage.bind(this)} />
                            </TabPane>
                        </TabContent>
                    </CardBody>
                </Card>
            </div>
        );
    }
}

export default InputSDM;