import React, { Component } from 'react';
// import { Link, NavLink } from 'react-router-dom';
import { Redirect} from 'react-router-dom';
import { connect } from 'react-redux';
import { deleteUser } from '../../services/Actions';
import { Nav} from 'reactstrap';
import PropTypes from 'prop-types';

import { AppNavbarBrand, AppSidebarToggler } from '@coreui/react';

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {
  render() {
    const user = this.props.user;
    if (!user.user_id) {
      return (<Redirect to="/" />);
    }
    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        {/* <AppNavbarBrand
          full={{ src: logo, width: 89, height: 25, alt: 'CoreUI Logo' }}
          minimized={{ src: sygnet, width: 30, height: 30, alt: 'CoreUI Logo' }}
        /> */}
        <AppNavbarBrand>HRMS</AppNavbarBrand>
        <AppSidebarToggler className="d-md-down-none" display="lg" />

        <Nav className="ml-auto" navbar>
          <a href="" onClick={() => this.props.logout()} className="dropdown-item"><i className="fa fa-sign-out"></i> Logout</a>
        </Nav>
        {/* <AppAsideToggler className="d-md-down-none" /> */}
        {/*<AppAsideToggler className="d-lg-none" mobile />*/}
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;
DefaultHeader = connect(
  state => ({
    user: state.user
  }),
  (dispatch, ownProps) => ({
    logout: () => {
      localStorage.clear();
      dispatch(deleteUser())
    }
  })
)(DefaultHeader);
export default DefaultHeader;
