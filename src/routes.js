import React from 'react';

const Dashboard = React.lazy(() => import('./views/Dashboard'));
const ListSDM = React.lazy(() => import('./views/SDM/List'));
const InputSDM = React.lazy(() => import('./views/SDM/InputSDM'));
const UpdateSDM = React.lazy(() => import('./views/SDM/UpdateSDM'));
const ListSDMView = React.lazy(() => import('./views/SDM/ViewSDM'));
const Historis = React.lazy(() => import('./views/SDM/Historis'));
const ListPsycology = React.lazy(() => import('./views/SDM/Psycology'));
const UpdatePsycology = React.lazy(() => import('./views/SDM/UpdatePsycology'));
const ListSkill = React.lazy(() => import('./views/SDMAllocation/ListSkill'));
const ListCategory = React.lazy(() => import('./views/SDMAllocation/ListCategory'));
const ListSkillAllocation = React.lazy(() => import('./views/SDMAllocation/ListSDMSkillAllocation'));
const AddSkillAllocation = React.lazy(() => import('./views/SDMAllocation/AddSDMSkillAllocation'));
const DetailSkillAllocation = React.lazy(() => import('./views/SDMAllocation/DetailSDMSkillAllocation'));
const UpdateSkillAllocation = React.lazy(() => import('./views/SDMAllocation/UpdateSDMSkillAllocation'));
const MultiFilter = React.lazy(() => import('./views/SDMAllocation/MultiFilteringSDM'));
const InputProject = React.lazy(() => import('./views/ProjectAssignment/InputProjectSDM'));
const ListDetailProject = React.lazy(() => import('./views/ProjectAssignment/ListDetailProjectSDM'));
const UpdateProject = React.lazy(() => import('./views/ProjectAssignment/UpdateProject'));
const ListDetailClient = React.lazy(() => import('./views/ProjectAssignment/ListDetailClient'));
const AddClient = React.lazy(() => import('./views/ProjectAssignment/AddClient'));
const UpdateClient = React.lazy(() => import('./views/ProjectAssignment/UpdateClient'));
const Hiring = React.lazy(() => import('./views/ProjectAssignment/HiringSDM'));
const Hire = React.lazy(() => import('./views/ProjectAssignment/HireSDM'));
const UpdateHiring = React.lazy(() => import('./views/ProjectAssignment/UpdateHiring'));
const Assignment = React.lazy(() => import('./views/ProjectAssignment/AssignmentSDM'));
const UpdateAssignment = React.lazy(() => import('./views/ProjectAssignment/UpdateAssignmentSDM'));
// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/home', exact: true, name: 'Dashboard', component: Dashboard },
  { path: '/sdm/list', exact: true, name: 'List SDM', component: ListSDM },
  { path: '/sdm/list/add', exact: true, name: 'Add SDM', component: InputSDM },
  { path: '/sdm/list/view/:id', exact: true, name: 'Detail SDM', component: ListSDMView },
  { path: '/sdm/list/update/:id', exact: true, name: 'Update SDM', component: UpdateSDM },
  { path: '/sdm/historis', exact: true, name: 'Historis SDM', component: Historis },
  { path: '/sdm/historis/view/:id', exact: true, name: 'Detail SDM', component: ListSDMView },
  { path: '/sdm/psycology', exact: true, name: 'List Pyscology', component: ListPsycology },
  { path: '/sdm/psycology/update/:id', exact: true, component: UpdatePsycology },
  { path: '/sdmallocation/listskill', exact: true, name: 'List Skill', component: ListSkill },
  { path: '/sdmallocation/category', exact: true, name: 'List Category', component: ListCategory },
  { path: '/sdmallocation/list', exact: true, name: 'List SDM Skill Allocation', component: ListSkillAllocation },
  { path: '/sdmallocation/list/add', exact: true, name: 'Add SDM Skill Allocation', component: AddSkillAllocation },
  { path: '/sdmallocation/list/view/:id', exact: true, name: 'Detail Skill', component: DetailSkillAllocation },
  { path: '/sdmallocation/list/update/:id', exact: true, name: 'Update SDM Skill Allocation', component: UpdateSkillAllocation },
  { path: '/sdmallocation/multifilter', exact: true, name: 'Multi Filtering SDM', component: MultiFilter },
  { path: '/projectassignment/add', exact: true, name: 'Input Project SDM', component: InputProject },
  { path: '/projectassignment/list', exact: true, name: 'List Detail Project SDM', component: ListDetailProject },
  { path: '/projectassignment/list/update/:id', exact: true, name: 'Update Project SDM', component: UpdateProject },
  { path: '/projectassignment/client', exact: true, name: 'List Detail Client', component: ListDetailClient },
  { path: '/projectassignment/client/add', exact: true, name: 'Add Client', component: AddClient },
  { path: '/projectassignment/client/update/:id', exact: true, name: 'Update Client', component: UpdateClient },
  { path: '/projectassignment/hiring', exact: true, name: 'Hiring SDM', component: Hiring },
  { path: '/projectassignment/hire/:id', exact: true, name: 'Hire SDM', component: Hire },
  { path: '/projectassignment/hiring/update/:id', exact: true, name: 'Update Hiring', component: UpdateHiring },
  { path: '/projectassignment/assignment', exact: true, name: 'SDM Assignment', component: Assignment },
  { path: '/projectassignment/assignment/update/:id', exact: true, name: 'Update SDM Assignment', component: UpdateAssignment }
];

export default routes;
