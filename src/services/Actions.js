export const SET_FETCHING = 'SET_FETCHING';
export const SET_USER = 'SET_USER';
export const DELETE_USER = 'DELETE_USER';

export function setFetching(v) {
    return { type: SET_FETCHING, state: v };
}

export function setUser(user) {
    return { type: SET_USER, user };
}

export function deleteUser() {
    return { type: DELETE_USER };
}

export function actionTryLogin(values) {
    return dispatch => {
        dispatch(setFetching(true));
        fetch('http://209.97.160.40:8080/SA061118/auth/token', {
            body: JSON.stringify(values),
            method: 'POST',
        })
            .then(response => response.json())
            .then(data => {
                const user = data;
                if (user !== null) {
                    dispatch(setUser(user));
                    localStorage.setItem("user", JSON.stringify(user));
                    localStorage.setItem("token", user.access_token);
                }
                if (user.access_token == null) {
                    alert(user.message)
                }
                dispatch(setFetching(false));
            })
            .catch(err => {
                dispatch(setFetching(false));
            });
    };

}