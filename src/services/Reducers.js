import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import {
    SET_FETCHING,
    SET_USER,
    DELETE_USER
} from './Actions';

const userDefault = {
    id: 0,
    nama: ''
}

function user(state = userDefault, action) {
    switch (action.type) {
        case SET_USER:
            return Object.assign(state, action.user);
        case DELETE_USER:
            return userDefault;
        default:
            return state;
    }
}

function isFetching(state = false, action) {
    switch (action.type) {
        case SET_FETCHING: return action.state;
        default: return state;
    }
}

const loginApp = combineReducers({
    user,
    isFetching,
    form: formReducer
});

export default loginApp;