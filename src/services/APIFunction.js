const token = localStorage.getItem("token")
function setHeaders(token) {
    return {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
    }
}
export function getDatas(url) {
    return fetch(url, {
        method: 'GET',
        headers: setHeaders(token),
        body: null
    }).then((response) => {
        return response.json();
    })
}
export function getDatasFinal(url, skip = '', top = '', filter = '', orderby = '') {
    const uriFilter = encodeURIComponent(filter);
    return fetch(url + '?$skip=' + skip + '&$top=' + top + '&$filter=' + uriFilter + '&$orderby=' + orderby, {
        method: 'GET',
        headers: setHeaders(token),
    }).then((response) => {
        return response.json();
    })
}
export function postDatas(url, values) {
    return fetch(url, {
        method: 'POST',
        headers: setHeaders(token),
        body: values,
    }).then((response) => {
        return response.json();
    })
}
export function putDatas(url, values) {
    return fetch(url, {
        method: 'PUT',
        headers: setHeaders(token),
        body: values,
    }).then((response) => {
        return response.json();
    })
}
export function deleteDatas(url) {
    return fetch(url, {
        method: 'DELETE',
        headers: setHeaders(token),
    }).then((response) => {
        return response.json();
    })
}